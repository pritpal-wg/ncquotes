<?php
if (!function_exists('pr')) {

    function pr($e) {
        echo "<pre>";
        print_r($e);
        echo "</pre>";
    }

}

if (!function_exists('vd')) {

    function vd($e) {
        echo "<pre>";
        var_dump($e);
        echo "</pre>";
    }

}

if (!function_exists('cd')) {

    function cd($e) {
        echo "<code>";
        print_r($e);
        echo "</code>";
    }

}

function pippin_product_category_add_new_meta_field() {
    // this will add the custom meta field to the add new term page
    $args = array(
        'post_type' => 'colors',
        'child_of' => 0,
        'parent' => '',
        'orderby' => 'name',
        'order' => 'ASC',
        'hide_empty' => 0,
        'hierarchical' => 1,
        'exclude' => '',
        'include' => '',
        'number' => '',
    );
    $sub_color = get_posts($args);
    ?>
    <div class="form-field">
        <label for="term_meta[custom_term_meta]"><?php _e('Select Subcategory color', 'ncquotes'); ?></label>
        <select id="term_meta[custom_term_meta]" name="term_meta[custom_term_meta][]" style="width:95%" multiple>
            <?php
            if (!empty($sub_color)):
                foreach ($sub_color as $key => $value) {
                    $meta = get_post_meta($value->ID, color, true);
                    ?>
                    <option value="<?php echo $meta; ?>" <?php
                    if ($meta == $value->name) {
                        echo 'selected=selected';
                    }
                    ?>><?php echo __($meta); ?></option>                           

                    <?php
                }
            endif;
            ?>
        </select>
         <!--<input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="">-->

    </div>
    <?php
}

function pippin_product_category_edit_meta_field($term) {
    //check if the term doesn't have any children
    {
        $children = get_terms($term->taxonomy, array(
            'parent' => $term->term_id,
            'hide_empty' => false
        ));
        if (!empty($children))
            return;
    }
    $args = array(
        'post_type' => 'colors',
        'child_of' => 0,
        'parent' => '',
        'orderby' => 'name',
        'order' => 'ASC',
        'hide_empty' => 0,
        'hierarchical' => 1,
        'exclude' => '',
        'include' => '',
        'number' => '',
    );

    $sub_color = get_posts($args);

    // put the term ID into a variable
    $t_id = $term->term_id;

    // retrieve the existing value(s) for this meta field. This returns an array
    $term_meta = get_option("taxonomy_$t_id", array());
    $sub_colors = array();
    if (isset($term_meta['sub_colors'])) {
        $sub_colors = $term_meta['sub_colors'];
    }

    ob_start();
    ?>
    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="sub_colors"><?php _e('Subcategory color', 'ncquotes') ?></label>
        </th>
        <td>
            <?php if (!empty($sub_color)) { ?>
                <?php foreach ($sub_color as $key => $value) { ?>
                    <?php $color = get_post_meta($value->ID, 'color', true) ?>
                    <?php $selected = in_array($color, $sub_colors) ? ' checked' : '' ?>
                    <label style="margin-right: 20px">
                        <input type="checkbox" name="sub_colors[]" value="<?php echo $color ?>"<?= $selected ?> />
                        <?php echo __($color) ?>
                    </label>
                <?php } ?>
            <?php } ?>
        </td>
    </tr>
    <?php
    echo ob_get_clean();
}

function nc_product_printing_edit_form_fields($term) {
    return;
    // put the term ID into a variable
    $embroidary_slug = 'embroidery';
    $personalization_vinyl_slug = 'personalization-vinyl';
    $t_id = $term->term_id;
    $t_slug = $term->slug;

    // retrieve the existing value(s) for this meta field. This returns an array
    $term_meta = get_option("taxonomy_$t_id", array());
    $metas = array(
        'setup_cost',
        'prev_setup_cost',
        'cost_for_numbers',
        'cost_for_names',
        'cost_for_both',
        'min_order_quantity',
    );
    foreach ($metas as $meta) {
        $$meta = isset($term_meta[$meta]) ? $term_meta[$meta] : '';
    }
    ob_start();
    ?>
    <tr class="form-field">
        <th scope="row">
            <label for="setup_cost"><?php _e('Setup Cost', 'ncquotes') ?></label>
        </th>
        <td>
            <input type="text" id="setup_cost" name="setup_cost" size="40" value="<?php echo $setup_cost ?>" />
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row">
            <label for="prev_setup_cost"><?php _e('Previous Setup Cost', 'ncquotes') ?></label>
        </th>
        <td>
            <input type="text" id="prev_setup_cost" name="prev_setup_cost" size="40" value="<?php echo $prev_setup_cost ?>" />
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row">
            <label for="min_order_quantity"><?php _e('Minimum Order Quantity', 'ncquotes') ?></label>
        </th>
        <td>
            <input type="number" id="min_order_quantity" name="min_order_quantity" size="40" value="<?php echo $min_order_quantity ?>" min="1" />
        </td>
    </tr>
    <?php if ($t_slug === $personalization_vinyl_slug) { ?>
        <tr class="form-field">
            <th scope="row">
                <label for="cost_for_numbers"><?php _e('Cost for Number', 'ncquotes') ?></label>
            </th>
            <td>
                <input type="text" id="cost_for_numbers" name="cost_for_numbers" size="40" value="<?php echo $cost_for_numbers ?>" />
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row">
                <label for="cost_for_names"><?php _e('Cost for Names', 'ncquotes') ?></label>
            </th>
            <td>
                <input type="text" id="cost_for_names" name="cost_for_names" size="40" value="<?php echo $cost_for_names ?>" />
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row">
                <label for="cost_for_both"><?php _e('Cost for Both Numbers & Names', 'ncquotes') ?></label>
            </th>
            <td>
                <input type="text" id="cost_for_both" name="cost_for_both" size="40" value="<?php echo $cost_for_both ?>" />
            </td>
        </tr>
    <?php } ?>
    <?php
    echo ob_get_clean();
}

function nc_product_printing_add_form_fields() {
    return;
    ?>
    <div class="form-field">
        <label for="setup_cost"><?php _e('Setup Cost', 'ncquotes') ?></label>
        <input type="text" id="setup_cost" name="setup_cost" size="40" />
    </div>
    <div class="form-field">
        <label for="prev_setup_cost"><?php _e('Previous Setup Cost', 'ncquotes') ?></label>
        <input type="text" id="prev_setup_cost" name="prev_setup_cost" size="40" />
    </div>
    <div class="form-field">
        <label for="min_order_quantity"><?php _e('Minimum Order Quantity', 'ncquotes') ?></label>
        <input type="number" id="min_order_quantity" name="min_order_quantity" size="40" value="1" min="1" />
    </div>
    <?php
}

function nc_save_product_printing_fields($term_id) {
    return;
    $term_meta = get_option("taxonomy_$term_id", array());
    $metas = array(
        'setup_cost',
        'prev_setup_cost',
        'cost_for_numbers',
        'cost_for_names',
        'cost_for_both',
        'min_order_quantity',
    );
    foreach ($metas as $meta) {
        $term_meta[$meta] = isset($_POST[$meta]) ? esc_sql($_POST[$meta]) : '';
    }
    update_option("taxonomy_$term_id", $term_meta);
}

function save_product_category_custom_meta($term_id) {
    $term_meta = get_option("taxonomy_$term_id", array());
    $sub_colors = isset($_POST['sub_colors']) ? $_POST['sub_colors'] : array();
    if (isset($term_meta['sub_colors']))
        unset($term_meta['sub_colors']);
    if (!empty($sub_colors)) {
        $term_meta['sub_colors'] = $sub_colors;
    }
    update_option("taxonomy_$term_id", $term_meta);
}

function nc_update_term_parent($parent, $term_id, $taxonomy) {
    if ($parent && $taxonomy === 'product_category') {
        $term_meta = get_option("taxonomy_$parent", array());
        if (isset($term_meta['sub_colors']) && !empty($term_meta['sub_colors'])) {
            @session_start();
            $_SESSION['nc_admin_notices'] = 'nc_error_setting_taxonomy_parent';
            return;
        }
    }
    return $parent;
}

function nc_error_setting_taxonomy_parent() {
    echo "<div class=\"error\"> <p>Can't Set Parent, Please remove all color associations from Parent Taxonomy First.</p></div>";
}

function nc_scr_printing_pricing_cats() {
    $ret = array();
    $scr_pricing = get_option('nc_scr_pricing');
    if (is_array($scr_pricing))
        foreach ($scr_pricing as $pricing)
            if (!in_array($pricing['pricing_category'], $ret))
                $ret[] = $pricing['pricing_category'];
    return $ret;
}

function nc_register_settings() {
    $group_settings = array(
        'nc-general-settings-group' => array(
            'nc_bottom_message',
            'nc_admin_email',
        ),
        'nc-embroidery-settings-group' => array(
            'nc_emb_num_print_locations',
            'nc_emb_print_locations',
            'nc_emb_min_order_quantity',
            'nc_emb_setup_cost',
            'nc_emb_prev_setup_cost',
            'nc_emb_allow_vinyl_personalization',
        ),
        'nc-screen_printing-settings-group' => array(
            'nc_scr_num_print_locations',
            'nc_scr_min_order_quantity',
            'nc_scr_setup_cost',
            'nc_scr_prev_setup_cost',
            'nc_scr_white_color_cost',
            'nc_scr_print_locations',
            'nc_scr_num_colors_art',
            'nc_scr_allow_vinyl_personalization',
        ),
        'nc-2screen_printing-settings-group' => array(
            'nc_2sc_num_print_locations',
            'nc_2sc_min_order_quantity',
            'nc_2sc_setup_cost',
            'nc_2sc_prev_setup_cost',
            'nc_2sc_white_color_cost',
            'nc_2sc_print_locations',
            'nc_2sc_num_colors_art',
            'nc_2sc_allow_vinyl_personalization',
        ),
        'nc-printable_vinyl-settings-group' => array(
            'nc_pri_num_print_locations',
            'nc_pri_print_locations',
            'nc_pri_allow_vinyl_personalization',
            'nc_pri_min_order_quantity',
            'nc_pri_setup_cost',
            'nc_pri_prev_setup_cost',
        ),
        'nc-personalization_vinyl-settings-group' => array(
            'nc_pre_num_print_locations',
            'nc_pre_print_locations',
            'nc_pre_min_order_quantity',
            'nc_pre_setup_cost',
            'nc_pre_prev_setup_cost',
        ),
        'nc-all_drinkware-settings-group' => array(
            'nc_all_num_print_locations',
            'nc_all_print_locations',
            'nc_all_min_order_quantity',
            'nc_all_setup_cost',
            'nc_all_prev_setup_cost',
        ),
        'nc-sublimation-settings-group' => array(
            'nc_sub_num_print_locations',
            'nc_sub_print_locations',
            'nc_sub_min_order_quantity',
            'nc_sub_setup_cost',
            'nc_sub_prev_setup_cost',
        ),
        'nc-hat-embrodiery-settings-group' => array(
            'nc_hat_num_print_locations',
            'nc_hat_print_locations',
            'nc_hat_allow_vinyl_personalization',
            'nc_hat_min_order_quantity',
            'nc_hat_setup_cost',
            'nc_hat_prev_setup_cost',
        ),
    );
    foreach ($group_settings as $group => $settings) {
        foreach ($settings as $setting) {
            register_setting($group, $setting);
        }
    }
}

function nc_active_current_menu() {
    global $screen;
    ob_start();
    ?>
    <script>
        (function ($) {
            $(document).ready(function () {
                //alert(1);
            });
        })(jQuery);
    </script>
    <?php
    echo ob_get_clean();
}

function nc_add_emb_pricing() {
    //set option name
    $option_name = 'nc_emb_pricing';

    //get prev option
    $nc_emb_pricing = get_option($option_name, array());

    //get unique key for pricing
    {
        $unique_key = substr(md5(uniqid('', true)), 0, 10);
        while (array_key_exists($unique_key, $nc_emb_pricing)) {
            $unique_key = substr(md5(uniqid('', true)), 0, 10);
        }
    }

    //add data
    {
        extract($_POST);
        $nc_emb_pricing[$unique_key] = array(
            'stitches' => $stitches_from . '-' . $stitches_to,
            'pieces' => $pieces_from . '-' . $pieces_to,
            'pricing' => $pricing,
        );
    }

    //update new data
    update_option($option_name, $nc_emb_pricing);
    die;
}

function nc_add_scr_pricing() {
    //set option name
    $option_name = 'nc_scr_pricing';

    //get prev option
    $nc_pricing = get_option($option_name, array());

    //get unique key for pricing
    {
        $unique_key = substr(md5(uniqid('', true)), 0, 10);
        while (array_key_exists($unique_key, $nc_pricing)) {
            $unique_key = substr(md5(uniqid('', true)), 0, 10);
        }
    }

    //add data
    {
        extract($_POST);
        $nc_pricing[$unique_key] = array(
            'pricing_category' => $pricing_category,
            'colors_from' => $colors_from,
            'colors_to' => $colors_to,
            'pricing' => $pricing,
        );
    }

    //update new data
    update_option($option_name, $nc_pricing);
    die;
}

function nc_add_2sc_pricing() {
    //set option name
    $option_name = 'nc_2sc_pricing';

    //get prev option
    $nc_pricing = get_option($option_name, array());

    //get unique key for pricing
    {
        $unique_key = substr(md5(uniqid('', true)), 0, 10);
        while (array_key_exists($unique_key, $nc_pricing)) {
            $unique_key = substr(md5(uniqid('', true)), 0, 10);
        }
    }

    //add data
    {
        extract($_POST);
        $nc_pricing[$unique_key] = array(
            'pricing_category' => $pricing_category,
            'colors_from' => $colors_from,
            'colors_to' => $colors_to,
            'pricing' => $pricing,
        );
    }

    //update new data
    update_option($option_name, $nc_pricing);
    die;
}

function nc_add_pri_pricing() {
    //set option name
    $option_name = 'nc_pri_pricing';

    //get prev option
    $nc_pricing = get_option($option_name, array());

    //get unique key for pricing
    {
        $unique_key = substr(md5(uniqid('', true)), 0, 10);
        while (array_key_exists($unique_key, $nc_pricing)) {
            $unique_key = substr(md5(uniqid('', true)), 0, 10);
        }
    }

    //add data
    {
        extract($_POST);
        $nc_pricing[$unique_key] = array(
            'location' => $location,
            'to_quantity' => $to_quantity,
            'price' => $price
        );
    }

    //update new data
    update_option($option_name, $nc_pricing);
    die;
}

function nc_add_hat_pricing() {
    //set option name
    $option_name = 'nc_hat_pricing';

    //get prev option
    $nc_pricing = get_option($option_name, array());

    //get unique key for pricing
    {
        $unique_key = substr(md5(uniqid('', true)), 0, 10);
        while (array_key_exists($unique_key, $nc_pricing)) {
            $unique_key = substr(md5(uniqid('', true)), 0, 10);
        }
    }

    //add data
    {
        extract($_POST);
        $nc_pricing[$unique_key] = array(
            'location' => $location,
            'to_quantity' => $to_quantity,
            'price' => $price
        );
    }

    //update new data
    update_option($option_name, $nc_pricing);
    die;
}

function nc_add_pre_pricing() {
    //set option name
    $option_name = 'nc_pre_pricing';

    //get prev option
    $nc_pricing = get_option($option_name, array());

    //get unique key for pricing
    {
        $unique_key = substr(md5(uniqid('', true)), 0, 10);
        while (array_key_exists($unique_key, $nc_pricing)) {
            $unique_key = substr(md5(uniqid('', true)), 0, 10);
        }
    }

    //add data
    {
        extract($_POST);
        $nc_pricing[$unique_key] = array(
            'to_quantity' => $to_quantity,
            'price_numbers' => $price_numbers,
            'price_names' => $price_names,
            'price_both' => $price_both,
        );
    }

    //update new data
    update_option($option_name, $nc_pricing);
    die;
}

function nc_add_all_pricing() {
    //set option name
    $option_name = 'nc_all_pricing';

    //get prev option
    $nc_pricing = get_option($option_name, array());

    //get unique key for pricing
    {
        $unique_key = substr(md5(uniqid('', true)), 0, 10);
        while (array_key_exists($unique_key, $nc_pricing)) {
            $unique_key = substr(md5(uniqid('', true)), 0, 10);
        }
    }

    //add data
    {
        extract($_POST);
        $nc_pricing[$unique_key] = array(
            'to_quantity' => $to_quantity,
            'price_1_side' => $price_1_side,
            'price_2_sides' => $price_2_sides,
        );
    }

    //update new data
    update_option($option_name, $nc_pricing);
    die;
}

function nc_add_sub_pricing() {
    //set option name
    $option_name = 'nc_sub_pricing';

    //get prev option
    $nc_pricing = get_option($option_name, array());

    //get unique key for pricing
    {
        $unique_key = substr(md5(uniqid('', true)), 0, 10);
        while (array_key_exists($unique_key, $nc_pricing)) {
            $unique_key = substr(md5(uniqid('', true)), 0, 10);
        }
    }

    //add data
    {
        extract($_POST);
        $nc_pricing[$unique_key] = array(
            'location' => $location,
            'to_quantity' => $to_quantity,
            'price' => $price,
        );
    }

    //update new data
    update_option($option_name, $nc_pricing);
    die;
}

function nc_get_the_excerpt($post_id) {
    global $post;
    $save_post = $post;
    $post = get_post($post_id);
    $output = get_the_excerpt();
    $post = $save_post;
    return $output;
}

function nc_inverseHex($color) {
    $color = TRIM($color);
    $prependHash = FALSE;

    if (strpos($color, '#') !== FALSE) {
        $prependHash = TRUE;
        $color = str_replace('#', NULL, $color);
    }

    switch ($len = strlen($color)) {
        case 3:
            $color = preg_replace("/(.)(.)(.)/", "\\1\\1\\2\\2\\3\\3", $color);
        case 6:
            break;
        default:
            trigger_error("Invalid hex length ($len). Must be (3) or (6)", E_USER_ERROR);
    }

    if (!preg_match('/[a-f0-9]{6}/i', $color)) {
        $color = htmlentities($color);
        trigger_error("Invalid hex string #$color", E_USER_ERROR);
    }

    $r = dechex(255 - hexdec(substr($color, 0, 2)));
    $r = (strlen($r) > 1) ? $r : '0' . $r;
    $g = dechex(255 - hexdec(substr($color, 2, 2)));
    $g = (strlen($g) > 1) ? $g : '0' . $g;
    $b = dechex(255 - hexdec(substr($color, 4, 2)));
    $b = (strlen($b) > 1) ? $b : '0' . $b;

    return ($prependHash ? '#' : '') . $r . $g . $b;
}

function nc_hide_add_quote_link() {
    if ('quotes' == get_post_type()) {
        echo '<style type="text/css">
            .page-title-action,
            #side-sortables,
            #pyre_post_options,
            #wp-content-wrap,
            #post-status-info,
            #edit-slug-box,
            #normal-sortables,
            label[for="pyre_post_options-hide"],
            #screen-meta-links
            {display:none;}
            </style>';
    }
}

function nc_quotes_meta_box() {
    add_meta_box('myplugin_sectionid', __('Quote Information', 'myplugin_textdomain'), 'nc_quotes_meta_box_callback', 'quotes');
}

function nc_quotes_meta_box_callback($post) {
    //used values
    {
        $_post_type = "products";
        $_quote_post_type = "quotes";
        $_taxonomy = 'product_category';
        $_size_taxonomy = 'product_size';
        $_color_post_type = 'colors';
        $_printing_taxonomy = 'product_printing';
        $_printing_slugs = get_option('_nc_default_printings');
        $_slug_printings = array_flip($_printing_slugs);
    }

    $post_id = $post->ID;
    $id = $post_id;
    $prod_id = get_post_meta($post_id, 'nc_prod_id', true);
    $product = get_post($prod_id);
    //get pricings
    $pricings = apply_filters('nc_get_product_pricings', get_post_meta($prod_id, '_nc_pricings', true));

    //get printings
    {
        $product_printing_ids = get_post_meta($prod_id, '_nc_printings', true);
        if (is_array($product_printing_ids) && !empty($product_printing_ids)) {
            $product_printings = get_terms($_printing_taxonomy, array(
                'hide_empty' => false,
                'fields' => 'id=>name',
                'include' => $product_printing_ids,
            ));
        }
    }

    //get product sizes
    {
        $product_size_ids = isset($pricings['size']) && is_array($pricings['size']) && !empty($pricings['size']) ? array_keys($pricings['size']) : array();
        if (!empty($product_size_ids)) {
            $product_sizes = get_terms($_size_taxonomy, array(
                'hide_empty' => false,
                'fields' => 'id=>name',
                'include' => $product_size_ids,
            ));
        }
    }

    //get product colors
    {
        $product_color_ids = isset($pricings['color']) && is_array($pricings['color']) && !empty($pricings['color']) ? array_keys($pricings['color']) : array();
        if (!empty($product_color_ids)) {
            $product_colors = get_posts(array(
                'post_type' => $_color_post_type,
                'include' => $product_color_ids,
            ));

            //color id as key
            {
                $temp = array();
                foreach ($product_colors as $product_color)
                    $temp[$product_color->ID] = $product_color;
                $product_colors = $temp;
            }
        }
    }

    //printing_type
    {
        $printing_type_id = get_post_meta($id, 'nc_printing_type_id', true);
        $product_printing = $product_printings[$printing_type_id];
        $printing_type_slug = get_term($printing_type_id, $_printing_taxonomy)->slug;
        $printing_type_short = substr($printing_type_slug, 0, 3);
    }


    //printing costs
    {
        $printing_type_setup_cost = get_option("nc_{$printing_type_short}_setup_cost", 0);
        $printing_type_prev_setup_cost = get_option("nc_{$printing_type_short}_prev_setup_cost", 0);
    }

    //color_id
    $color_id = get_post_meta($id, 'nc_color_id', true);
    $is_dark_color = get_post_meta($color_id, 'is_dark_color', true);

    //size & quantity
    $prod_quantity = get_post_meta($id, 'nc_size_quantity', true);

    //num_stitches
    $num_stitches = get_post_meta($id, 'nc_print_location', true);
    if (!is_array($num_stitches) || empty($num_stitches)) {
        $num_stitches = get_post_meta($id, 'nc_num_stitches', true);
    }

    //num_colors_art
    $num_colors_art = get_post_meta($id, 'nc_num_colors_art', true);

    //nc_size_quantity
    $size_quantity = get_post_meta($id, 'nc_size_quantity', true);

    //nc_is_previous_order
    $is_previous_order = get_post_meta($id, 'nc_is_previous_order', true);

    //nc_vinyl_personalization
    $vinyl_personalization = get_post_meta($id, 'nc_vinyl_personalization', true);

    //nc_is_white_color
    $nc_is_white_color = get_post_meta($id, 'nc_is_white_color', true);

    //nc_white_color_print_cost
    $nc_white_color_print_cost = get_option('nc_scr_white_color_cost');

    //nc_scr_pricing
    $nc_scr_pricing = get_option('nc_' . $printing_type_short . '_pricing', array());
    $scr_pricing_cat = get_post_meta($prod_id, '_nc_' . $printing_type_short . '_pricing_cat', true);
    $mega_cost = 0;
    $tot_items = array_sum($size_quantity);
    $GLOBALS['tot_items'] = $tot_items;
    //calculate various prices
    {
        //setup string and cost
        {
            $setup_cost = 0;
            $setup_string = '';
            $i = 0;
            if (is_array($num_stitches)) {
                foreach ($num_stitches as $k => $location) {
                    $location = trim($location);
                    if (!$location)
                        continue;
                    $cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                    $setup_string .= "$$cost + ";
                    $setup_cost += $cost;
                    $i++;
                }
                $setup_string = substr($setup_string, 0, -3);
            }
            $setup_string = ($i > 1 ? $setup_string . " = $" . $setup_cost : '$' . $setup_cost);
            $mega_cost+=$setup_cost;
        }

        //decoration cost
        {
            $decoration_cost = 0;
            if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['screen_printing'] || $printing_type_slug === $_printing_slugs['2screen_printing'])) {
                foreach ($num_stitches as $k => $location) {
                    $location = trim($location);
                    if (!$location)
                        continue;
                    $colors = $num_colors_art[$k];
                    if ($is_dark_color && !$nc_is_white_color[$k])
                        $colors++;
                    $cost = nc_get_scr_pri_cost($nc_scr_pricing, $scr_pricing_cat, $tot_items);
                    if ($cost)
                        $cost = $cost * $colors;
                    $decoration_cost += ($cost * $tot_items);
                }
            }
            if (is_array($num_stitches) && $printing_type_slug === $_printing_slugs['embroidery']) {
                foreach ($num_stitches as $k => $stitches) {
                    if (!$stitches)
                        continue;
                    $stitches_cost = nc_get_stitches_cost($stitches, $tot_items);
                    $decoration_cost += ($stitches_cost * $tot_items);
                }
            }
            if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['printable_vinyl'] || $printing_type_slug === $_printing_slugs['sublimation'] || $printing_type_slug === $_printing_slugs['hats_embroidery'])) {
                foreach ($num_stitches as $k => $location) {
                    if (!$location)
                        continue;
                    $location_cost = nc_get_location_cost($location, $tot_items, ($printing_type_slug === $_printing_slugs['printable_vinyl'] ? 'pri' : ($printing_type_slug === $_printing_slugs['hats_embroidery'] ? 'hat' : 'sub')));
                    $decoration_cost += ($location_cost * $tot_items);
                }
            }
            if (is_array($num_stitches) && $printing_type_slug === $_printing_slugs['all_drinkware']) {
                foreach ($num_stitches as $k => $location) {
                    if (!$location)
                        continue;
                    $location_cost = nc_get_all_location_cost($location, $tot_items);
                    $decoration_cost += ($location_cost * $tot_items);
                }
            }
            $mega_cost += $decoration_cost;
        }

        //personalization cost
        {
            $personalization_cost = 0;
            if ($vinyl_personalization && $printing_type_slug !== $_printing_slugs['all_drinkware']) {
                $vinyl_prices = get_option('nc_pre_pricing');
                if (is_array($vinyl_prices)) {
                    $output = array_filter($vinyl_prices, 'nc_anony_func_2');
                    if (empty($output))
                        $output = $vinyl_prices;
                    $select_vinyl_price = array_shift($output);
                    if (is_array($select_vinyl_price)) {
                        $l_vinyl_personalization = strtolower($vinyl_personalization);
                        if (isset($select_vinyl_price["price_{$l_vinyl_personalization}"])) {
                            $personalization_cost = ($select_vinyl_price["price_{$l_vinyl_personalization}"] * $tot_items);
                        }
                    }
                }
            }
            $mega_cost += $personalization_cost;
        }

        //product total cost
        {
            $size_cost = 0;
            foreach ($prod_quantity as $s_id => $s_qty) {
                $size_cost += ($s_qty * $pricings['size'][$s_id]);
            }
            $color_price = isset($pricings['color'][$color_id]) ? $pricings['color'][$color_id] : 0;
            $color_mega_cost = $color_price * $tot_items;
            $prod_tot_cost = $size_cost + $color_mega_cost;
            $mega_cost += $prod_tot_cost;
        }
    }
    ob_start();
    ?>
    <style>
        .v-center {
            height: 100vh;
            width: 100%;
            display: table;
            position: relative;
            text-align: center;
        }

        .v-center > div {
            display: table-cell;
            vertical-align: middle;
            position: relative;
            top: -10%;
        }

        .btn {
            font-size: 2vmin;
            padding: 0.75em 1.5em;
            background-color: #fff;
            border: 1px solid #bbb;
            color: #333;
            text-decoration: none;
            display: inline;
            border-radius: 4px;
            -webkit-transition: background-color 1s ease;
            -moz-transition: background-color 1s ease;
            transition: background-color 1s ease;
        }

        .btn:hover {
            background-color: #ddd;
            -webkit-transition: background-color 1s ease;
            -moz-transition: background-color 1s ease;
            transition: background-color 1s ease;
        }

        .btn-small {
            padding: .75em 1em;
            font-size: 0.8em;
        }

        .modal-box {
            display: none;
            position: absolute;
            z-index: 1000;
            width: 98%;
            background: white;
            border-bottom: 1px solid #aaa;
            border-radius: 4px;
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
            border: 1px solid rgba(0, 0, 0, 0.1);
            background-clip: padding-box;
        }
        @media (min-width: 32em) {

            .modal-box { width: 70%; }
        }

        .modal-box header,
        .modal-box .modal-header {
            padding: 1.25em 1.5em;
            border-bottom: 1px solid #ddd;
        }

        .modal-box header h3,
        .modal-box header h4,
        .modal-box .modal-header h3,
        .modal-box .modal-header h4 { margin: 0; }

        .modal-box .modal-body { padding: 2em 1.5em; }

        .modal-box footer,
        .modal-box .modal-footer {
            padding: 1em;
            border-top: 1px solid #ddd;
            background: rgba(0, 0, 0, 0.02);
            text-align: right;
        }

        .modal-overlay {
            opacity: 0;
            filter: alpha(opacity=0);
            position: absolute;
            top: 0;
            left: 0;
            z-index: 900;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.3) !important;
        }

        a.close {
            line-height: 1;
            font-size: 1.5em;
            position: absolute;
            top: 5%;
            right: 2%;
            text-decoration: none;
            color: #bbb;
        }

        a.close:hover {
            color: #222;
            -webkit-transition: color 1s ease;
            -moz-transition: color 1s ease;
            transition: color 1s ease;
        }
        div#popup1 {
            position: fixed;
            left: 25% !important;
        }
        .modal-box {
            width: 50% !important;
        }
        table {
            //max-width: 100%;
            background-color: transparent;
        }

        th {
            text-align: left;
        }

        .table {
            width: 100%;
            margin-bottom: 20px;
        }

        .table thead > tr > th,
        .table tbody > tr > th,
        .table tfoot > tr > th,
        .table thead > tr > td,
        .table tbody > tr > td,
        .table tfoot > tr > td {
            padding: 8px;
            line-height: 1.428571429;
            vertical-align: top;
            border-top: 1px solid #dddddd;
        }

        .table thead > tr > th {
            vertical-align: bottom;
        }

        .table caption + thead tr:first-child th,
        .table colgroup + thead tr:first-child th,
        .table thead:first-child tr:first-child th,
        .table caption + thead tr:first-child td,
        .table colgroup + thead tr:first-child td,
        .table thead:first-child tr:first-child td {
            border-top: 0;
        }

        .table tbody + tbody {
            border-top: 2px solid #dddddd;
        }

        .table .table {
            background-color: #ffffff;
        }

        .table-condensed thead > tr > th,
        .table-condensed tbody > tr > th,
        .table-condensed tfoot > tr > th,
        .table-condensed thead > tr > td,
        .table-condensed tbody > tr > td,
        .table-condensed tfoot > tr > td {
            padding: 5px;
        }

        .table-bordered {
            border: 1px solid #dddddd;
        }

        .table-bordered > thead > tr > th,
        .table-bordered > tbody > tr > th,
        .table-bordered > tfoot > tr > th,
        .table-bordered > thead > tr > td,
        .table-bordered > tbody > tr > td,
        .table-bordered > tfoot > tr > td {
            border: 1px solid #dddddd;
        }

        .table-striped > tbody > tr:nth-child(odd) > td,
        .table-striped > tbody > tr:nth-child(odd) > th {
            background-color: #f9f9f9;
        }

        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: #f5f5f5;
        }

        table col[class^="col-"] {
            display: table-column;
            float: none;
        }

        table td[class^="col-"],
        table th[class^="col-"] {
            display: table-cell;
            float: none;
        }

        .table > thead > tr > td.active,
        .table > tbody > tr > td.active,
        .table > tfoot > tr > td.active,
        .table > thead > tr > th.active,
        .table > tbody > tr > th.active,
        .table > tfoot > tr > th.active,
        .table > thead > tr.active > td,
        .table > tbody > tr.active > td,
        .table > tfoot > tr.active > td,
        .table > thead > tr.active > th,
        .table > tbody > tr.active > th,
        .table > tfoot > tr.active > th {
            background-color: #f5f5f5;
        }

        .table > thead > tr > td.success,
        .table > tbody > tr > td.success,
        .table > tfoot > tr > td.success,
        .table > thead > tr > th.success,
        .table > tbody > tr > th.success,
        .table > tfoot > tr > th.success,
        .table > thead > tr.success > td,
        .table > tbody > tr.success > td,
        .table > tfoot > tr.success > td,
        .table > thead > tr.success > th,
        .table > tbody > tr.success > th,
        .table > tfoot > tr.success > th {
            background-color: #dff0d8;
            border-color: #d6e9c6;
        }

        .table > thead > tr > td.danger,
        .table > tbody > tr > td.danger,
        .table > tfoot > tr > td.danger,
        .table > thead > tr > th.danger,
        .table > tbody > tr > th.danger,
        .table > tfoot > tr > th.danger,
        .table > thead > tr.danger > td,
        .table > tbody > tr.danger > td,
        .table > tfoot > tr.danger > td,
        .table > thead > tr.danger > th,
        .table > tbody > tr.danger > th,
        .table > tfoot > tr.danger > th {
            background-color: #f2dede;
            border-color: #eed3d7;
        }

        .table > thead > tr > td.warning,
        .table > tbody > tr > td.warning,
        .table > tfoot > tr > td.warning,
        .table > thead > tr > th.warning,
        .table > tbody > tr > th.warning,
        .table > tfoot > tr > th.warning,
        .table > thead > tr.warning > td,
        .table > tbody > tr.warning > td,
        .table > tfoot > tr.warning > td,
        .table > thead > tr.warning > th,
        .table > tbody > tr.warning > th,
        .table > tfoot > tr.warning > th {
            background-color: #fcf8e3;
            border-color: #fbeed5;
        }

        .table-hover > tbody > tr > td.success:hover,
        .table-hover > tbody > tr > th.success:hover,
        .table-hover > tbody > tr.success:hover > td {
            background-color: #d0e9c6;
            border-color: #c9e2b3;
        }

        .table-hover > tbody > tr > td.danger:hover,
        .table-hover > tbody > tr > th.danger:hover,
        .table-hover > tbody > tr.danger:hover > td {
            background-color: #ebcccc;
            border-color: #e6c1c7;
        }

        .table-hover > tbody > tr > td.warning:hover,
        .table-hover > tbody > tr > th.warning:hover,
        .table-hover > tbody > tr.warning:hover > td {
            background-color: #faf2cc;
            border-color: #f8e5be;
        }
    </style>
    <table class="table table-bordered">
        <tbody>
            <?php if ($nc_user_details = get_post_meta($id, '_nc_user_details', true)) { ?>
                <tr>
                    <th colspan="2"><span style="margin-left: 2.5em;font-size: 110%">User Details</span></th>
                </tr>
                <tr>
                    <th>Full Name</th>
                    <td><?php echo $nc_user_details['full_name'] ?></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><?php echo $nc_user_details['email'] ?></td>
                </tr>
                <tr>
                    <th>Phone Number</th>
                    <td><?php echo $nc_user_details['phone'] ?></td>
                </tr>
            <?php } ?>
            <tr>
                <th colspan="2"><span style="margin-left: 2.5em;font-size: 110%">Quote Details</span></th>
            </tr>
            <tr>
                <th>Product</th>
                <td><?php echo get_the_title($prod_id) ?></td>
            </tr>
            <tr>
                <th>Color</th>
                <td><?php echo $product_colors[$color_id]->post_title ?></td>
            </tr>
            <tr>
                <th>Decoration Method</th>
                <td><?php echo $product_printing ?></td>
            </tr>
            <tr>
                <th>Total Items</th>
                <td><?php echo $tot_items ?></td>
            </tr>
            <tr hidden>
                <th>Setup Cost</th>
                <td><?php echo $setup_string ?></td>
            </tr>
            <tr>
                <th>Decoration Cost</th>
                <td><?php echo "$" . ($decoration_cost + $setup_cost) ?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['screen_printing'] || $printing_type_slug === $_printing_slugs['2screen_printing'])) { ?>
                        <table class="table table-bordered" style="text-align:left">
                            <tbody>
                                <tr>
                                    <th class="left"><b>Location</b></th>
                                    <th class="left"><b>Number of Colors</b></th>
                                    <th class="right"><b>Unit Price</b></th>
                                    <th class="left"><b>Quantity</b></th>
                                    <!--<th class="left"><b>Setup Cost</b></th>-->
                                    <th class="left"><b>Total</b></th>
                                </tr>
                                <?php foreach ($num_stitches as $k => $location) { ?>
                                    <?php
                                    $location = trim($location);
                                    if (!$location)
                                        continue;
                                    $colors = $num_colors_art[$k];
                                    if ($is_dark_color && !$nc_is_white_color[$k])
                                        $colors++;
//                                    $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                    $cost = nc_get_scr_pri_cost($nc_scr_pricing, $scr_pricing_cat, $tot_items);
                                    ?>
                                    <tr>
                                        <th><?= $location ?></th>
                                        <td class="left"><?= $colors ?></td>
                                        <td class="right"><?php echo $cost ?></td>
                                        <td><?php echo $tot_items ?></td>
                                        <!--<td><?php echo "$" . $s_cost ?></td>-->
                                        <td><?php echo "$" . (($cost * $tot_items * $colors)) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    <?php if (is_array($num_stitches) && $printing_type_slug === $_printing_slugs['embroidery']) { ?>
                        <table class="table table-bordered" style="text-align:left">
                            <thead>
                                <tr>
                                    <th>Stitches</th>
                                    <th>Cost</th>
                                    <th>Quantity</th>
                                    <!--<th>Setup Cost</th>-->
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($num_stitches as $k => $stitches) { ?>
                                    <?php
                                    if (!$stitches)
                                        continue;
                                    $stitches_cost = nc_get_stitches_cost($stitches, $tot_items);
//                                    $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                    ?>
                                    <tr>
                                        <th><?php echo $stitches ?></th>
                                        <td>$<?php echo $stitches_cost ?></td>
                                        <td><?php echo $tot_items ?></td>
                                        <!--<td><?php echo "$" . $s_cost ?></td>-->
                                        <td>$<?php echo ($tot_items * $stitches_cost) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    <?php if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['printable_vinyl'] || $printing_type_slug === $_printing_slugs['sublimation'] || $printing_type_slug === $_printing_slugs['hats_embroidery'])) { ?>
                        <table class="table table-bordered" style="text-align:left">
                            <thead>
                                <tr>
                                    <th>Location</th>
                                    <th>Cost</th>
                                    <th>Quantity</th>
                                    <!--<th>Setup Cost</th>-->
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($num_stitches as $k => $location) { ?>
                                    <?php
                                    if (!$location)
                                        continue;
                                    $location_cost = nc_get_location_cost($location, $tot_items, ($printing_type_slug === $_printing_slugs['printable_vinyl'] ? 'pri' : ($printing_type_slug === $_printing_slugs['hats_embroidery'] ? 'hat' : 'sub')));
//                                    $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                    ?>
                                    <tr>
                                        <th><?php echo $location ?></th>
                                        <td>$<?php echo $location_cost ?></td>
                                        <td><?php echo $tot_items ?></td>
                                        <!--<td><?php echo "$" . $s_cost ?></td>-->
                                        <td>$<?php echo ($tot_items * $location_cost) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    <?php if ($printing_type_slug === $_printing_slugs['all_drinkware']) { ?>
                        <table class="table table-bordered" style="text-align:left">
                            <thead>
                                <tr>
                                    <th>Location</th>
                                    <th>Cost</th>
                                    <th>Quantity</th>
                                    <!--<th>Setup Cost</th>-->
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($num_stitches as $k => $stitches) { ?>
                                    <?php
                                    if (!$stitches)
                                        continue;
                                    $stitches_cost = nc_get_all_location_cost($stitches, $tot_items);
//                                    $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                    ?>
                                    <tr>
                                        <th><?php echo $stitches ?></th>
                                        <td>$<?php echo $stitches_cost ?></td>
                                        <td><?php echo $tot_items ?></td>
                                        <!--<td><?php echo "$" . $s_cost ?></td>-->
                                        <td>$<?php echo ($tot_items * $stitches_cost) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                </td>
            </tr>
            <?php if ($printing_type_slug !== $_printing_slugs['all_drinkware']) { ?>
                <tr>
                    <th>Personalization Cost</th>
                    <td><?php echo "$" . $personalization_cost ?></td>
                </tr>
            <?php } ?>
            <?php if ($vinyl_personalization && $printing_type_slug !== $_printing_slugs['all_drinkware']) { ?>
                <tr>
                    <td colspan="2">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Personalization</th>
                                    <th>Cost</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $vinyl_personalization ?></td>
                                    <td><?php echo "$" . $select_vinyl_price["price_{$l_vinyl_personalization}"] ?></td>
                                    <td><?php echo $tot_items ?></td>
                                    <td><?php echo "$" . $personalization_cost ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <th colspan="2">Total Cost of Products</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="table table-bordered" style="text-align:left">
                        <thead>
                            <tr>
                                <th class="left"><b>Size</b></th>
                                <th class="right"><b>Unit Price</b></th>
                                <th class="left"><b>Quantity</b></th>
                                <th class="right"><b>Total</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $avg_dec_cost = round(($decoration_cost + $setup_cost + $personalization_cost) / $tot_items, 2);
                            ?>
                            <?php foreach ($pricings['size'] as $s_id => $s_price) { ?>
                                <?php
                                $temp = get_term_by('term_id', $s_id, $_size_taxonomy);
                                $s_name = $temp->name;
                                $qty = $prod_quantity[$s_id] ? $prod_quantity[$s_id] : 0;
                                if (!$qty)
                                    continue;
                                $avg_prod_unit_price = ($color_price + $s_price + $avg_dec_cost);
                                ?>
                                <tr>
                                    <td class="left"><?php echo $s_name ?></td>
                                    <td class="center"><?php echo "$" . $avg_prod_unit_price ?></td>
                                    <td class="right"><?php echo $qty ?></td>
                                    <td class="right"><?php echo "$" . ($avg_prod_unit_price * $prod_quantity[$s_id]) ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <th>Quote Total</th>
                <td><?php echo "$" . $mega_cost ?></td>
            </tr>
        </tbody>
    </table>
    <?php
    echo ob_get_clean();
}

function nc_send_email_admin($id, $admin_email = '', $fullname, $phone) {
    if (!$id)
        return;

    if (!$admin_email)
        $admin_email = get_option('nc_admin_email');
    if (!$admin_email || !is_email($admin_email))
        return;

    //used values
    {
        $_post_type = "products";
        $_quote_post_type = "quotes";
        $_taxonomy = 'product_category';
        $_size_taxonomy = 'product_size';
        $_color_post_type = 'colors';
        $_printing_taxonomy = 'product_printing';
        $_printing_slugs = get_option('_nc_default_printings');
        $_slug_printings = array_flip($_printing_slugs);
    }


    //get product
    $prod_id = get_post_meta($id, 'nc_prod_id', true);
    $product = get_post($prod_id);

    //get data
    {
        //excerpt
        $excerpt = nc_get_the_excerpt($prod_id);

        //get pricings
        $pricings = apply_filters('nc_get_product_pricings', get_post_meta($prod_id, '_nc_pricings', true));

        //get printings
        {
            $product_printing_ids = get_post_meta($prod_id, '_nc_printings', true);
            if (is_array($product_printing_ids) && !empty($product_printing_ids)) {
                $product_printings = get_terms($_printing_taxonomy, array(
                    'hide_empty' => false,
                    'fields' => 'id=>name',
                    'include' => $product_printing_ids,
                ));
            }
        }

        //get product sizes
        {
            $product_size_ids = isset($pricings['size']) && is_array($pricings['size']) && !empty($pricings['size']) ? array_keys($pricings['size']) : array();
            if (!empty($product_size_ids)) {
                $product_sizes = get_terms($_size_taxonomy, array(
                    'hide_empty' => false,
                    'fields' => 'id=>name',
                    'include' => $product_size_ids,
                ));
            }
        }

        //get product colors
        {
            $product_color_ids = isset($pricings['color']) && is_array($pricings['color']) && !empty($pricings['color']) ? array_keys($pricings['color']) : array();
            if (!empty($product_color_ids)) {
                $product_colors = get_posts(array(
                    'post_type' => $_color_post_type,
                    'include' => $product_color_ids,
                ));

                //color id as key
                {
                    $temp = array();
                    foreach ($product_colors as $product_color)
                        $temp[$product_color->ID] = $product_color;
                    $product_colors = $temp;
                }
            }
        }

        //printing_type
        {
            $printing_type_id = get_post_meta($id, 'nc_printing_type_id', true);
            $product_printing = $product_printings[$printing_type_id];
            $printing_type_slug = get_term($printing_type_id, $_printing_taxonomy)->slug;
            $printing_type_short = substr($printing_type_slug, 0, 3);
        }

        //printing costs
        {
            $printing_type_setup_cost = get_option("nc_{$printing_type_short}_setup_cost", 0);
            $printing_type_prev_setup_cost = get_option("nc_{$printing_type_short}_prev_setup_cost", 0);
        }

        //color_id
        $color_id = get_post_meta($id, 'nc_color_id', true);
        $is_dark_color = get_post_meta($color_id, 'is_dark_color', true);

        //size & quantity
        $prod_quantity = get_post_meta($id, 'nc_size_quantity', true);

        //num_stitches
        $num_stitches = get_post_meta($id, 'nc_print_location', true);
        if (!is_array($num_stitches) || empty($num_stitches)) {
            $num_stitches = get_post_meta($id, 'nc_num_stitches', true);
        }

        //num_colors_art
        $num_colors_art = get_post_meta($id, 'nc_num_colors_art', true);

        //nc_size_quantity
        $size_quantity = get_post_meta($id, 'nc_size_quantity', true);

        //nc_is_previous_order
        $is_previous_order = get_post_meta($id, 'nc_is_previous_order', true);

        //nc_vinyl_personalization
        $vinyl_personalization = get_post_meta($id, 'nc_vinyl_personalization', true);

        //nc_is_white_color
        $nc_is_white_color = get_post_meta($id, 'nc_is_white_color', true);

        //nc_white_color_print_cost
        $nc_white_color_print_cost = get_option('nc_scr_white_color_cost');

        //nc_scr_pricing
        $nc_scr_pricing = get_option('nc_' . $printing_type_short . '_pricing', array());
        $scr_pricing_cat = get_post_meta($prod_id, '_nc_' . $printing_type_short . '_pricing_cat', true);

        //mega cost
        $mega_cost = 0;
        $tot_items = array_sum($size_quantity);
        $GLOBALS['tot_items'] = $tot_items;
        //calculate various prices
        {
            //setup string and cost
            {
                $setup_cost = 0;
                $setup_string = '';
                $i = 0;
                if (is_array($num_stitches)) {
                    foreach ($num_stitches as $k => $location) {
                        $location = trim($location);
                        if (!$location)
                            continue;
                        $cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                        $setup_string .= "$$cost + ";
                        $setup_cost += $cost;
                        $i++;
                    }
                    $setup_string = substr($setup_string, 0, -3);
                }
                $setup_string = ($i > 1 ? $setup_string . " = $" . $setup_cost : '$' . $setup_cost);
                $mega_cost+=$setup_cost;
            }

            //decoration cost
            {
                $decoration_cost = 0;
                if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['screen_printing'] || $printing_type_slug === $_printing_slugs['2screen_printing'])) {
                    foreach ($num_stitches as $k => $location) {
                        $location = trim($location);
                        if (!$location)
                            continue;
                        $colors = $num_colors_art[$k];
                        if ($is_dark_color && !$nc_is_white_color[$k])
                            $colors++;
                        $cost = nc_get_scr_pri_cost($nc_scr_pricing, $scr_pricing_cat, $tot_items);
                        if ($cost)
                            $cost = $cost * $colors;
                        $decoration_cost += ($cost * $tot_items);
                    }
                }
                if (is_array($num_stitches) && $printing_type_slug === $_printing_slugs['embroidery']) {
                    foreach ($num_stitches as $k => $stitches) {
                        if (!$stitches)
                            continue;
                        $stitches_cost = nc_get_stitches_cost($stitches, $tot_items);
                        $decoration_cost += ($stitches_cost * $tot_items);
                    }
                }
                if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['printable_vinyl'] || $printing_type_slug === $_printing_slugs['sublimation'] || $printing_type_slug === $_printing_slugs['hats_embroidery'])) {
                    foreach ($num_stitches as $k => $location) {
                        if (!$location)
                            continue;
                        $location_cost = nc_get_location_cost($location, $tot_items, ($printing_type_slug === $_printing_slugs['printable_vinyl'] ? 'pri' : ($printing_type_slug === $_printing_slugs['hats_embroidery'] ? 'hat' : 'sub')));
                        $decoration_cost += ($location_cost * $tot_items);
                    }
                }
                if (is_array($num_stitches) && $printing_type_slug === $_printing_slugs['all_drinkware']) {
                    foreach ($num_stitches as $k => $location) {
                        if (!$location)
                            continue;
                        $location_cost = nc_get_all_location_cost($location, $tot_items);
                        $decoration_cost += ($location_cost * $tot_items);
                    }
                }
                $mega_cost += $decoration_cost;
            }

            //personalization cost
            {
                $personalization_cost = 0;
                if ($vinyl_personalization && $printing_type_slug !== $_printing_slugs['all_drinkware']) {
                    $vinyl_prices = get_option('nc_pre_pricing');
                    if (is_array($vinyl_prices)) {
                        $output = array_filter($vinyl_prices, 'nc_anony_func_2');
                        if (empty($output))
                            $output = $vinyl_prices;
                        $select_vinyl_price = array_shift($output);
                        if (is_array($select_vinyl_price)) {
                            $l_vinyl_personalization = strtolower($vinyl_personalization);
                            if (isset($select_vinyl_price["price_{$l_vinyl_personalization}"])) {
                                $personalization_cost = ($select_vinyl_price["price_{$l_vinyl_personalization}"] * $tot_items);
                            }
                        }
                    }
                }
                $mega_cost += $personalization_cost;
            }

            //product total cost
            {
                $size_cost = 0;
                foreach ($prod_quantity as $s_id => $s_qty) {
                    $size_cost += ($s_qty * $pricings['size'][$s_id]);
                }
                $color_price = isset($pricings['color'][$color_id]) ? $pricings['color'][$color_id] : 0;
                $color_mega_cost = $color_price * $tot_items;
                $prod_tot_cost = $size_cost + $color_mega_cost;
                $mega_cost += $prod_tot_cost;
            }
        }
    }

    //begin page execution
    ob_start();
    ?>
    <div>
        <div>
            <table border="0" cellpadding="5" cellspacing="5">
                <tbody>
                    <tr>
                        <th>Full Name</th>
                        <td><?php echo $fullname ?></td>
                    </tr>
                    <tr>
                        <th>Phone Number</th>
                        <td><?php echo $phone ?></td>
                    </tr>
                    <tr>
                        <th>Product</th>
                        <td><?php echo get_the_title($prod_id) ?></td>
                    </tr>
                    <tr>
                        <th>Color</th>
                        <td><?php echo $product_colors[$color_id]->post_title ?></td>
                    </tr>
                    <tr>
                        <th>Decoration Method</th>
                        <td><?php echo $product_printing ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="clear: both"></div>
        <table border="0" cellspacing="5" cellpadding="5" style=" width:45%; float:left;">
            <tbody>
                <tr>
                    <td valign="top">
                        <table border="0" cellspacing="5" cellpadding="5">
                            <tbody>
                                <tr>
                                    <th>Total Items</th>
                                    <td><?php echo $tot_items ?></td>
                                </tr>
                                <tr>
                                    <th>Setup Cost</th>
                                    <td><?php echo $setup_string ?></td>
                                </tr>
                                <tr>
                                    <th>Decoration Cost</th>
                                    <td><?php echo "$" . $decoration_cost ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <?php if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['screen_printing'] || $printing_type_slug === $_printing_slugs['2screen_printing'])) { ?>
                                            <table class="table table-bordered" style="text-align:left">
                                                <tbody>
                                                    <tr>
                                                        <th class="left"><b>Location</b></th>
                                                        <th class="left"><b>Number of Colors</b></th>
                                                        <th class="right"><b>Unit Price</b></th>
                                                        <th class="left"><b>Quantity</b></th>
                                                        <!--<th class="left"><b>Setup Cost</b></th>-->
                                                        <th class="left"><b>Total</b></th>
                                                    </tr>
                                                    <?php foreach ($num_stitches as $k => $location) { ?>
                                                        <?php
                                                        $location = trim($location);
                                                        if (!$location)
                                                            continue;
                                                        $colors = $num_colors_art[$k];
                                                        if ($is_dark_color && !$nc_is_white_color[$k])
                                                            $colors++;
//                                                        $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                                        $cost = nc_get_scr_pri_cost($nc_scr_pricing, $scr_pricing_cat, $tot_items);
                                                        ?>
                                                        <tr>
                                                            <th><?= $location ?></th>
                                                            <td class="left"><?= $colors ?></td>
                                                            <td class="right"><?php echo $cost ?></td>
                                                            <td><?php echo $tot_items ?></td>
                                                            <!--<td><?php echo "$" . $s_cost      ?></td>-->
                                                            <td><?php echo "$" . (($cost * $tot_items * $colors)) ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        <?php } ?>
                                        <?php if (is_array($num_stitches) && $printing_type_slug === $_printing_slugs['embroidery']) { ?>
                                            <table class="table table-bordered" style="text-align:left">
                                                <thead>
                                                    <tr>
                                                        <th>Stitches</th>
                                                        <th>Cost</th>
                                                        <th>Quantity</th>
                                                        <!--<th>Setup Cost</th>-->
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($num_stitches as $k => $stitches) { ?>
                                                        <?php
                                                        if (!$stitches)
                                                            continue;
                                                        $stitches_cost = nc_get_stitches_cost($stitches, $tot_items);
//                                                        $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                                        ?>
                                                        <tr>
                                                            <th><?php echo $stitches ?></th>
                                                            <td>$<?php echo $stitches_cost ?></td>
                                                            <td><?php echo $tot_items ?></td>
                                                            <!--<td><?php echo "$" . $s_cost ?></td>-->
                                                            <td>$<?php echo ($tot_items * $stitches_cost) ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        <?php } ?>
                                        <?php if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['printable_vinyl'] || $printing_type_slug === $_printing_slugs['sublimation'] || $printing_type_slug === $_printing_slugs['hats_embroidery'])) { ?>
                                            <table class="table table-bordered" style="text-align:left">
                                                <thead>
                                                    <tr>
                                                        <th>Location</th>
                                                        <th>Cost</th>
                                                        <th>Quantity</th>
                                                        <!--<th>Setup Cost</th>-->
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($num_stitches as $k => $location) { ?>
                                                        <?php
                                                        if (!$location)
                                                            continue;
                                                        $location_cost = nc_get_location_cost($location, $tot_items, ($printing_type_slug === $_printing_slugs['printable_vinyl'] ? 'pri' : ($printing_type_slug === $_printing_slugs['hats_embroidery'] ? 'hat' : 'sub')));
//                                                        $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                                        ?>
                                                        <tr>
                                                            <th><?php echo $location ?></th>
                                                            <td>$<?php echo $location_cost ?></td>
                                                            <td><?php echo $tot_items ?></td>
                                                            <!--<td><?php echo "$" . $s_cost     ?></td>-->
                                                            <td>$<?php echo ($tot_items * $location_cost) ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        <?php } ?>
                                        <?php if ($printing_type_slug === $_printing_slugs['all_drinkware']) { ?>
                                            <table class="table table-bordered" style="text-align:left">
                                                <thead>
                                                    <tr>
                                                        <th>Location</th>
                                                        <th>Cost</th>
                                                        <th>Quantity</th>
                                                        <!--<th>Setup Cost</th>-->
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($num_stitches as $k => $stitches) { ?>
                                                        <?php
                                                        if (!$stitches)
                                                            continue;
                                                        $stitches_cost = nc_get_all_location_cost($stitches, $tot_items);
//                                                        $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                                        ?>
                                                        <tr>
                                                            <th><?php echo $stitches ?></th>
                                                            <td>$<?php echo $stitches_cost ?></td>
                                                            <td><?php echo $tot_items ?></td>
                                                            <!--<td><?php echo "$" . $s_cost ?></td>-->
                                                            <td>$<?php echo ($tot_items * $stitches_cost) ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php if ($printing_type_slug !== $_printing_slugs['all_drinkware']) { ?>
                                    <tr>
                                        <th>Personalization Cost</th>
                                        <td><?php echo "$" . $personalization_cost ?></td>
                                    </tr>
                                <?php } ?>
                                <?php if ($vinyl_personalization && $printing_type_slug !== $_printing_slugs['all_drinkware']) { ?>
                                    <tr>
                                        <td colspan="2">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Personalization</th>
                                                        <th>Cost</th>
                                                        <th>Quantity</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><?php echo $vinyl_personalization ?></td>
                                                        <td><?php echo "$" . $select_vinyl_price["price_{$l_vinyl_personalization}"] ?></td>
                                                        <td><?php echo $tot_items ?></td>
                                                        <td><?php echo "$" . $personalization_cost ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <th colspan="2">Total Cost of Products</th>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table class="table table-bordered" style="text-align:left">
                                            <thead>
                                                <tr>
                                                    <th class="left"><b>Size</b></th>
                                                    <th class="right"><b>Unit Price</b></th>
                                                    <th class="left"><b>Quantity</b></th>
                                                    <th class="right"><b>Total</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $avg_dec_cost = round(($decoration_cost + $setup_cost + $personalization_cost) / $tot_items, 2);
                                                ?>
                                                <?php foreach ($pricings['size'] as $s_id => $s_price) { ?>
                                                    <?php
                                                    $temp = get_term_by('term_id', $s_id, $_size_taxonomy);
                                                    $s_name = $temp->name;
                                                    $qty = $prod_quantity[$s_id] ? $prod_quantity[$s_id] : 0;
                                                    if (!$qty)
                                                        continue;
                                                    $avg_prod_unit_price = ($color_price + $s_price + $avg_dec_cost);
                                                    ?>
                                                    <tr>
                                                        <td class="left"><?php echo $s_name ?></td>
                                                        <td class="center"><?php echo "$" . $avg_prod_unit_price ?></td>
                                                        <td class="right"><?php echo $qty ?></td>
                                                        <td class="right"><?php echo "$" . ($avg_prod_unit_price * $prod_quantity[$s_id]) ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Quote Total</th>
                                    <td><?php echo "$" . $mega_cost ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="clear: both"></div>
    <hr />
    <div class="phone">
        <div class="top-border">This is only a quote. To finalize order please email or call to confirm.</div>
        <h5>Email: <a href="mailto:order@nclogowear.com">order@nclogowear.com</a></h5>
        <h5>Call us: 919-821-4646 </h5>
    </div>
    <?php
    $content = ob_get_clean();
    add_filter('wp_mail_content_type', 'nc_wp_mail_content_type');
    add_filter('wp_mail_from', 'nc_wp_mail_from');
    wp_mail($admin_email, 'New Quote at ' . get_site_url(), $content);
    remove_filter('wp_mail_content_type', 'nc_wp_mail_content_type');
    remove_filter('wp_mail_from', 'nc_wp_mail_from');
}

function nc_send_user_quote_email() {
    extract($_POST);
    update_post_meta($quote_id, '_nc_user_details', array(
        'full_name' => $full_name,
        'email' => $email,
        'phone' => $phone,
    ));
    nc_send_email_admin($quote_id, '', $full_name, $phone);
    nc_send_email_admin($quote_id, $email, $full_name, $phone);
    die;
}

function nc_add_tax_image() {
    if (get_bloginfo('version') >= 3.5)
        wp_enqueue_media();
    else {
        wp_enqueue_style('thickbox');
        wp_enqueue_script('thickbox');
    }

    echo '<div class="form-field">
		<label for="taxonomy_image">' . __('Image', 'categories-images') . '</label>
		<input type="text" name="taxonomy_image" id="taxonomy_image" value="" />
		<br/>
		<button class="z_upload_image_button button">' . __('Upload/Add image', 'categories-images') . '</button>
	</div>' . nc_script();
}

function nc_script() {
    return '<script type="text/javascript">
	    jQuery(document).ready(function($) {
			var wordpress_ver = "' . get_bloginfo("version") . '", upload_button;
			$(".z_upload_image_button").click(function(event) {
				upload_button = $(this);
				var frame;
				if (wordpress_ver >= "3.5") {
					event.preventDefault();
					if (frame) {
						frame.open();
						return;
					}
					frame = wp.media();
					frame.on( "select", function() {
						// Grab the selected attachment.
						var attachment = frame.state().get("selection").first();
						frame.close();
						if (upload_button.parent().prev().children().hasClass("tax_list")) {
							upload_button.parent().prev().children().val(attachment.attributes.url);
							upload_button.parent().prev().prev().children().attr("src", attachment.attributes.url);
						}
						else
							$("#taxonomy_image").val(attachment.attributes.url);
					});
					frame.open();
				}
				else {
					tb_show("", "media-upload.php?type=image&amp;TB_iframe=true");
					return false;
				}
			});
			
			$(".z_remove_image_button").click(function() {
				$(".taxonomy-image").attr("src", "' . ADV_IMAGE_PLACEHOLDER . '");
				$("#taxonomy_image").val("");
				$(this).parent().siblings(".title").children("img").attr("src","' . ADV_IMAGE_PLACEHOLDER . '");
				$(".inline-edit-col :input[name=\'taxonomy_image\']").val("");
				return false;
			});
			
			if (wordpress_ver < "3.5") {
				window.send_to_editor = function(html) {
					imgurl = $("img",html).attr("src");
					if (upload_button.parent().prev().children().hasClass("tax_list")) {
						upload_button.parent().prev().children().val(imgurl);
						upload_button.parent().prev().prev().children().attr("src", imgurl);
					}
					else
						$("#taxonomy_image").val(imgurl);
					tb_remove();
				}
			}
			
			$(".editinline").click(function() {	
			    var tax_id = $(this).parents("tr").attr("id").substr(4);
			    var thumb = $("#tag-"+tax_id+" .thumb img").attr("src");

				if (thumb != "' . ADV_IMAGE_PLACEHOLDER . '") {
					$(".inline-edit-col :input[name=\'taxonomy_image\']").val(thumb);
				} else {
					$(".inline-edit-col :input[name=\'taxonomy_image\']").val("");
				}
				
				$(".inline-edit-col .title img").attr("src",thumb);
			});
	    });
	</script>';
}

function nc_edit_texonomy_field($taxonomy) {
    if (get_bloginfo('version') >= 3.5)
        wp_enqueue_media();
    else {
        wp_enqueue_style('thickbox');
        wp_enqueue_script('thickbox');
    }

    if (nc_taxonomy_image_url($taxonomy->term_id, NULL, TRUE) == ADV_IMAGE_PLACEHOLDER)
        $image_url = "";
    else
        $image_url = nc_taxonomy_image_url($taxonomy->term_id, NULL, TRUE);
    echo '<tr class="form-field">
		<th scope="row" valign="top"><label for="taxonomy_image">' . __('Image', 'categories-images') . '</label></th>
		<td><img class="taxonomy-image" src="' . nc_taxonomy_image_url($taxonomy->term_id, 'medium', TRUE) . '"/><br/><input type="text" name="taxonomy_image" id="taxonomy_image" value="' . $image_url . '" /><br />
		<button class="z_upload_image_button button">' . __('Upload/Add image', 'categories-images') . '</button>
		<button class="z_remove_image_button button">' . __('Remove image', 'categories-images') . '</button>
		</td>
	</tr>' . nc_script();
}

function nc_taxonomy_image_url($term_id = NULL, $size = 'full', $return_placeholder = FALSE) {
    if (!$term_id) {
        if (is_category())
            $term_id = get_query_var('cat');
        elseif (is_tax()) {
            $current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
            $term_id = $current_term->term_id;
        }
    }

    $taxonomy_image_url = get_option('nc_taxonomy_image' . $term_id);
    if (!empty($taxonomy_image_url)) {
        $attachment_id = nc_get_attachment_id_by_url($taxonomy_image_url);
        if (!empty($attachment_id)) {
            $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
            $taxonomy_image_url = $taxonomy_image_url[0];
        }
    }

    if ($return_placeholder)
        return ($taxonomy_image_url != '') ? $taxonomy_image_url : ADV_IMAGE_PLACEHOLDER;
    else
        return $taxonomy_image_url;
}

function nc_taxonomy_columns($columns) {
    $new_columns = array();
    $new_columns['cb'] = $columns['cb'];
    $new_columns['thumb'] = __('Image', 'categories-images');

    unset($columns['cb']);

    return array_merge($new_columns, $columns);
}

function nc_taxonomy_column($columns, $column, $id) {
    if ($column == 'thumb')
        $columns = '<span><img src="' . nc_taxonomy_image_url($id, 'thumbnail', TRUE) . '" alt="' . __('Thumbnail', 'categories-images') . '" class="wp-post-image" /></span>';
    return $columns;
}

function nc_save_taxonomy_image($term_id) {
    if (isset($_POST['taxonomy_image']))
        update_option('nc_taxonomy_image' . $term_id, $_POST['taxonomy_image'], NULL);
}

function nc_add_style() {
    echo '<style type="text/css" media="screen">
		th.column-thumb {width:60px;}
		.form-field img.taxonomy-image {border:1px solid #eee;max-width:300px;max-height:300px;}
		.inline-edit-row fieldset .thumb label span.title {width:48px;height:48px;border:1px solid #eee;display:inline-block;}
		.column-thumb span {width:48px;height:48px;border:1px solid #eee;display:inline-block;}
		.inline-edit-row fieldset .thumb img,.column-thumb img {width:48px;height:48px;}
	</style>';
}

function nc_quick_edit_custom_box($column_name, $screen, $name) {
    if ($column_name == 'thumb')
        echo '<fieldset>
		<div class="thumb inline-edit-col">
			<label>
				<span class="title"><img src="" alt="Thumbnail"/></span>
				<span class="input-text-wrap"><input type="text" name="taxonomy_image" value="" class="tax_list" /></span>
				<span class="input-text-wrap">
					<button class="z_upload_image_button button">' . __('Upload/Add image', 'categories-images') . '</button>
					<button class="z_remove_image_button button">' . __('Remove image', 'categories-images') . '</button>
				</span>
			</label>
		</div>
	</fieldset>';
}

function nc_change_insert_button_text($safe_text, $text) {
    return str_replace("Insert into Post", "Use this image", $text);
}

function nc_get_attachment_id_by_url($image_src) {
    global $wpdb;
    $query = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid = %s", $image_src);
    $id = $wpdb->get_var($query);
    return (!empty($id)) ? $id : NULL;
}

function nc_wp_mail_content_type() {
    return 'text/html';
}

function nc_wp_mail_from() {
    $sitename = strtolower($_SERVER['SERVER_NAME']);
    if (substr($sitename, 0, 4) == 'www.') {
        $sitename = substr($sitename, 4);
    }
    $from_email = 'noreply@' . $sitename;
    return $from_email;
}

function nc_delete_pricing() {
    extract($_POST);

    //set option name
    $option_name = 'nc_' . $type . '_pricing';

    //get prev option
    $nc_pricing = get_option($option_name, array());
    $ret = 0;
    if (isset($nc_pricing[$pricing_id])) {
        unset($nc_pricing[$pricing_id]);
        $ret = update_option($option_name, $nc_pricing);
    }
    echo $ret;
    die;
}

function nc_update_pricing() {
    extract($_POST);

    //set option name
    $option_name = 'nc_' . $type . '_pricing';

    //get prev option
    $nc_pricing = get_option($option_name, array());
    $ret = 0;
    if ($type == 'emb') {
        $nc_pricing[$pricing_id] = array(
            'stitches' => $stitches_from . '-' . $stitches_to,
            'pieces' => $pieces_from . '-' . $pieces_to,
            'pricing' => $pricing,
        );
        $ret = update_option($option_name, $nc_pricing);
    }
    if ($type == 'scr' || $type == '2sc') {
        $nc_pricing[$pricing_id] = array(
            'pricing_category' => $pricing_category,
            'colors_from' => $colors_from,
            'colors_to' => $colors_to,
            'pricing' => $pricing,
        );
        $ret = update_option($option_name, $nc_pricing);
    }
    if ($type == 'pri' || $type == 'sub' || $type == 'hat') {
        $nc_pricing[$pricing_id] = array(
            'location' => $location,
            'to_quantity' => $to_quantity,
            'price' => $price
        );
        $ret = update_option($option_name, $nc_pricing);
    }
    if ($type == 'pre') {
        $nc_pricing[$pricing_id] = array(
            'to_quantity' => $to_quantity,
            'price_numbers' => $price_numbers,
            'price_names' => $price_names,
            'price_both' => $price_both,
        );
        $ret = update_option($option_name, $nc_pricing);
    }
    if ($type == 'all') {
        $nc_pricing[$pricing_id] = array(
            'to_quantity' => $to_quantity,
            'price_1_side' => $price_1_side,
            'price_2_sides' => $price_2_sides,
        );
        $ret = update_option($option_name, $nc_pricing);
    }
    echo $ret;
    die;
}

function nc_get_stitches_cost($stitches, $tot_items) {
    $cost = 0;
    $emb_pricing = get_option('nc_emb_pricing');
    foreach ($emb_pricing as $emb_price) {
        if ($emb_price['stitches'] == $stitches) {
            $pieces_arr = explode('-', $emb_price['pieces']);
            $pieces_lb = $pieces_arr[0];
            $pieces_ub = $pieces_arr[1];
            if ($pieces_lb <= $tot_items && $tot_items <= $pieces_ub) {
                return $emb_price['pricing'];
            }
        }
    }
    return $cost;
}

function nc_get_location_cost($location, $tot_items, $type) {
    $cost = 0;
    $pricings = get_option('nc_' . $type . '_pricing');
    $GLOBALS['tot_items'] = $tot_items;
    $GLOBALS['location'] = $location;
    $output = array_filter($pricings, 'nc_anony_func_3');
    if (empty($output))
        return $cost;
    $select_pricing = array_shift($output);
    if (is_array($select_pricing))
        $cost = $select_pricing['price'];
    return $cost;
}

function nc_get_all_location_cost($location, $tot_items) {
    $cost = 0;
    if ($location)
        $location = trim(strtolower(str_replace(' ', '_', $location)));
    $pricings = get_option('nc_all_pricing');
    $GLOBALS['tot_items'] = $tot_items;
    $GLOBALS['location'] = $location;
    $output = array_filter($pricings, 'nc_anony_func_2');
    if (empty($output))
        return $cost;
    $select_pricing = array_shift($output);
    if (is_array($select_pricing))
        $cost = $select_pricing['price_' . $location];
    return $cost;
}

function nc_product_size_add_form_fields() {
    ?>
    <div class="form-field">
        <label for="term_meta[sort_order]"><?php _e('Order'); ?></label>
        <input type="number" name="term_meta[sort_order]" id="term_meta[sort_order]" value="0">
        <p class="description"><?php _e('Order to show the size on front'); ?></p>
    </div>
    <?php
}

function nc_product_size_edit_form_fields($term) {
    $t_id = $term->term_id;
    $term_meta = get_option("taxonomy_$t_id");
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="term_meta[sort_order]"><?php _e('Order'); ?></label></th>
        <td>
            <input type="number" name="term_meta[sort_order]" id="term_meta[sort_order]" value="<?php echo esc_attr($term_meta['sort_order']) ? esc_attr($term_meta['sort_order']) : 0; ?>">
            <p class="description"><?php _e('Order to show the size on front'); ?></p>
        </td>
    </tr>
    <?php
}

function nc_save_product_size_meta($t_id) {
    if (isset($_POST['term_meta'])) {
        $term_meta = get_option("taxonomy_$t_id");
        $cat_keys = array_keys($_POST['term_meta']);
        foreach ($cat_keys as $key) {
            if (isset($_POST['term_meta'][$key])) {
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        // Save the option array.
        update_option("taxonomy_$t_id", $term_meta);
    }
}

function nc_add_product_size_columns($e) {
    $e['size_order'] = "Order";
    return $e;
}

function nc_add_product_size_column_content($content, $column_name, $t_id) {
    $term_meta = get_option("taxonomy_$t_id");
    return is_array($term_meta) && isset($term_meta['sort_order']) ? $term_meta['sort_order'] : '-';
}

function nc_apply_size_order($terms, $taxonomies) {
    if (in_array('product_size', $taxonomies)) {
        $temp = $temp2 = array();
        foreach ($terms as $term_id => $term_name) {
            $term_meta = get_option("taxonomy_$term_id");
            if (is_array($term_meta) && isset($term_meta['sort_order']) && is_numeric($term_meta['sort_order']))
                $temp[$term_id] = $term_meta['sort_order'];
            else
                $temp2[$term_id] = 0;
        }
        asort($temp);
        $temp = $temp + $temp2;
        foreach ($temp as $term_id => $order) {
            $temp[$term_id] = $terms[$term_id];
        }
        $terms = $temp;
    }
    return $terms;
}

function nc_edit_form_after_title() {
    global $post;
    $template_file = get_post_meta($post->ID, '_wp_page_template', true);
    echo "<br /><br /><br /><br /><center><h3>Page Editor is disabled for Template - <i style=\"font-weight: normal\">" . $this->templates[$template_file] . "</i></h3></center>";
}

function nc_get_scr_pri_cost($nc_scr_pricing, $scr_pricing_cat, $tot_items) {
    $cost = 0;
    if (!empty($nc_scr_pricing)) {
        foreach ($nc_scr_pricing as $scr_pricing) {
            if ($scr_pricing['pricing_category'] == $scr_pricing_cat && $tot_items >= $scr_pricing['colors_from'] && $tot_items <= $scr_pricing['colors_to']) {
                $cost = $scr_pricing['pricing'];
                break;
            }
        }
    }
    return $cost;
}

function nc_sort_product_sizes($pricings) {
    if (is_array($pricings)) {
        if (isset($pricings['size'])) {
            $sizes = &$pricings['size'];
            $temp = $temp2 = array();
            foreach ($sizes as $size_id => $e) {
                $s_meta = get_option("taxonomy_$size_id");
                if (is_array($s_meta) && isset($s_meta['sort_order']))
                    $temp[$size_id] = $s_meta['sort_order'];
                else
                    $temp2[$size_id] = 0;
            }
            if (is_array($temp)) {
                asort($temp);
                $temp = $temp + $temp2;
                $temp2 = array();
                foreach ($temp as $s_id => $e) {
                    $temp2[$s_id] = $sizes[$size_id];
                }
                $sizes = $temp2;
            }
        }
    }
    return $pricings;
}

//anonymous functions
{

    function nc_anony_func_1($price) {
        return $price || $price === '0' ? true : false;
    }

    function nc_anony_func_2($vinyl_price) {
        global $tot_items;
        return ($vinyl_price['to_quantity'] >= $tot_items);
    }

    function nc_anony_func_3($pricing) {
        global $tot_items, $location;
        return ($location == $pricing['location'] && $pricing['to_quantity'] >= $tot_items);
    }

}
?>