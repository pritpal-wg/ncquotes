<?php

// Creating the widget 
class nc_cat_widget extends WP_Widget {

    protected $domain = 'ncquotes';

    function __construct() {
        parent::__construct(
// Base ID of your widget
                'nc_cat_widget',
// Widget name will appear in UI
                __('NC Quotes: Categories', $this->domain),
// Widget description
                array('description' => __('Widget for showing Top Level Category Links', $this->domain),)
        );
    }

// Creating widget front-end
// This is where the action happens
    public function widget($args, $instance) {

        $single_quote_product_page_id = nc_PageTemplater::getPage('single_quote_product', true);
        if (!$single_quote_product_page_id)
            return;
        $quote_final_page_id = nc_PageTemplater::getPage('quote_final', true);
        if (!$quote_final_page_id)
            return;
        //get the page for showing sub categories
        {
            $page_id = nc_PageTemplater::getPage('product_category', true);
            if (!$page_id)
                return;
            $page_url = get_site_url() . "/?p=$page_id";
        }

        //get top level categories
        {
            $categories = get_terms('product_category', array(
                'hide_empty' => false,
                'fields' => 'id=>name',
                'parent' => '0',
            ));
            if (empty($categories))
                return;
        }

        $title = apply_filters('widget_title', $instance['title']);
// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
        ?>
        <ul>
            <?php foreach ($categories as $cat_id => $category_name) { ?>
                <li><a href="<?php echo $page_url . '&cat_id=' . $cat_id ?>"><?= $category_name ?></a></li>
            <?php } ?>
        </ul>
        <?php
        echo $args['after_widget'];
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Quotes Categories', $this->domain);
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

// Class wpb_widget ends here
// Register and load the widget
function nc_cat_widget() {
    register_widget('nc_cat_widget');
}

add_action('widgets_init', 'nc_cat_widget');
