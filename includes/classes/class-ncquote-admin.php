<?php
/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 * @author     Your Name <email@example.com>
 */
class Ncquotes_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @var      string    $plugin_name       The name of this plugin.
     * @var      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        add_action('admin_menu', array(&$this, 'register_my_custom_menu_page'));

        // Hook into the 'init' action Current Menu
        add_action('admin_head', array(&$this, 'cwebc_active_current_menu'));

        //Hide default Riders Post Type Menu Item from Admin Menu                
        add_action('admin_menu', array(&$this, 'cwebc_remove_weather_menu_items'));

        add_action('admin_menu', array(&$this, 'cwebc_remove_product_menu_items'));

        add_action('admin_menu', array(&$this, 'cwebc_remove_color_menu_items'));

        add_action('admin_menu', array(&$this, 'cwebc_remove_category_menu_items'));


        /* Add Custom Column for codes post */
        add_filter('manage_codes_posts_columns', 'wpse188743_codes_cpt_columns');

        function wpse188743_codes_cpt_columns($columns) {
            unset($columns['date']);
            $new_columns = array(
                'cityname' => __('City', 'weather'),
                'sortcode' => __('Shortcode', 'weather'),
                'date' => __('Date', 'weather')
            );

            return array_merge($columns, $new_columns);
        }

        add_action('manage_codes_posts_custom_column', 'my_manage_codes_columns', 10, 2);

        function my_manage_codes_columns($column, $post_id) {

            global $citiesArray;
            switch ($column) {
                case 'cityname' :
                    $city_name = get_post_meta($post_id, 'Location', true);
                    if (!empty($city_name)):
                        echo $citiesArray[$city_name]['name'];
                    endif;
                    break;
                case 'sortcode' :
                    $city_code = get_post_meta($post_id, 'citySortCode', true);
                    if (!empty($city_code)):
                        echo "[weather id='" . $city_code . "']";
                    endif;
                    break;
            }
        }

    }

    /**
     * Register the stylesheets for the Dashboard.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Plugin_Name_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Plugin_Name_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name, plugin_dir_url(dirname(dirname(__FILE__))) . 'admin/css/ncquote-admin.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name . '-icon', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/css/ncquote-icons.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name . '-easyselect', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/css/jquery.easyselect.min.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the dashboard.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Plugin_Name_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Plugin_Name_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->plugin_name . '-mousewheel', plugin_dir_url(dirname(dirname(__FILE__))) . 'admin/js/jscolor.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . '-easyselect', plugin_dir_url(dirname(dirname(__FILE__))) . 'admin/js/jquery.easyselect.min.js', array('jquery'), $this->version, false);
//        wp_enqueue_script($this->plugin_name . '-jquery1', plugin_dir_url(dirname(dirname(__FILE__))) . 'admin/js/jquery-1.10.1.min.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . '-easyselect', plugin_dir_url(dirname(dirname(__FILE__))) . 'admin/js/easy_select_custom.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . '-customjs', plugin_dir_url(dirname(dirname(__FILE__))) . 'admin/js/custom_jquery.js', array('jquery'), $this->version, false);
    }

    function register_my_custom_menu_page() {
        global $submenu;
        add_menu_page(__('Weather', 'weather'), __('LW-Quote', 'Ncquotes'), 'read', 'settings', array(&$this, 'manage_settings'), plugins_url('ncquotes/admin/img/icon.png'));
        add_submenu_page('settings', __('Dashboard', 'Ncquotes'), __('Dashboard', 'Ncquotes'), 'read', 'Dashboard', array(&$this, 'manage_dashboard'));
        add_submenu_page('settings', __('Products', 'Ncquotes'), __('Products', 'Ncquotes'), 'read', 'Products', array(&$this, 'manage_products'));
        add_submenu_page('settings', __('Category', 'Ncquotes'), __('Category', 'Ncquotes'), 'read', 'Category', array(&$this, 'manage_category'));
        add_submenu_page('settings', __('Colors', 'Ncquotes'), __('Colors', 'Ncquotes'), 'read', 'Colors', array(&$this, 'manage_colors'));
        add_submenu_page('settings', __('Size', 'Ncquotes'), __('Size', 'Ncquotes'), 'read', 'Size', array(&$this, 'manage_size'));
        add_submenu_page('settings', __('Printing', 'Ncquotes'), __('Printing', 'Ncquotes'), 'read', 'Printing', array(&$this, 'manage_printing'));
        add_submenu_page('settings', __('Quotes', 'Ncquotes'), __('Quotes', 'Ncquotes'), 'read', 'Quotes', array(&$this, 'manage_quotes'));
        add_submenu_page('settings', __('Settings', 'Ncquotes'), __('Settings', 'Ncquotes'), 'read', 'settings', array(&$this, 'manage_settings'));
        add_submenu_page('settings', __('Embroidery Options', 'Ncquotes'), __('Embroidery Options', 'Ncquotes'), 'read', 'embroidery_options', array(&$this, 'manage_embroidary_options'));
        add_submenu_page('settings', __('Screen Printing Options', 'Ncquotes'), __('Screen Printing Options', 'Ncquotes'), 'read', 'screen_printing_options', array(&$this, 'manage_screen_printing_options'));
        add_submenu_page('settings', __('Screen Printing Options 2', 'Ncquotes'), __('Screen Printing Options 2', 'Ncquotes'), 'read', 'screen_printing_options2', array(&$this, 'manage_screen_printing_options2'));
        add_submenu_page('settings', __('Printable Vinyl Options', 'Ncquotes'), __('Printable Vinyl Options', 'Ncquotes'), 'read', 'printable_vinyl_options', array(&$this, 'manage_printable_vinyl_options'));
        add_submenu_page('settings', __('Personalization Vinyl Options', 'Ncquotes'), __('Personalization Vinyl Options', 'Ncquotes'), 'read', 'personalization_vinyl_options', array(&$this, 'manage_presentational_vinyl_options'));
        add_submenu_page('settings', __('All Drinkware Options', 'Ncquotes'), __('All Drinkware Options', 'Ncquotes'), 'read', 'all_drinkware_options', array(&$this, 'manage_all_drinkware_options'));
        add_submenu_page('settings', __('Sublimation Options', 'Ncquotes'), __('Sublimation Options', 'Ncquotes'), 'read', 'sublimation_options', array(&$this, 'manage_sublimation_options'));
        add_submenu_page('settings', __('Hats Embroidery Options', 'Ncquotes'), __('Hat Embroidery Options', 'Ncquotes'), 'read', 'hats_embroidery_options', array(&$this, 'manage_hats_embroidery_options'));


        //$submenu['daily-diary'][0][0] = 'Daily Diary';
    }

    /*
     * Dashboard Fucntion
     */

    function weather_dashboard_page() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'admin/dashboard.php');
        }
    }

    /*
     * Staff Fucntion
     */

    function manage_codes() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            echo '<div align="center"><div class="spinner-myimg"></div></div>';
            $url = admin_url() . 'edit.php?post_type=codes';
            ?>
            <script>location.href = '<?php echo $url; ?>';</script>
            <?php
        }
    }

    function manage_products() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            echo '<div align="center"><div class="spinner-myimg"></div></div>';
            $url = admin_url() . 'edit.php?post_type=products';
            ?>
            <script>location.href = '<?php echo $url; ?>';</script>
            <?php
        }
    }

    function manage_category() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            echo '<div align="center"><div class="spinner-myimg"></div></div>';
            $url = admin_url() . 'edit-tags.php?taxonomy=product_category&post_type=products';
            ?>
            <script>location.href = '<?php echo $url; ?>';</script>
            <?php
        }
    }

    function manage_colors() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            echo '<div align="center"><div class="spinner-myimg"></div></div>';
            $url = admin_url() . 'edit.php?post_type=colors';
            ?>
            <script>location.href = '<?php echo $url; ?>';</script>
            <?php
        }
    }

    function manage_size() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            echo '<div align="center"><div class="spinner-myimg"></div></div>';
            $url = admin_url() . 'edit-tags.php?taxonomy=product_size&post_type=products';
            ?>
            <script>location.href = '<?php echo $url; ?>';</script>
            <?php
        }
    }

    function manage_printing() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            echo '<div align="center"><div class="spinner-myimg"></div></div>';
            $url = admin_url() . 'edit-tags.php?taxonomy=product_printing&post_type=products';
            ?>
            <script>location.href = '<?php echo $url; ?>';</script>
            <?php
        }
    }

    function manage_quotes() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            echo '<div align="center"><div class="spinner-myimg"></div></div>';
            $url = admin_url() . 'edit.php?post_type=quotes';
            ?>
            <script>location.href = '<?php echo $url; ?>';</script>
            <?php
        }
    }

    /*
     * Setting Fucntion
     */

    function manage_settings() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/classSettings.php');
        }
    }

    function manage_embroidary_options() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/classEmbSettings.php');
        }
    }

    function manage_screen_printing_options() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/classScrPriSettings.php');
        }
    }

    function manage_screen_printing_options2() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/class2ScrPriSettings.php');
        }
    }

    function manage_printable_vinyl_options() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/classPriVnySettings.php');
        }
    }

    function manage_hats_embroidery_options() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/classHatsEmbSettings.php');
        }
    }

    function manage_presentational_vinyl_options() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/classPreVnySettings.php');
        }
    }

    function manage_all_drinkware_options() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/classAllDriSettings.php');
        }
    }

    function manage_sublimation_options() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/classSublimationSettings.php');
        }
    }

    function manage_dashboard() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'Ncquotes'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/classDashboard.php');
        }
    }

    /*
     * Hide Menus
     */

    function cwebc_remove_weather_menu_items() {
        remove_menu_page('edit.php?post_type=codes');
    }

    function cwebc_remove_product_menu_items() {
        remove_menu_page('edit.php?post_type=products');
    }

    function cwebc_remove_color_menu_items() {
        remove_menu_page('edit.php?post_type=colors');
    }

    function cwebc_remove_category_menu_items() {
        remove_menu_page('edit.php?taxonomy=product_category');
    }

    function cwebc_remove_size_menu_items() {
        remove_menu_page('edit.php?post_type=size');
    }

    function cwebc_remove_printing_menu_items() {
        remove_menu_page('edit.php?post_type=printing');
    }

    function cwebc_remove_quotes_menu_items() {
        remove_menu_page('edit.php?post_type=quotes');
    }

    function cwebc_active_current_menu() {
        //Code for show current menu
        $screen = get_current_screen();
//        pr($screen);die;
        ?>
        <script type="text/javascript">
            (function ($) {
                $(document).ready(function () {
        <?php if ($screen->id == 'edit-products' || $screen->id == 'products') { ?>
                        $('#toplevel_page_settings').addClass('wp-menu-open');
                        $('#toplevel_page_settings').addClass('wp-has-current-submenu');
                        $('#toplevel_page_Printing').addClass('wp-menu-open');
                        $('#toplevel_page_Printing').addClass('wp-has-current-submenu');
                        $('#toplevel_page_settings>a').addClass('wp-has-current-submenu');

                        $('a[href$="Products"]').parent().addClass('current');
                        $('.toplevel_page_weather').addClass('current');
                        $('.toplevel_page_weather').parent().addClass('wp-has-current-submenu');
        <?php } ?>
                });
            })(jQuery);
        </script>
        <?php
    }

}
?>