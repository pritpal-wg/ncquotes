<?php
$nc_settings_group = 'nc-all_drinkware-settings-group';
?>
<style>
    #nc_pri_pricing_table {
        width: 100%;
    }

    #nc_pri_pricing_table th,#nc_pri_pricing_table td {
        padding: 1px 2px;
    }

    #nc_pri_pricing_table input[type="number"] {
        width: 170px;
    }
</style>
<div class="wrap">
    <h2>All Drinkware Options</h2>
    <form method="post" action="options.php">
        <?php settings_fields($nc_settings_group); ?>
        <?php do_settings_sections($nc_settings_group); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Set number of location of prints</th>
                <td><input type="number" name="nc_all_num_print_locations" value="<?php echo get_option('nc_all_num_print_locations') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Possible locations of prints</th>
                <td>
                    <textarea rows="4" cols="35" readonly><?php echo get_option('nc_all_print_locations', '') ?></textarea>
                    <p class="description">Multiple values in comma separated manner. (CSV)</p>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">Minimum Order Quantity</th>
                <td><input type="number" name="nc_all_min_order_quantity" value="<?php echo get_option('nc_all_min_order_quantity', '') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Setup Cost</th>
                <td><input type="number" step="any" name="nc_all_setup_cost" value="<?php echo get_option('nc_all_setup_cost', '') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Previous Setup Cost</th>
                <td><input type="number" step="any" name="nc_all_prev_setup_cost" value="<?php echo get_option('nc_all_prev_setup_cost') ?>" /></td>
            </tr>

            <tr valign="top">
                <td colspan="2">
                    <table id="nc_pri_pricing_table" border="1">
                        <thead>
                            <tr>
                                <th>To Quantity</th>
                                <th>1 Side Price</th>
                                <th>2 Side Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach (get_option('nc_all_pricing', array()) as $uniq_key => $pricing_info) { ?>
                                <tr data-pricing_id="<?php echo $uniq_key ?>">
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['to_quantity'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <input type="number" class="nc_to_quantity" min="1" placeholder="To Quantity" value="<?php echo $pricing_info['to_quantity'] ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['price_1_side'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <input type="number" class="nc_price_1_side" step="any" placeholder="Price for 1 Side" value="<?php echo $pricing_info['price_1_side'] ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['price_2_sides'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <input type="number" class="nc_price_2_sides" step="any" placeholder="Price for 2 Sides" value="<?php echo $pricing_info['price_2_sides'] ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="update_pricing" style="display: none">Done</a>
                                        <a href="javascript:;" class="edit_pricing">Edit</a>
                                        <span>| <a href="javascript:;" class="delete_pricing">Delete</a></span>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th><input type="number" id="nc_to_quantity" min="1" placeholder="To Quantity" /></th>
                                <th>
                                    <input type="number" id="nc_price_1_side" step="any" placeholder="Price for 1 Side" />
                                </th>
                                <th>
                                    <input type="number" id="nc_price_2_sides" step="any" placeholder="Price for 2 Sides" />
                                </th>
                                <th>
                                    <button type="button" id="nc_add_pricing" class="button button-primary" style="width: 100%">Add</button>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
</div>
<script>
    (function ($) {
        var ajax_url = '<?php echo admin_url('admin-ajax.php') ?>';
        $(document).on('click', '#nc_add_pricing', function (e) {
            e.preventDefault();
            //get fields
            {
                var to_quantity_field = $('#nc_to_quantity');
                var price_1_side_field = $('#nc_price_1_side');
                var price_2_sides_field = $('#nc_price_2_sides');
            }

            //get values
            {
                var to_quantity = to_quantity_field.val();
                var price_1_side = price_1_side_field.val();
                var price_2_sides = price_2_sides_field.val();
            }

            //check if all values are entered
            if (to_quantity && price_1_side && price_2_sides) {
                $.post(ajax_url, {
                    action: 'nc_add_all_pricing',
                    to_quantity: to_quantity,
                    price_1_side: price_1_side,
                    price_2_sides: price_2_sides,
                }, function () {
                    var pricing_table = $('#nc_pri_pricing_table');
                    var tr = '<tr>';
                    tr += '<td>' + to_quantity + '</td>';
                    tr += '<td>' + price_1_side + '</td>';
                    tr += '<td>' + price_2_sides + '</td>';
                    tr += '<td>Reload Page to View Actions</td>';
                    tr += '</tr>';
                    pricing_table.find('tbody').append(tr);
                    pricing_table.find('tfoot input[type="number"]').val('');
                });
            }
        }).on('click', '.delete_pricing', function (e) {
            e.preventDefault();
            var conf = confirm('Are you Sure???');
            if (!conf)
                return;
            var el = $(this);
            var old_html = el.html();
            el.html("Please Wait...").prop('disabled', true);
            var tr = el.closest('tr');
            var pricing_id = tr.data('pricing_id');
            $.post(ajax_url, {
                action: 'nc_delete_pricing',
                type: 'all',
                pricing_id: pricing_id
            }).done(function (data) {
                if (data) {
                    tr.hide(function () {
                        $(this).remove();
                    });
                } else {
                    el.html(old_html).prop('disabled', false);
                    alert("Error: Something went wrong...");
                }
            }).fail(function (error) {
                el.html(old_html).prop('disabled', false);
                alert("Error: Something went wrong...");
            });
        }).on('click', '.edit_pricing', function (e) {
            e.preventDefault();
            var el = $(this);
            el.hide();
            var td = el.closest('td');
            td.find(".update_pricing").show();
            td.find('.delete_pricing').parent().hide();
            var tr = el.closest('tr');
            tr.find('.nc_text').hide();
            tr.find('.nc_input').show();
        }).on('click', '.update_pricing', function (e) {
            e.preventDefault();
            var el = $(this);
            var old_html = el.html();
            el.html("Please Wait...").prop("disabled", true);
            var tr = el.closest('tr');

            //get values
            {
                var pricing_id = tr.data('pricing_id');
                var to_quantity = tr.find('.nc_to_quantity').val();
                var price_1_side = tr.find('.nc_price_1_side').val();
                var price_2_sides = tr.find('.nc_price_2_sides').val();
            }

            if (to_quantity && price_1_side && price_2_sides) {
                $.post(ajax_url, {
                    action: 'nc_update_pricing',
                    type: 'all',
                    pricing_id: pricing_id,
                    to_quantity: to_quantity,
                    price_1_side: price_1_side,
                    price_2_sides: price_2_sides,
                }).done(function () {
                    el.hide();
                    el.html(old_html).prop("disabled", false);
                    var td = el.closest('td');
                    td.find(".edit_pricing").show();
                    td.find('.delete_pricing').parent().show();
                    tr.find('.nc_text').show();
                    tr.find('.nc_input').hide();
                    //update the view
                    {
                        tr.find('td:nth-child(1) .nc_text').text(to_quantity);
                        tr.find('td:nth-child(2) .nc_text').text(price_1_side);
                        tr.find('td:nth-child(3) .nc_text').text(price_2_sides);
                    }
                });
            }
        });
    })(jQuery);
</script>