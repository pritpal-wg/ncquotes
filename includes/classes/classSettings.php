<?php
$nc_settings_group = 'nc-general-settings-group';
//get previously saved settings
{
    $nc_bottom_message = get_option('nc_bottom_message');
    $nc_admin_email = get_option('nc_admin_email');
}
?>
<div class="wrap">
    <h2>NC Quotes Settings</h2>
    <form method="post" action="options.php">
        <?php settings_fields($nc_settings_group); ?>
        <?php do_settings_sections($nc_settings_group); ?>
        <table class="form-table">
            <tr valign="top">
                <th colspan="2">
                    <span style="font-size: 109%">Bottom Message</span>
                    <?= '<hr />' ?>
                </th>
            </tr>

            <tr valign="top">
                <th scope="row">Bottom Message</th>
                <td>
                    <?php echo '<textarea rows="4" cols="40" name="nc_bottom_message">' . $nc_bottom_message . '</textarea>'; ?>
                    <p class="description">
                        Type message for customers to be displayed at bottom of screen.
                    </p>
                </td>
            </tr>

            <tr valign="top">
                <th colspan="2">
                    <span style="font-size: 109%">Notifications</span>
                    <?= '<hr />' ?>
                </th>
            </tr>

            <tr valign="top">
                <th scope="row">Admin Email</th>
                <td><input type="email" name="nc_admin_email" value="<?= $nc_admin_email ?>" /></td>
            </tr>

        </table>
        <?php submit_button(); ?>
    </form>
</div>