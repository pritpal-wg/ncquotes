<?php
/***********Size category start here*********/

function add_size_taxonomy_to_product(){
 
    //set the name of the taxonomy
    $taxonomy = 'product_size';
    //set the post types for the taxonomy
    $object_type = 'Products';
     
    //populate our array of names for our taxonomy
    $labels = array(
        'name'               => 'Size',
        'singular_name'      => 'Size',
        'search_items'       => 'Search Size',
        'all_items'          => 'All Size',
        'parent_item'        => 'Parent Size',
        'parent_item_colon'  => 'Parent Size',
        'update_item'        => 'Update Size',
        'edit_item'          => 'Edit Size',
        'add_new_item'       => 'Add New Size', 
        'new_item_name'      => 'New Size',
        'menu_name'          => 'Size'
    );
     
    //define arguments to be used 
    $args = array(
        'labels'            => $labels,
        'hierarchical'      => true,
        'show_ui'           => true,
        'how_in_nav_menus'  => true,
        'public'            => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'product_size')
    );
    

     
    //call the register_taxonomy function
    register_taxonomy($taxonomy, $object_type, $args); 
}
add_action('init','add_size_taxonomy_to_product'); 

/****************Size category End Here***********/

function add_category_taxonomy_to_product(){
 
    //set the name of the taxonomy
    $taxonomy = 'product_category';
    //set the post types for the taxonomy
    $object_type = 'products';
     
    //populate our array of names for our taxonomy
    $labels = array(
        'name'               => 'Products Category',
        'singular_name'      => 'Products Category',
        'search_items'       => 'Search Products Category',
        'all_items'          => 'All Products Category',
        'parent_item'        => 'Parent Products Category',
        'parent_item_colon'  => 'Parent Products Category',
        'update_item'        => 'Update Products Category',
        'edit_item'          => 'Edit Products Category',
        'add_new_item'       => 'Add New Products Category', 
        'new_item_name'      => 'New Products Category',
        'menu_name'          => 'Products Category'
    );
     
    //define arguments to be used 
    $args = array(
        'labels'            => $labels,
        'hierarchical'      => true,
        'show_ui'           => true,
        'how_in_nav_menus'  => true,
        'public'            => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'product_category')
    );
    

     
    //call the register_taxonomy function
    register_taxonomy($taxonomy, $object_type, $args); 
}
add_action('init','add_category_taxonomy_to_product'); 
 
/***********Printing category start here*********/

function add_printing_taxonomy_to_product(){
 
    //set the name of the taxonomy
    $taxonomy = 'product_printing';
    //set the post types for the taxonomy
    $object_type = 'Products';
     
    //populate our array of names for our taxonomy
    $labels = array(
        'name'               => 'Product Printing',
        'singular_name'      => 'Printing',
        'search_items'       => 'Search Printing',
        'all_items'          => 'All Printing',
        'parent_item'        => 'Parent Printing',
        'parent_item_colon'  => 'Parent Printing',
        'update_item'        => 'Update Printing',
        'edit_item'          => 'Edit Printing',
        'add_new_item'       => 'Add New Printing', 
        'new_item_name'      => 'New Printing',
        'menu_name'          => 'Printing'
    );
     
    //define arguments to be used 
    $args = array(
        'labels'            => $labels,
        'hierarchical'      => true,
        'show_ui'           => true,
        'how_in_nav_menus'  => true,
        'public'            => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array('slug' => 'product_printing')
    );
    

     
    //call the register_taxonomy function
    register_taxonomy($taxonomy, $object_type, $args); 
}
add_action('init','add_printing_taxonomy_to_product'); 

/****************Product category End Here***********/


