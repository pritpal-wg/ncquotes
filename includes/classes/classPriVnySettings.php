<?php
$nc_settings_group = 'nc-printable_vinyl-settings-group';
$nc_pri_print_locations = array_map('trim', explode(',', get_option('nc_pri_print_locations')));
?>
<style>
    #nc_pri_pricing_table {
        width: 100%;
    }

    #nc_pri_pricing_table th,#nc_pri_pricing_table td {
        padding: 1px 2px;
    }

    #nc_pri_pricing_table input[type="number"] {
        width: 170px;
    }
</style>
<div class="wrap">
    <h2>Printable Vinyl Options</h2>
    <form method="post" action="options.php">
        <?php settings_fields($nc_settings_group); ?>
        <?php do_settings_sections($nc_settings_group); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Set number of location of prints</th>
                <td><input type="number" name="nc_pri_num_print_locations" value="<?php echo get_option('nc_pri_num_print_locations') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Possible locations of prints</th>
                <td>
                    <textarea rows="4" cols="35" name="nc_pri_print_locations"><?php echo get_option('nc_pri_print_locations', '') ?></textarea>
                    <p class="description">
                        Multiple values in comma separated manner. (CSV)
                    </p>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">Allow vinyl personalization</th>
                <td>
                    <label>
                        <input type="radio" name="nc_pri_allow_vinyl_personalization" value="1" checked /> True
                    </label>
                    <label style="margin-left: 3%">
                        <input type="radio" name="nc_pri_allow_vinyl_personalization" value="0"<?php echo get_option('nc_pri_allow_vinyl_personalization') === '0' ? ' checked' : '' ?> /> False
                    </label>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">Minimum Order Quantity</th>
                <td><input type="number" name="nc_pri_min_order_quantity" value="<?php echo get_option('nc_pri_min_order_quantity', '') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Setup Cost</th>
                <td><input type="number" step="any" name="nc_pri_setup_cost" value="<?php echo get_option('nc_pri_setup_cost', '') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Previous Setup Cost</th>
                <td><input type="number" step="any" name="nc_pri_prev_setup_cost" value="<?php echo get_option('nc_pri_prev_setup_cost') ?>" /></td>
            </tr>

            <tr valign="top">
                <td colspan="2">
                    <table id="nc_pri_pricing_table" border="1">
                        <thead>
                            <tr>
                                <th>Location</th>
                                <th>To Quantity</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach (get_option('nc_pri_pricing', array()) as $uniq_key => $pricing_info) { ?>
                                <tr data-pricing_id="<?php echo $uniq_key ?>">
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['location'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <select class="nc_location">
                                                <option value="" hidden>Select Location</option>
                                                <?php foreach ($nc_pri_print_locations as $loc) { ?>
                                                    <?php
                                                    $selected = $pricing_info['location'] == $loc ? ' selected' : '';
                                                    ?>
                                                    <option<?php echo $selected ?>><?php echo $loc ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['to_quantity'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <input type="number" class="nc_to_quantity" min="1" placeholder="To Quantity" value="<?php echo $pricing_info['to_quantity'] ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['price'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <input type="number" class="nc_price" step="any" placeholder="Price" value="<?php echo $pricing_info['price'] ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="update_pricing" style="display: none">Done</a>
                                        <a href="javascript:;" class="edit_pricing">Edit</a>
                                        <span>| <a href="javascript:;" class="delete_pricing">Delete</a></span>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>
                                    <select id="nc_location">
                                        <option value="" hidden>Select Location</option>
                                        <?php foreach ($nc_pri_print_locations as $loc) { ?>
                                            <option><?php echo $loc ?></option>
                                        <?php } ?>
                                    </select>
                                </th>
                                <th><input type="number" id="nc_to_quantity" min="1" placeholder="To Quantity" /></th>
                                <th><input type="number" id="nc_price" step="any" placeholder="Price" /></th>
                                <th><button type="button" id="nc_add_pricing" class="button button-primary" style="width: 100%">Add</button></th>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
</div>
<script>
    (function ($) {
        var ajax_url = '<?php echo admin_url('admin-ajax.php') ?>';
        $(document).on('click', '#nc_add_pricing', function (e) {
            e.preventDefault();
            //get fields
            {
                var location_field = $('#nc_location');
                var to_quantity_field = $('#nc_to_quantity');
                var price_field = $('#nc_price');
            }

            //get values
            {
                var location = location_field.val();
                var to_quantity = to_quantity_field.val();
                var price = price_field.val();
            }

            //check if all values are entered
            if (location && to_quantity && price) {
                $.post(ajax_url, {
                    action: 'nc_add_pri_pricing',
                    location: location,
                    to_quantity: to_quantity,
                    price: price,
                }, function () {
                    var pricing_table = $('#nc_pri_pricing_table');
                    var tr = '<tr>';
                    tr += '<td>' + location + '</td>';
                    tr += '<td>' + to_quantity + '</td>';
                    tr += '<td>' + price + '</td>';
                    tr += '<td>Reload Page to View Actions</td>';
                    tr += '</tr>';
                    pricing_table.find('tbody').append(tr);
                    pricing_table.find('tfoot input[type="number"]').val('');
                });
            }
        }).on('click', '.delete_pricing', function (e) {
            e.preventDefault();
            var conf = confirm('Are you Sure???');
            if (!conf)
                return;
            var el = $(this);
            var old_html = el.html();
            el.html("Please Wait...").prop('disabled', true);
            var tr = el.closest('tr');
            var pricing_id = tr.data('pricing_id');
            $.post(ajax_url, {
                action: 'nc_delete_pricing',
                type: 'pri',
                pricing_id: pricing_id
            }).done(function (data) {
                if (data) {
                    tr.hide(function () {
                        $(this).remove();
                    });
                } else {
                    el.html(old_html).prop('disabled', false);
                    alert("Error: Something went wrong...");
                }
            }).fail(function (error) {
                el.html(old_html).prop('disabled', false);
                alert("Error: Something went wrong...");
            });
        }).on('click', '.edit_pricing', function (e) {
            e.preventDefault();
            var el = $(this);
            el.hide();
            var td = el.closest('td');
            td.find(".update_pricing").show();
            td.find('.delete_pricing').parent().hide();
            var tr = el.closest('tr');
            tr.find('.nc_text').hide();
            tr.find('.nc_input').show();
        }).on('click', '.update_pricing', function (e) {
            e.preventDefault();
            var el = $(this);
            var old_html = el.html();
            el.html("Please Wait...").prop("disabled", true);
            var tr = el.closest('tr');

            //get values
            {
                var pricing_id = tr.data('pricing_id');
                var location = tr.find('.nc_location').val();
                var to_quantity = tr.find('.nc_to_quantity').val();
                var price = tr.find('.nc_price').val();
            }

            if (location && to_quantity && price) {
                $.post(ajax_url, {
                    action: 'nc_update_pricing',
                    type: 'pri',
                    pricing_id: pricing_id,
                    location: location,
                    to_quantity: to_quantity,
                    price: price
                }).done(function () {
                    el.hide();
                    el.html(old_html).prop("disabled", false);
                    var td = el.closest('td');
                    td.find(".edit_pricing").show();
                    td.find('.delete_pricing').parent().show();
                    tr.find('.nc_text').show();
                    tr.find('.nc_input').hide();
                    //update the view
                    {
                        tr.find('td:nth-child(1) .nc_text').text(location);
                        tr.find('td:nth-child(2) .nc_text').text(to_quantity);
                        tr.find('td:nth-child(3) .nc_text').text(price);
                    }
                });
            }
        });
    })(jQuery);
</script>