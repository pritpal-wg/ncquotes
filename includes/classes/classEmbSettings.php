<?php
$nc_settings_group = 'nc-embroidery-settings-group';
$embroidary_slug = 'embroidery';
?>
<style>
    #nc_emb_pricing_table {
        width: 100%;
    }

    #nc_emb_pricing_table th,#nc_emb_pricing_table td {
        padding: 1px 2px;
    }

    #nc_emb_pricing_table input[type="number"] {
        width: 100px;
    }
</style>
<div class="wrap">
    <h2>Embroidery Options</h2>
    <form method="post" action="options.php" novalidate>
        <?php settings_fields($nc_settings_group); ?>
        <?php do_settings_sections($nc_settings_group); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Set number of location of prints</th>
                <td><input type="number" step="1" min="1" name="nc_emb_num_print_locations" value="<?php echo get_option('nc_emb_num_print_locations', '') ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Possible locations of prints</th>
                <td>
                    <textarea rows="4" cols="35" name="nc_emb_print_locations"><?php echo get_option('nc_emb_print_locations', '') ?></textarea>
                    <p class="description">
                        Multiple values in comma separated manner. (CSV)
                    </p>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Minimum Order Quantity</th>
                <td><input type="number" name="nc_emb_min_order_quantity" value="<?php echo get_option('nc_emb_min_order_quantity', '') ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Setup Cost</th>
                <td><input type="number" step="any" name="nc_emb_setup_cost" value="<?php echo get_option('nc_emb_setup_cost', '') ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Previous Setup Cost</th>
                <td><input type="number" step="any" name="nc_emb_prev_setup_cost" value="<?php echo get_option('nc_emb_prev_setup_cost') ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Allow vinyl personalization</th>
                <td>
                    <label>
                        <input type="radio" name="nc_emb_allow_vinyl_personalization" value="1" checked /> True
                    </label>
                    <label style="margin-left: 3%">
                        <input type="radio" name="nc_emb_allow_vinyl_personalization" value="0"<?php echo get_option('nc_emb_allow_vinyl_personalization') === '0' ? ' checked' : '' ?> /> False
                    </label>
                </td>
            </tr>
            <tr valign="top">
                <td colspan="2">
                    <table id="nc_emb_pricing_table" border="1">
                        <thead>
                            <tr>
                                <th>Stitches</th>
                                <th>Pieces</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach (get_option('nc_emb_pricing', array()) as $uniq_key => $pricing_info) { ?>
                                <tr data-pricing_id="<?php echo $uniq_key ?>">
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['stitches'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <?php
                                            $arr = array_map('trim', explode('-', $pricing_info['stitches']));
                                            $stitches_from = $arr[0];
                                            $stitches_to = $arr[1];
                                            ?>
                                            <input type="number" class="nc_stitches_from" min="1" placeholder="from" value="<?php echo $stitches_from ?>" /> - 
                                            <input type="number" class="nc_stitches_to" min="2" placeholder="to" value="<?php echo $stitches_to ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['pieces'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <?php
                                            $arr = array_map('trim', explode('-', $pricing_info['pieces']));
                                            $pieces_from = $arr[0];
                                            $pieces_to = $arr[1];
                                            ?>
                                            <input type="number" class="nc_pieces_from" min="1" placeholder="from" value="<?php echo $pieces_from ?>" /> - 
                                            <input type="number" class="nc_pieces_to" min="2" placeholder="to" value="<?php echo $pieces_to ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['pricing'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <input type="number" class="nc_pricing" placeholder="Price" step="any" min="0.01" value="<?php echo $pricing_info['pricing'] ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="update_pricing" style="display: none">Done</a>
                                        <a href="javascript:;" class="edit_pricing">Edit</a>
                                        <span>| <a href="javascript:;" class="delete_pricing">Delete</a></span>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>
                                    <input type="number" id="nc_stitches_from" min="1" placeholder="from" /> - <input type="number" id="nc_stitches_to" min="2" placeholder="to" />
                                </th>
                                <th>
                                    <input type="number" id="nc_pieces_from" min="1" placeholder="from" /> - <input type="number" id="nc_pieces_to" min="2" placeholder="to" />
                                </th>
                                <th>
                                    <input type="number" id="nc_pricing" placeholder="Price" step="any" min="0.01" />
                                </th>
                                <th>
                                    <button type="button" id="nc_add_pricing" class="button button-primary" style="width: 100%">Add</button>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
</div>
<script>
    (function ($) {
        var ajax_url = '<?php echo get_admin_url('', 'admin-ajax.php') ?>';
        $(document).ready(function () {
        }).on('click', '#nc_add_pricing', function (e) {
            e.preventDefault();
            //get fields
            {
                var stitches_from_field = $('#nc_stitches_from');
                var stitches_to_field = $('#nc_stitches_to');
                var pieces_from_field = $('#nc_pieces_from');
                var pieces_to_field = $('#nc_pieces_to');
                var pricing_feild = $('#nc_pricing');
            }

            //get values
            {
                var stitches_from = stitches_from_field.val();
                var stitches_to = stitches_to_field.val();
                var pieces_from = pieces_from_field.val();
                var pieces_to = pieces_to_field.val();
                var pricing = pricing_feild.val();
            }

            //check if all values are entered
            if (stitches_from && stitches_to && pieces_from && pieces_to && pricing) {
                $.post(ajax_url, {
                    action: 'nc_add_emb_pricing',
                    stitches_from: stitches_from,
                    stitches_to: stitches_to,
                    pieces_from: pieces_from,
                    pieces_to: pieces_to,
                    pricing: pricing,
                }, function () {
                    var pricing_table = $('#nc_emb_pricing_table');
                    var tr = '<tr>';
                    tr += '<td>' + stitches_from + ' - ' + stitches_to + '</td>';
                    tr += '<td>' + pieces_from + ' - ' + pieces_to + '</td>';
                    tr += '<td>' + pricing + '</td>';
                    tr += '<td>Reload Page to View Actions</td>';
                    tr += '</tr>';
                    pricing_table.find('tbody').append(tr);
                    pricing_table.find('tfoot input[type="number"]').val('');
                });
            }
        }).on('click', '.delete_pricing', function (e) {
            e.preventDefault();
            var conf = confirm('Are you Sure???');
            if (!conf)
                return;
            var el = $(this);
            var old_html = el.html();
            el.html("Please Wait...").prop('disabled', true);
            var tr = el.closest('tr');
            var pricing_id = tr.data('pricing_id');
            $.post(ajax_url, {
                action: 'nc_delete_pricing',
                type: 'emb',
                pricing_id: pricing_id
            }).done(function (data) {
                if (data) {
                    tr.hide(function () {
                        $(this).remove();
                    });
                } else {
                    el.html(old_html).prop('disabled', false);
                    alert("Error: Something went wrong...");
                }
            }).fail(function (error) {
                el.html(old_html).prop('disabled', false);
                alert("Error: Something went wrong...");
            });
        }).on('click', '.edit_pricing', function (e) {
            e.preventDefault();
            var el = $(this);
            el.hide();
            var td = el.closest('td');
            td.find(".update_pricing").show();
            td.find('.delete_pricing').parent().hide();
            var tr = el.closest('tr');
            tr.find('.nc_text').hide();
            tr.find('.nc_input').show();
        }).on('click', '.update_pricing', function (e) {
            e.preventDefault();
            var el = $(this);
            var old_html = el.html();
            el.html("Please Wait...").prop("disabled", true);
            var tr = el.closest('tr');
            //get values
            {
                var pricing_id = tr.data('pricing_id');
                var stitches_from = tr.find('.nc_stitches_from').val();
                var stitches_to = tr.find('.nc_stitches_to').val();
                var pieces_from = tr.find('.nc_pieces_from').val();
                var pieces_to = tr.find('.nc_pieces_to').val();
                var pricing = tr.find('.nc_pricing').val();
            }
            if (pricing_id && stitches_from && stitches_to && pieces_from && pieces_to && pricing) {
                $.post(ajax_url, {
                    action: 'nc_update_pricing',
                    type: 'emb',
                    pricing_id: pricing_id,
                    stitches_from: stitches_from,
                    stitches_to: stitches_to,
                    pieces_from: pieces_from,
                    pieces_to: pieces_to,
                    pricing: pricing
                }).done(function () {
                    el.hide();
                    el.html(old_html).prop("disabled", false);
                    var td = el.closest('td');
                    td.find(".edit_pricing").show();
                    td.find('.delete_pricing').parent().show();
                    tr.find('.nc_text').show();
                    tr.find('.nc_input').hide();
                    //update the view
                    {
                        tr.find('td:nth-child(1) .nc_text').text(stitches_from + " - " + stitches_to);
                        tr.find('td:nth-child(2) .nc_text').text(pieces_from + " - " + pieces_to);
                        tr.find('td:nth-child(3) .nc_text').text(pricing);
                    }
                });
            }
        });
    })(jQuery);
</script>