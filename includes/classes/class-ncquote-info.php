<?php

class weatherInfo{

    private $orderby;
    private $order;
    private $wpdb;

    function __construct($order='desc', $orderby='id') {
        global $wpdb;
        $this->orderby = $orderby;
        $this->order = $order;
        $this->wpdb = $wpdb;
    }
	
	/* Insert Data */
    function addJsonData($data){
			global $wpdb;
			$query="INSERT INTO `".$wpdb->prefix."weather_data`
                                       (`city_id`,`city_name`,`response`,`add_date`
                                        ) VALUES
                                       ('".$data['city_id']."',
                                        '".$data['city_name']."',
										'".$data['response']."', 
										'".date('Y-m-d H:i:s')."'										
                                        )"; 
										
          return $wpdb->get_results($query);		 
	}	
	
	
	/* Check Recent Entry  */
    function checkEntry($city){
        global $wpdb;
		   $beforeTime=date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s').' -4 hours'));
		   $dateTime=date('Y-m-d H:i:s');
           $query="SELECT * FROM ".$wpdb->prefix."weather_data WHERE city_id='".$city."' AND add_date BETWEEN '".$beforeTime."' AND '".$dateTime."' ORDER BY id desc limit 0,1"; 
        
           $rec=$wpdb->get_results($query);
			if(!empty($rec)):
				return $rec[0];
			else:
				return '';
			endif;
	}  
	
	
	
	/* Insert Sunrise And Sunset Dat */
    function addSunriseSunset($city,$sun){
			global $wpdb;
			$query="INSERT INTO `".$wpdb->prefix."weather_sun_info`
                                       (`city_id`,`sunrise`,`sunset`,`date_data`,`add_date`
                                        ) VALUES
                                       ('".$city."',
                                        '".date('H:i',strtotime($sun['rise']))."',
										'".date('H:i',strtotime($sun['set']))."', 
										'".date('Y-m-d',strtotime($sun['rise']))."', 
										'".date('Y-m-d H:i:s')."'										
                                        )"; 
          return $wpdb->get_results($query);		 
	} 	
		
	function checkSunInfo($city,$start_date){
		global $wpdb;
			$query="SELECT `sunrise`,`sunset` FROM ".$wpdb->prefix."weather_sun_info WHERE city_id='".$city."' AND date_data='".$start_date."' ORDER BY id desc limit 0,1"; 
			
			$rec=$wpdb->get_results($query);
			if(!empty($rec)):
				return array('rise'=>$rec[0]->sunrise,'set'=>$rec[0]->sunset);
			else:
				return showSunInfo($city,$start_date);
			endif;		
	}	
	
	
}
?>
