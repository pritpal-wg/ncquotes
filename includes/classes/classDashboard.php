<?php
$posts = get_posts(
        array(
            'post_type' => 'quotes',
            'posts_per_page' => 10,
        )
);
//pr($posts);
?>
<div class="wrap">
    <div id="header-area"> 
        <div class="logo">
            <div class="emg-icon-option-left"></div>
            <div class="emg-cp-title"><h2><?php echo __('LW-Quote Information', 'Ncquotes'); ?></h2></div>
        </div>
    </div>

    <div id="main-area">

        <?php //show_error_message();?>
        <div>

            <div class="clear"></div>
        </div>
        <!--Start Bootstrap -->
        <div class="row">
            <div class="_main-row">


                <div class="col-xs-9 right-op-panel padding0">
                    <!-- Tab panes -->

                    <div class="tab-content">



                        <div class="tab-pane" id="media">
                            <div class="of-info-cy-main">
                                <h3 style="margin: 0"><?php echo _e('Recent Quote Requests', 'Ncquotes'); ?></h3>

                            </div>			  
                            <table style="width: 100%" class="setting_table">
                                <tr>
                                    <th>Date</th>
                                    <th>Product Name</th>
                                    <th>View Details</th>
                                </tr>
                                <?php foreach ($posts as $post) { ?>
                                    <?php
                                    $id = $post->ID;
                                    $prod_id = get_post_meta($id, 'nc_prod_id', true);
                                    $product = get_post($prod_id);
                                    ?>
                                    <tr class="row1" style="text-align:center;">
                                        <td><?php echo $post->post_date ?></td>
                                        <td><?php echo $product->post_title ?></td>
                                        <td><a href="<?php echo get_site_url() . "/wp-admin/post.php?post=$id&action=edit" ?>">View Details</a></td>
                                    </tr> 
                                <?php } ?>
                            </table>

                            <div class="of-info-cy-main">
                                <h3 style="margin: 0"><?php echo __('Latest Products'); ?></h3>

                            </div>
                            <?php
                            $args = array(
                                'numberposts' => 10,
                                'offset' => 0,
                                'category' => 0,
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'post_type' => 'products',
                                'post_status' => 'draft, publish, future, pending, private',
                                'suppress_filters' => true
                            );
                            $latest_products = wp_get_recent_posts(
                                    array(
                                'numberposts' => 10,
                                'offset' => 0,
                                'category' => 0,
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'post_type' => 'products',
                                'post_status' => 'draft, publish, future, pending, private',
                                'suppress_filters' => true
                                    ), OBJECT
                            );
                            ?>
                            <table style="width: 100%" class="setting_table">
                                <tr>
                                    <th>Product</th>
                                    <th>Categories</th>
                                    <th>View detail</th>
                                </tr>
                                <?php foreach ($latest_products as $product) { ?>
                                    <?php
                                    $product_categories = get_the_terms($product->ID, 'product_category');
                                    ?>
                                    <tr style="text-align:center">
                                        <td><?php echo $product->post_title ?></td>
                                        <td>
                                            <?php
                                            $cat_str = '';
                                            foreach ($product_categories as $cat) {
                                                $cat_str .= $cat->name . ', ';
                                            }
                                            if ($cat_str) {
                                                $cat_str = rtrim($cat_str, ', ');
                                            }
                                            echo $cat_str ? $cat_str : '<small style="color: grey">No Categories</small>'
                                            ?>
                                        </td>
                                        <td><a href="<?php echo get_site_url() . "/wp-admin/post.php?post=$product->ID&action=edit" ?>">View Details</a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>