<?php
$nc_settings_group = 'nc-2screen_printing-settings-group';
?>
<style>
    #nc_scr_pri_pricing_table {
        width: 100%;
    }

    #nc_scr_pri_pricing_table th,#nc_scr_pri_pricing_table td {
        padding: 1px 2px;
    }

    #nc_scr_pri_pricing_table input[type="number"] {
        width: 100px;
    }
</style>
<div class="wrap">
    <h2>Screen Printing Options 2</h2>
    <form method="post" action="options.php">
        <?php settings_fields($nc_settings_group); ?>
        <?php do_settings_sections($nc_settings_group); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Set number of location of prints</th>
                <td><input type="number" name="nc_2sc_num_print_locations" value="<?php echo get_option('nc_2sc_num_print_locations', '') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Minimum Order Quantity</th>
                <td><input type="number" name="nc_2sc_min_order_quantity" value="<?php echo get_option('nc_2sc_min_order_quantity') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Setup Cost</th>
                <td><input type="number" step="any" name="nc_2sc_setup_cost" value="<?php echo get_option('nc_2sc_setup_cost', '') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Previous Setup Cost</th>
                <td><input type="number" step="any" name="nc_2sc_prev_setup_cost" value="<?php echo get_option('nc_2sc_prev_setup_cost', '') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Additional Price for white color prints</th>
                <td><input type="number" step="any" name="nc_2sc_white_color_cost" value="<?php echo get_option('nc_2sc_white_color_cost', '') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Possible locations of prints</th>
                <td>
                    <textarea rows="4" cols="35" name="nc_2sc_print_locations"><?php echo get_option('nc_2sc_print_locations', '') ?></textarea>
                    <p class="description">
                        Multiple values in comma separated manner. (CSV)
                    </p>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">Possible number of colors for art</th>
                <td><input type="number" step="1" name="nc_2sc_num_colors_art" value="<?= get_option('nc_2sc_num_colors_art', '') ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Allow vinyl personalization</th>
                <td>
                    <label>
                        <input type="radio" name="nc_2sc_allow_vinyl_personalization" value="1" checked /> True
                    </label>
                    <label style="margin-left: 3%">
                        <input type="radio" name="nc_2sc_allow_vinyl_personalization" value="0"<?php echo get_option('nc_2sc_allow_vinyl_personalization') === '0' ? ' checked' : '' ?> /> False
                    </label>
                </td>
            </tr>

            <tr valign="top">
                <td colspan="2">
                    <table id="nc_scr_pri_pricing_table" border="1">
                        <thead>
                            <tr>
                                <th>Pricing Category</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach (get_option('nc_2sc_pricing', array()) as $uniq_key => $pricing_info) { ?>
                                <tr data-pricing_id="<?php echo $uniq_key ?>">
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['pricing_category'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <input type="text" class="nc_pricing_category" placeholder="Pricing Category" value="<?php echo $pricing_info['pricing_category'] ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['colors_from'] ?> - <?php echo $pricing_info['colors_to'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <input type="number" class="nc_colors_from" min="1" placeholder="from" value="<?php echo $pricing_info['colors_from'] ?>" /> - <input type="number" class="nc_colors_to" min="2" placeholder="to" value="<?php echo $pricing_info['colors_to'] ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="nc_text"><?php echo $pricing_info['pricing'] ?></div>
                                        <div class="nc_input" style="display: none">
                                            <input type="number" class="nc_scr_pricing" placeholder="Price" step="any" min="0.01" value="<?php echo $pricing_info['pricing'] ?>" />
                                        </div>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="update_pricing" style="display: none">Done</a>
                                        <a href="javascript:;" class="edit_pricing">Edit</a>
                                        <span>| <a href="javascript:;" class="delete_pricing">Delete</a></span>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th><input type="text" id="nc_pricing_category" placeholder="Pricing Category" /></th>
                                <th>
                                    <input type="number" id="nc_colors_from" min="1" placeholder="from" /> - <input type="number" id="nc_colors_to" min="2" placeholder="to" />
                                </th>
                                <th>
                                    <input type="number" id="nc_scr_pricing" placeholder="Price" step="any" min="0.01" />
                                </th>
                                <th>
                                    <button type="button" id="nc_add_scr_pricing" class="button button-primary" style="width: 100%">Add</button>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
</div>
<script>
    (function ($) {
        var ajax_url = '<?php echo get_admin_url('', 'admin-ajax.php') ?>';
        $(document).ready(function () {
        }).on('click', '#nc_add_scr_pricing', function (e) {
            e.preventDefault();
            //get fields
            {
                var pricing_category_field = $('#nc_pricing_category');
                var colors_from_field = $('#nc_colors_from');
                var colors_to_field = $('#nc_colors_to');
                var pricing_feild = $('#nc_scr_pricing');
            }

            //get values
            {
                var pricing_category = pricing_category_field.val();
                var colors_from = colors_from_field.val();
                var colors_to = colors_to_field.val();
                var pricing = pricing_feild.val();
            }

            //check if all values are entered
            if (pricing_category && colors_from && colors_to && pricing) {
                $.post(ajax_url, {
                    action: 'nc_add_2sc_pricing',
                    pricing_category: pricing_category,
                    colors_from: colors_from,
                    colors_to: colors_to,
                    pricing: pricing,
                }, function () {
                    var pricing_table = $('#nc_scr_pri_pricing_table');
                    var tr = '<tr>';
                    tr += '<td>' + pricing_category + '</td>';
                    tr += '<td>' + colors_from + ' - ' + colors_to + '</td>';
                    tr += '<td>' + pricing + '</td>';
                    tr += '<td>Reload Page to View Actions</td>';
                    tr += '</tr>';
                    pricing_table.find('tbody').append(tr);
                    pricing_table.find('tfoot input').val('');
                });
            }
        }).on('click', '.delete_pricing', function (e) {
            e.preventDefault();
            var conf = confirm('Are you Sure???');
            if (!conf)
                return;
            var el = $(this);
            var old_html = el.html();
            el.html("Please Wait...").prop('disabled', true);
            var tr = el.closest('tr');
            var pricing_id = tr.data('pricing_id');
            $.post(ajax_url, {
                action: 'nc_delete_pricing',
                type: '2sc',
                pricing_id: pricing_id
            }).done(function (data) {
                if (data) {
                    tr.hide(function () {
                        $(this).remove();
                    });
                } else {
                    el.html(old_html).prop('disabled', false);
                    alert("Error: Something went wrong...");
                }
            }).fail(function (error) {
                el.html(old_html).prop('disabled', false);
                alert("Error: Something went wrong...");
            });
        }).on('click', '.edit_pricing', function (e) {
            e.preventDefault();
            var el = $(this);
            el.hide();
            var td = el.closest('td');
            td.find(".update_pricing").show();
            td.find('.delete_pricing').parent().hide();
            var tr = el.closest('tr');
            tr.find('.nc_text').hide();
            tr.find('.nc_input').show();
        }).on('click', '.update_pricing', function (e) {
            e.preventDefault();
            var el = $(this);
            var old_html = el.html();
            el.html("Please Wait...").prop("disabled", true);
            var tr = el.closest('tr');

            //get values
            {
                var pricing_id = tr.data('pricing_id');
                var pricing_category = tr.find('.nc_pricing_category').val();
                var colors_from = tr.find('.nc_colors_from').val();
                var colors_to = tr.find('.nc_colors_to').val();
                var pricing = tr.find('.nc_scr_pricing').val();
            }

            if (pricing_category && colors_from && colors_to && pricing) {
                $.post(ajax_url, {
                    action: 'nc_update_pricing',
                    type: '2sc',
                    pricing_id: pricing_id,
                    pricing_category: pricing_category,
                    colors_from: colors_from,
                    colors_to: colors_to,
                    pricing: pricing
                }).done(function () {
                    el.hide();
                    el.html(old_html).prop("disabled", false);
                    var td = el.closest('td');
                    td.find(".edit_pricing").show();
                    td.find('.delete_pricing').parent().show();
                    tr.find('.nc_text').show();
                    tr.find('.nc_input').hide();
                    //update the view
                    {
                        tr.find('td:nth-child(1) .nc_text').text(pricing_category);
                        tr.find('td:nth-child(2) .nc_text').text(colors_from + ' - ' + colors_to);
                        tr.find('td:nth-child(3) .nc_text').text(pricing);
                    }
                });
            }
        });
    })(jQuery);
</script>