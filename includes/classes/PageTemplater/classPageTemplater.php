<?php

if (!defined('ABSPATH'))
    return;

class nc_PageTemplater {

    /**
     * A reference to an instance of this class.
     */
    private static $instance;

    /**
     * The array of templates that this plugin tracks.
     */
    protected $templates;

    /**
     * Returns an instance of this class.
     */
    public static function getPage($page = 'product_category', $get_id = false) {
        if (!is_string($page))
            return;
        $page = strtolower(trim($page));
        if ($page) {
            global $wpdb;
            $sql = "SELECT * FROM `{$wpdb->postmeta}` WHERE meta_key='_wp_page_template' and meta_value LIKE '%$page%'";
            $page_id = null;
            $page_ids = $wpdb->get_results($sql);
            if ($page_ids) {
                foreach ($page_ids as $_page_id) {
                    if (!$page_id && 'page' === get_post_type($_page_id->post_id)) {
                        $page_id = $_page_id;
                    }
                }
            }
            if ($page_id) {
                //confirm if correct template is received
                {
                    $obj = self::get_instance();
                    $template = $page_id->meta_value;
                    if (!array_key_exists($template, $obj->templates))
                        return;
                }
                $page_id = $page_id->post_id;
                $page_link = get_permalink($page_id);
                return $get_id === TRUE ? $page_id : $page_link;
            }
        }
        return;
    }

    public static function get_instance() {
        if (null == self::$instance) {
            self::$instance = new nc_PageTemplater();
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting filters and administration functions.
     */
    private function __construct() {
        $this->templates = array();
        // Add a filter to the attributes metabox to inject template into the cache.
        add_filter(
                'page_attributes_dropdown_pages_args', array($this, 'register_project_templates')
        );
        // Add a filter to the save post to inject out template into the page cache
        add_filter(
                'wp_insert_post_data', array($this, 'register_project_templates')
        );
        // Add a filter to the template include to determine if the page has our
        // template assigned and return it's path
        add_filter(
                'template_include', array($this, 'view_project_template')
        );

        //remove page editor when page template is selected
        {
//            add_action('init', array($this, 'remove_page_editor'));
        }
        // Add your templates to this array.
        $this->templates = array(
            'nc-product_category-page.php' => 'NC Quotes: Categories Page',
            'nc-single_quote_product.php' => 'NC Quotes: Product Page',
            'nc-quote_final-page.php' => 'NC Quotes: Show Quote',
        );
    }

    /**
     * Remove the Page Editor when the template is selected
     */
    public function remove_page_editor() {
        if (!is_admin()) {
            return;
        }

        // Get the post ID on edit post with filter_input super global inspection.
        $current_post_id = filter_input(INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT);
        // Get the post ID on update post with filter_input super global inspection.
        $update_post_id = filter_input(INPUT_POST, 'post_ID', FILTER_SANITIZE_NUMBER_INT);

        // Check to see if the post ID is set, else return.
        if (isset($current_post_id)) {
            $post_id = absint($current_post_id);
        } else if (isset($update_post_id)) {
            $post_id = absint($update_post_id);
        } else {
            return;
        }

        if (isset($post_id)) {
            // Get the template of the current post.
            $template_file = get_post_meta($post_id, '_wp_page_template', true);

            if (array_key_exists($template_file, $this->templates)) {
                add_action('edit_form_after_title', 'nc_edit_form_after_title');
                remove_post_type_support('page', 'editor');
            }
        }
        //remove_post_type_support( 'page', 'editor' );
    }

    /**
     * Adds our template to the pages cache in order to trick WordPress
     * into thinking the template file exists where it doens't really exist.
     *
     */
    public function register_project_templates($atts) {
        // Create the key used for the themes cache
        $cache_key = 'page_templates-' . md5(get_theme_root() . '/' . get_stylesheet());
        // Retrieve the cache list.
        // If it doesn't exist, or it's empty prepare an array
        $templates = wp_get_theme()->get_page_templates();
        if (empty($templates)) {
            $templates = array();
        }
        // New cache, therefore remove the old one
        wp_cache_delete($cache_key, 'themes');
        // Now add our template to the list of templates by merging our templates
        // with the existing templates array from the cache.
        $templates = array_merge($templates, $this->templates);
        // Add the modified cache to allow WordPress to pick it up for listing
        // available templates
        wp_cache_add($cache_key, $templates, 'themes', 1800);
        return $atts;
    }

    /**
     * Checks if the template is assigned to the page
     */
    public function view_project_template($template) {
        global $post;
        if (!isset($this->templates[get_post_meta($post->ID, '_wp_page_template', true)])) {
            return $template;
        }
        $file = plugin_dir_path(__FILE__) . 'templates/' . get_post_meta($post->ID, '_wp_page_template', true);

        // Just to be safe, we check if the file exist first
        if (file_exists($file))
            return $file;
        else
            echo $file;
        return $template;
    }

}

add_action('plugins_loaded', array('nc_PageTemplater', 'get_instance'));
