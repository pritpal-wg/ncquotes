<?php
$_taxonomy = 'product_category';
$cat_id = isset($_GET['cat_id']) && is_numeric($_GET['cat_id']) && $_GET['cat_id'] > 0 ? intval($_GET['cat_id']) : 0;
if (!$cat_id) {
    wp_redirect(get_site_url());
    die;
}
//check if category is valid
{
    $cat_info = get_term($cat_id, $_taxonomy);
    if (!$cat_info) {
        wp_redirect(get_site_url());
        die;
    }
}
//check if category has children
{
    $children = get_terms($_taxonomy, array(
        'hide_empty' => FALSE,
        'fields' => 'id=>name',
        'parent' => $cat_id,
    ));
    if (empty($children))
        $children = NULL;
}

//check if category has products
{
    $args = array(
        'post_type' => 'products',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => $_taxonomy,
                'field' => 'term_id',
                'terms' => $cat_id,
            )
        ),
    );
    $products = new WP_Query($args);
}

//begin page execution
get_header();
?>
<div id="content">
    <div class="fusion-row">
        <h3>Get a Quote</h3>
    </div>
    <?php
    $parts_dir = dirname(__DIR__) . '/parts/';
    if ($children)
        include_once $parts_dir . 'nc_subcategories.php';
    else
        include_once $parts_dir . 'nc_category-products.php';
    ?>
</div>
<?php get_footer() ?>