<?php
//used values
{
    $_post_type = "products";
    $_quote_post_type = "quotes";
    $_taxonomy = 'product_category';
    $_size_taxonomy = 'product_size';
    $_color_post_type = 'colors';
    $_printing_taxonomy = 'product_printing';
    $_printing_slugs = get_option('_nc_default_printings');
    $_slug_printings = array_flip($_printing_slugs);
}

//is product received?
{
    $prod_id = isset($_GET['prod_id']) && is_numeric($_GET['prod_id']) && $_GET['prod_id'] > 0 ? $_GET['prod_id'] : 0;
    if (!$prod_id) {
        wp_redirect(get_site_url());
        die;
    }
}

//get product
$product = get_post($prod_id);

//is valid product?
{
    if (!$product || $product->post_type !== $_post_type) {
        wp_redirect(get_site_url());
        die;
    }
}

//get data
{
    //get page id
    $prod_page_id = $post->ID;

    //excerpt
    $excerpt = nc_get_the_excerpt($prod_id);

    //get pricings
    $pricings = get_post_meta($prod_id, '_nc_pricings', true);

    //get printings
    {
        $product_printing_ids = get_post_meta($prod_id, '_nc_printings', true);
        if (is_array($product_printing_ids) && !empty($product_printing_ids)) {
            $product_printings = get_terms($_printing_taxonomy, array(
                'hide_empty' => false,
                'fields' => 'id=>name',
                'include' => $product_printing_ids,
            ));
        }
    }

    //get product sizes
    {
        $product_size_ids = isset($pricings['size']) && is_array($pricings['size']) && !empty($pricings['size']) ? array_keys($pricings['size']) : array();
        if (!empty($product_size_ids)) {
            add_filter('get_terms', 'nc_apply_size_order', 10, 2);
            $product_sizes = get_terms($_size_taxonomy, array(
                'hide_empty' => false,
                'fields' => 'id=>name',
                'include' => $product_size_ids,
            ));
            remove_filter('get_terms', 'nc_apply_size_order');
        }
    }

    //get product colors
    {
        $product_color_ids = isset($pricings['color']) && is_array($pricings['color']) && !empty($pricings['color']) ? array_keys($pricings['color']) : array();
        if (!empty($product_color_ids)) {
            $product_colors = get_posts(array(
                'post_type' => $_color_post_type,
                'include' => $product_color_ids,
                'orderby' => 'meta_value_num',
                'meta_key' => 'sort_order',
                'order' => 'ASC'
            ));

            //color id as key
            {
                $temp = array();
                foreach ($product_colors as $product_color)
                    $temp[$product_color->ID] = $product_color;
                $product_colors = $temp;
            }
        }
    }

    //printing_type
    {
        //is printing type selected?
        {
            $printing_type_id = isset($_GET['printing_type']) && is_numeric($_GET['printing_type']) && $_GET['printing_type'] > 0 ? $_GET['printing_type'] : 0;
        }

        //check for valid printing type
        if ($printing_type_id) {
            if (!in_array($printing_type_id, $product_printing_ids)) {
                wp_redirect(get_site_url() . "/?p=$prod_page_id&prod_id=$prod_id");
                die;
            }
            $printing_type_slug = get_term($printing_type_id, $_printing_taxonomy)->slug;
        }
    }

    //color_id
    {
        //is color selected?
        {
            $color_id = $printing_type_id && isset($_GET['color_id']) && is_numeric($_GET['color_id']) && $_GET['color_id'] > 0 ? ($_GET['color_id']) : 0;
        }

        //check for valid color_id
        if ($color_id) {
            if (!in_array($color_id, $product_color_ids)) {
                wp_redirect(get_site_url() . "/?p=$prod_page_id&prod_id=$prod_id&printing_type=$printing_type_id");
                die;
            }
        }
    }

    //size & quantity
    {
        $prod_quantity = $color_id && (isset($_POST['submit']) || isset($_POST['step4_sub'])) ? $_POST['quantity'] : null;
        if (is_array($prod_quantity)) {
            $temp = array();
            foreach ($prod_quantity as $s_id => $qty)
                if (trim($qty) && trim($qty) > 0)
                    $temp[$s_id] = trim($qty);
            $prod_quantity = $temp;
        }
    }
}

//check if final submission
if (isset($_POST['step4_sub'])) {
    //$num_stitches
    {
        $num_stitches = isset($_POST['num_stitches']) ? $_POST['num_stitches'] : '';
        $is_previous_order = isset($_POST['is_previous_order']) ? $_POST['is_previous_order'] : '';
        $print_location = isset($_POST['print_location']) ? $_POST['print_location'] : '';
        $vinyl_personalization = isset($_POST['vinyl_personalization']) ? $_POST['vinyl_personalization'] : '';
        $num_colors_art = isset($_POST['num_colors_art']) ? $_POST['num_colors_art'] : '';
        $is_white_color = isset($_POST['is_white_color']) ? $_POST['is_white_color'] : '';
    }

    //insert new quote
    $new_quote = array(
        'post_content' => '',
        'post_title' => 'Quote - ' . date('Y-m-d H:i:s'),
        'post_status' => 'publish',
        'post_type' => $_quote_post_type,
        'comment_status' => 'closed',
    );
    $new_quote_id = wp_insert_post($new_quote);
    if (is_numeric($new_quote_id) && $new_quote_id > 0) {
        //add metas
        {
            update_post_meta($new_quote_id, '_wp_page_template', 'nc-quote_final-page.php');
            update_post_meta($new_quote_id, 'nc_prod_id', $prod_id);
            update_post_meta($new_quote_id, 'nc_printing_type_id', $printing_type_id);
            update_post_meta($new_quote_id, 'nc_color_id', $color_id);
            update_post_meta($new_quote_id, 'nc_size_quantity', $prod_quantity);
            update_post_meta($new_quote_id, 'nc_num_stitches', $num_stitches);
            update_post_meta($new_quote_id, 'nc_is_previous_order', $is_previous_order);
            update_post_meta($new_quote_id, 'nc_print_location', $print_location);
            update_post_meta($new_quote_id, 'nc_vinyl_personalization', $vinyl_personalization);
            update_post_meta($new_quote_id, 'nc_num_colors_art', $num_colors_art);
            update_post_meta($new_quote_id, 'nc_is_white_color', $is_white_color);
        }

        //hook after adding the quote
        do_action('nc_after_add_quote', $new_quote_id);

        //redirect to quote page
        {
            $quote_final_page_id = nc_PageTemplater::getPage('quote_final', true);
            $quote_url = get_site_url() . "/?p=$quote_final_page_id&id=$new_quote_id";
            wp_redirect($quote_url);
            die;
        }
    }
}

//get the parts url
$parts_dir = dirname(__DIR__) . '/parts/';

//begin page execution
get_header();
?>
<div hidden>
    <h3><?php echo get_the_title($prod_id) ?></h3>
    <?= $excerpt ? '<p>' . $excerpt . '</p>' : '' ?>
    <?php if (isset($product_sizes)) { ?>
        <p><?php echo implode('-', $product_sizes) ?></p>
    <?php } ?>
    <p><?php echo count($product_color_ids) ?> Color<?php echo count($product_color_ids) !== 1 ? 's' : '' ?></p>
</div>
<div class="fusion-clearfix"></div>
<?php
if (!$printing_type_id) {
    //step 1
    include $parts_dir . 'nc_printing_selection.php';
} elseif (!$color_id) {
    //step 2
    include $parts_dir . 'nc_product_color_selection.php';
} elseif (!$prod_quantity) {
    //step 3
    include $parts_dir . 'nc_product_size_quantity_selection.php';
} else {
    //step 4
    include $parts_dir . 'nc_product_decoration_locations.php';
}
?>
<?php get_footer() ?>