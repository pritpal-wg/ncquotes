<?php
//used values
{
    $_post_type = "products";
    $_quote_post_type = "quotes";
    $_taxonomy = 'product_category';
    $_size_taxonomy = 'product_size';
    $_color_post_type = 'colors';
    $_printing_taxonomy = 'product_printing';
    $_printing_slugs = get_option('_nc_default_printings');
    $_slug_printings = array_flip($_printing_slugs);
}

//is quote_id received?
{
    $id = isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0 ? $_GET['id'] : 0;
    if (!$id) {
        wp_redirect(get_site_url());
        die;
    }
}

//get product
$prod_id = get_post_meta($id, 'nc_prod_id', true);
$product = get_post($prod_id);

//get data
{
    //get page id
    $prod_page_id = $post->ID;

    //excerpt
    $excerpt = nc_get_the_excerpt($prod_id);

    //get pricings
    $pricings = apply_filters('nc_get_product_pricings', get_post_meta($prod_id, '_nc_pricings', true));

    //get printings
    {
        $product_printing_ids = get_post_meta($prod_id, '_nc_printings', true);
        if (is_array($product_printing_ids) && !empty($product_printing_ids)) {
            $product_printings = get_terms($_printing_taxonomy, array(
                'hide_empty' => false,
                'fields' => 'id=>name',
                'include' => $product_printing_ids,
            ));
        }
    }

    //get product sizes
    {
        $product_size_ids = isset($pricings['size']) && is_array($pricings['size']) && !empty($pricings['size']) ? array_keys($pricings['size']) : array();
        if (!empty($product_size_ids)) {
            $product_sizes = get_terms($_size_taxonomy, array(
                'hide_empty' => false,
                'fields' => 'id=>name',
                'include' => $product_size_ids,
            ));
        }
    }

    //get product colors
    {
        $product_color_ids = isset($pricings['color']) && is_array($pricings['color']) && !empty($pricings['color']) ? array_keys($pricings['color']) : array();
        if (!empty($product_color_ids)) {
            $product_colors = get_posts(array(
                'post_type' => $_color_post_type,
                'include' => $product_color_ids,
            ));

            //color id as key
            {
                $temp = array();
                foreach ($product_colors as $product_color)
                    $temp[$product_color->ID] = $product_color;
                $product_colors = $temp;
            }
        }
    }

    //printing_type
    {
        $printing_type_id = get_post_meta($id, 'nc_printing_type_id', true);
        $product_printing = $product_printings[$printing_type_id];
        $printing_type_slug = get_term($printing_type_id, $_printing_taxonomy)->slug;
        $printing_type_short = substr($printing_type_slug, 0, 3);
    }

    //printing costs
    {
        $printing_type_setup_cost = get_option("nc_{$printing_type_short}_setup_cost", 0);
        $printing_type_prev_setup_cost = get_option("nc_{$printing_type_short}_prev_setup_cost", 0);
    }

    //color_id
    $color_id = get_post_meta($id, 'nc_color_id', true);
    $is_dark_color = get_post_meta($color_id, 'is_dark_color', true);

    //size & quantity
    $prod_quantity = get_post_meta($id, 'nc_size_quantity', true);

    //num_stitches
    $num_stitches = get_post_meta($id, 'nc_print_location', true);
    if (!is_array($num_stitches) || empty($num_stitches)) {
        $num_stitches = get_post_meta($id, 'nc_num_stitches', true);
    }

    //num_colors_art
    $num_colors_art = get_post_meta($id, 'nc_num_colors_art', true);

    //nc_size_quantity
    $size_quantity = get_post_meta($id, 'nc_size_quantity', true);

    //nc_is_previous_order
    $is_previous_order = get_post_meta($id, 'nc_is_previous_order', true);

    //nc_vinyl_personalization
    $vinyl_personalization = get_post_meta($id, 'nc_vinyl_personalization', true);

    //nc_is_white_color
    $nc_is_white_color = get_post_meta($id, 'nc_is_white_color', true);

    //nc_white_color_print_cost
    $nc_white_color_print_cost = get_option('nc_scr_white_color_cost');

    //nc_scr_pricing
    $nc_scr_pricing = get_option('nc_' . $printing_type_short . '_pricing', array());
    $scr_pricing_cat = get_post_meta($prod_id, '_nc_' . $printing_type_short . '_pricing_cat', true);

    //mega cost
    $mega_cost = 0;
    $tot_items = array_sum($size_quantity);
    $GLOBALS['tot_items'] = $tot_items;
}
$_id = $id;
//get the parts url
$parts_dir = dirname(__DIR__) . '/parts/';

//calculate various prices
{
    //setup string and cost
    {
        $setup_cost = 0;
        $setup_string = '';
        $i = 0;
        if (is_array($num_stitches)) {
            foreach ($num_stitches as $k => $location) {
                $location = trim($location);
                if (!$location)
                    continue;
                $cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                $setup_string .= "$$cost + ";
                $setup_cost += $cost;
                $i++;
            }
            $setup_string = substr($setup_string, 0, -3);
        }
        $setup_string = ($i > 1 ? $setup_string . " = $" . $setup_cost : '$' . $setup_cost);
        $mega_cost+=$setup_cost;
    }

    //decoration cost
    {
        $decoration_cost = 0;
        if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['screen_printing'] || $printing_type_slug === $_printing_slugs['2screen_printing'])) {
            foreach ($num_stitches as $k => $location) {
                $location = trim($location);
                if (!$location)
                    continue;
                $colors = $num_colors_art[$k];
                if ($is_dark_color && !$nc_is_white_color[$k])
                    $colors++;
                $cost = nc_get_scr_pri_cost($nc_scr_pricing, $scr_pricing_cat, $tot_items);
                if ($cost)
                    $cost = $cost * $colors;
                $decoration_cost += ($cost * $tot_items);
            }
        }
        if (is_array($num_stitches) && $printing_type_slug === $_printing_slugs['embroidery']) {
            foreach ($num_stitches as $k => $stitches) {
                if (!$stitches)
                    continue;
                $stitches_cost = nc_get_stitches_cost($stitches, $tot_items);
                $decoration_cost += ($stitches_cost * $tot_items);
            }
        }
        if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['printable_vinyl'] || $printing_type_slug === $_printing_slugs['sublimation'] || $printing_type_slug === $_printing_slugs['hats_embroidery'])) {
            foreach ($num_stitches as $k => $location) {
                if (!$location)
                    continue;
                $location_cost = nc_get_location_cost($location, $tot_items, ($printing_type_slug === $_printing_slugs['printable_vinyl'] ? 'pri' : ($printing_type_slug === $_printing_slugs['hats_embroidery'] ? 'hat' : 'sub')));
                $decoration_cost += ($location_cost * $tot_items);
            }
        }
        if (is_array($num_stitches) && $printing_type_slug === $_printing_slugs['all_drinkware']) {
            foreach ($num_stitches as $k => $location) {
                if (!$location)
                    continue;
                $location_cost = nc_get_all_location_cost($location, $tot_items);
                $decoration_cost += ($location_cost * $tot_items);
            }
        }
        $mega_cost += $decoration_cost;
    }

    //personalization cost
    {
        $personalization_cost = 0;
        if ($vinyl_personalization && $printing_type_slug !== $_printing_slugs['all_drinkware']) {
            $vinyl_prices = get_option('nc_pre_pricing');
            if (is_array($vinyl_prices)) {
                $output = array_filter($vinyl_prices, 'nc_anony_func_2');
                if (empty($output))
                    $output = $vinyl_prices;
                $select_vinyl_price = array_shift($output);
                if (is_array($select_vinyl_price)) {
                    $l_vinyl_personalization = strtolower($vinyl_personalization);
                    if (isset($select_vinyl_price["price_{$l_vinyl_personalization}"])) {
                        $personalization_cost = ($select_vinyl_price["price_{$l_vinyl_personalization}"] * $tot_items);
                    }
                }
            }
        }
        $mega_cost += $personalization_cost;
    }

    //product total cost
    {
        $size_cost = 0;
        foreach ($prod_quantity as $s_id => $s_qty) {
            $size_cost += ($s_qty * $pricings['size'][$s_id]);
        }
        $color_price = isset($pricings['color'][$color_id]) ? $pricings['color'][$color_id] : 0;
        $color_mega_cost = $color_price * $tot_items;
        $prod_tot_cost = $size_cost + $color_mega_cost;
        $mega_cost += $prod_tot_cost;
    }
}
//begin page execution
get_header();
?>
<div>
    <h3>Printable Quote</h3>
    <div>
        <div class="left-col">
            <div>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th>Product</th>
                            <td><?php echo get_the_title($prod_id) ?></td>
                        </tr>
                        <tr>
                            <th>Color</th>
                            <td><?php echo $product_colors[$color_id]->post_title ?></td>
                        </tr>
                        <tr>
                            <th>Decoration Method</th>
                            <td><?php echo $product_printing ?></td>
                        </tr>
                        <tr>
                            <th>Total Items</th>
                            <td><?php echo $tot_items ?></td>
                        </tr>
                        <tr>
                            <th>Setup Cost</th>
                            <td><?php echo $setup_string ?></td>
                        </tr>
                        <tr>
                            <th>Decoration Cost</th>
                            <td><?php echo "$" . ($decoration_cost) ?></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?php if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['screen_printing'] || $printing_type_slug === $_printing_slugs['2screen_printing'])) { ?>
                                    <table class="table table-bordered hidden-xs" style="text-align:left">
                                        <tbody>
                                            <tr>
                                                <th class="left"><b>Location</b></th>
                                                <th class="left"><b>Number of Colors</b></th>
                                                <th class="right"><b>Unit Price</b></th>
                                                <th class="left"><b>Quantity</b></th>
                                                <!--<th class="left"><b>Setup Cost</b></th>-->
                                                <th class="left"><b>Total</b></th>
                                            </tr>
                                            <?php foreach ($num_stitches as $k => $location) { ?>
                                                <?php
                                                $location = trim($location);
                                                if (!$location)
                                                    continue;
                                                $colors = $num_colors_art[$k];
                                                if ($is_dark_color && !$nc_is_white_color[$k])
                                                    $colors++;
//                                            $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                                $cost = nc_get_scr_pri_cost($nc_scr_pricing, $scr_pricing_cat, $tot_items);
                                                ?>
                                                <tr>
                                                    <th><?= $location ?></th>
                                                    <td class="left"><?= $colors ?></td>
                                                    <td class="right"><?= $cost ?></td>
                                                    <td><?= $tot_items ?></td>
                                                    <!--<td><?= "$" . $s_cost ?></td>-->
                                                    <td><?= "$" . (($cost * $tot_items * $colors)) ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <div class="visible-xs">
                                        <table class="table table-bordered table-striped" style="text-align: center">
                                            <tbody>
                                                <?php foreach ($num_stitches as $k => $location) { ?>
                                                    <?php
                                                    $location = trim($location);
                                                    if (!$location)
                                                        continue;
                                                    $colors = $num_colors_art[$k];
                                                    if ($is_dark_color && !$nc_is_white_color[$k])
                                                        $colors++;
                                                    $cost = nc_get_scr_pri_cost($nc_scr_pricing, $scr_pricing_cat, $tot_items);
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <b>Location</b> - <?= $location ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Colors</b> - <?= $colors ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Cost</b> - <?= $cost ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Quantity</b> - <?= $tot_items ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Total Cost</b> - <?= "$" . (($cost * $tot_items * $colors)) ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } ?>
                                <?php if (is_array($num_stitches) && $printing_type_slug === $_printing_slugs['embroidery']) { ?>
                                    <table class="table table-bordered hidden-xs" style="text-align:left">
                                        <thead>
                                            <tr>
                                                <th>Stitches</th>
                                                <th>Cost</th>
                                                <th>Quantity</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($num_stitches as $k => $stitches) { ?>
                                                <?php
                                                if (!$stitches)
                                                    continue;
                                                $stitches_cost = nc_get_stitches_cost($stitches, $tot_items);
//                                            $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                                ?>
                                                <tr>
                                                    <th><?php echo $stitches ?></th>
                                                    <td>$<?php echo $stitches_cost ?></td>
                                                    <td><?php echo $tot_items ?></td>
                                                    <!--<td><?php echo "$" . $s_cost ?></td>-->
                                                    <td>$<?php echo ($tot_items * $stitches_cost) ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <div class="visible-xs">
                                        <table class="table table-bordered table-striped" style="text-align: center">
                                            <tbody>
                                                <?php foreach ($num_stitches as $k => $stitches) { ?>
                                                    <?php
                                                    if (!$stitches)
                                                        continue;
                                                    $stitches_cost = nc_get_stitches_cost($stitches, $tot_items);
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <b>Stitches</b> - <?php echo $stitches ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Cost</b> - $<?php echo $stitches_cost ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Quantity</b> - <?php echo $tot_items ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Total</b> - $<?php echo ($tot_items * $stitches_cost) ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } ?>
                                <?php if (is_array($num_stitches) && ($printing_type_slug === $_printing_slugs['printable_vinyl'] || $printing_type_slug === $_printing_slugs['sublimation'] || $printing_type_slug === $_printing_slugs['hats_embroidery'])) { ?>
                                    <table class="table table-bordered hidden-xs" style="text-align:left">
                                        <thead>
                                            <tr>
                                                <th>Location</th>
                                                <th>Cost</th>
                                                <th>Quantity</th>
                                                <!--<th>Setup Cost</th>-->
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($num_stitches as $k => $location) { ?>
                                                <?php
                                                if (!$location)
                                                    continue;
                                                $location_cost = nc_get_location_cost($location, $tot_items, ($printing_type_slug === $_printing_slugs['printable_vinyl'] ? 'pri' : ($printing_type_slug === $_printing_slugs['hats_embroidery'] ? 'hat' : 'sub')));
//                                            $s_cost = ((float) $is_previous_order[$k] ? $printing_type_prev_setup_cost : $printing_type_setup_cost) * (isset($num_colors_art[$k]) ? $num_colors_art[$k] : 1);
                                                ?>
                                                <tr>
                                                    <th><?php echo $location ?></th>
                                                    <td>$<?php echo $location_cost ?></td>
                                                    <td><?php echo $tot_items ?></td>
                                                    <!--<td><?php echo "$" . $s_cost ?></td>-->
                                                    <td>$<?php echo ($tot_items * $location_cost) ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <div class="visible-xs">
                                        <table class="table table-bordered table-striped" style="text-align: center">
                                            <tbody>
                                                <?php foreach ($num_stitches as $k => $location) { ?>
                                                    <?php
                                                    if (!$location)
                                                        continue;
                                                    $location_cost = nc_get_location_cost($location, $tot_items, ($printing_type_slug === $_printing_slugs['printable_vinyl'] ? 'pri' : ($printing_type_slug === $_printing_slugs['hats_embroidery'] ? 'hat' : 'sub')));
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <b>Location</b> - <?php echo $location ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Cost</b> - $<?php echo $location_cost ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Quantity</b> - <?php echo $tot_items ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Total</b> - $<?php echo ($tot_items * $location_cost) ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } ?>
                                <?php if ($printing_type_slug === $_printing_slugs['all_drinkware']) { ?>
                                    <table class="table table-bordered hidden-xs" style="text-align:left">
                                        <thead>
                                            <tr>
                                                <th>Location</th>
                                                <th>Cost</th>
                                                <th>Quantity</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($num_stitches as $k => $stitches) { ?>
                                                <?php
                                                if (!$stitches)
                                                    continue;
                                                $stitches_cost = nc_get_all_location_cost($stitches, $tot_items);
                                                ?>
                                                <tr>
                                                    <th><?php echo $stitches ?></th>
                                                    <td>$<?php echo $stitches_cost ?></td>
                                                    <td><?php echo $tot_items ?></td>
                                                    <td>$<?php echo ($tot_items * $stitches_cost) ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <div class="visible-xs">
                                        <table class="table table-bordered table-striped" style="text-align: center">
                                            <tbody>
                                                <?php foreach ($num_stitches as $k => $stitches) { ?>
                                                    <?php
                                                    if (!$stitches)
                                                        continue;
                                                    $stitches_cost = nc_get_all_location_cost($stitches, $tot_items);
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <b>Location</b> - <?php echo $stitches ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Cost</b> - $<?php echo $stitches_cost ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Quantity</b> - <?php echo $tot_items ?>
                                                            <hr style="margin: 5px 0" />
                                                            <b>Total</b> - $<?php echo ($tot_items * $stitches_cost) ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php if ($printing_type_slug !== $_printing_slugs['all_drinkware']) { ?>
                            <tr>
                                <th>Personalization Cost</th>
                                <td><?php echo "$" . $personalization_cost ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($vinyl_personalization && $printing_type_slug !== $_printing_slugs['all_drinkware']) { ?>
                            <tr>
                                <td colspan="2">
                                    <table class="table table-bordered hidden-xs">
                                        <thead>
                                            <tr>
                                                <th>Personalization</th>
                                                <th>Cost</th>
                                                <th>Quantity</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $vinyl_personalization ?></td>
                                                <td><?php echo "$" . $select_vinyl_price["price_{$l_vinyl_personalization}"] ?></td>
                                                <td><?php echo $tot_items ?></td>
                                                <td><?php echo "$" . $personalization_cost ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="visible-xs">
                                        <table class="table table-bordered" style="text-align: center">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>Personalization</b> - <?php echo $vinyl_personalization ?>
                                                        <hr style="margin: 5px 0" />
                                                        <b>Cost</b> - <?php echo "$" . $select_vinyl_price["price_{$l_vinyl_personalization}"] ?>
                                                        <hr style="margin: 5px 0" />
                                                        <b>Quantity</b> - <?php echo $tot_items ?>
                                                        <hr style="margin: 5px 0" />
                                                        <b>Total</b> - <?php echo "$" . $personalization_cost ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th colspan="2">Total Cost of Decorated Products</th>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table class="table table-bordered hidden-xs" style="text-align:left">
                                    <thead>
                                        <tr>
                                            <th class="left"><b>Size</b></th>
                                            <th class="right"><b>Unit Price</b></th>
                                            <th class="left"><b>Quantity</b></th>
                                            <th class="right"><b>Total</b></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $avg_dec_cost = round(($decoration_cost + $setup_cost + $personalization_cost) / $tot_items, 2);
                                        ?>
                                        <?php foreach ($pricings['size'] as $s_id => $s_price) { ?>
                                            <?php
                                            $temp = get_term_by('term_id', $s_id, $_size_taxonomy);
                                            $s_name = $temp->name;
                                            $qty = $prod_quantity[$s_id] ? $prod_quantity[$s_id] : 0;
                                            if (!$qty)
                                                continue;
                                            $avg_prod_unit_price = ($color_price + $s_price + $avg_dec_cost);
                                            ?>
                                            <tr>
                                                <td class="left"><?php echo $s_name ?></td>
                                                <td class="center"><?php echo "$" . $avg_prod_unit_price ?></td>
                                                <td class="right"><?php echo $qty ?></td>
                                                <td class="right"><?php echo "$" . ($avg_prod_unit_price * $prod_quantity[$s_id]) ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="visible-xs">
                                    <table class="table table-bordered" style="text-align: center">
                                        <tbody>
                                            <?php
                                            $avg_dec_cost = round(($decoration_cost + $setup_cost + $personalization_cost) / $tot_items, 2);
                                            ?>
                                            <?php foreach ($pricings['size'] as $s_id => $s_price) { ?>
                                                <?php
                                                $temp = get_term_by('term_id', $s_id, $_size_taxonomy);
                                                $s_name = $temp->name;
                                                $qty = $prod_quantity[$s_id] ? $prod_quantity[$s_id] : 0;
                                                if (!$qty)
                                                    continue;
                                                $avg_prod_unit_price = ($color_price + $s_price + $avg_dec_cost);
                                                ?>
                                                <tr>
                                                    <td>
                                                        <b>Size</b> - <?php echo $s_name ?>
                                                        <hr style="margin: 5px 0" />
                                                        <b>Cost</b> - <?php echo "$" . $avg_prod_unit_price ?>
                                                        <hr style="margin: 5px 0" />
                                                        <b>Quantity</b> - <?php echo $qty ?>
                                                        <hr style="margin: 5px 0" />
                                                        <b>Total</b> - <?php echo "$" . ($avg_prod_unit_price * $prod_quantity[$s_id]) ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr hidden>
                            <th>Decoration Cost</th>
                            <td><?php echo "$" . ($decoration_cost + $setup_cost) ?></td>
                        </tr>
                        <?php if ($printing_type_slug !== $_printing_slugs['all_drinkware']) { ?>
                            <tr hidden>
                                <th>Personalization Cost</th>
                                <td><?php echo "$" . $personalization_cost ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th>Total Quote Price</th>
                            <td>
                                <?php echo "$" . $mega_cost ?><br />
                                This does not include shipping or taxes if applicable
                            </td>
                        </tr>
                        <tr class="left_bottom_links">
                            <td colspan="2">Your salesperson will provide a quote if you need Art Services.</td>
                        </tr>
                        <tr class="left_bottom_links">
                            <td colspan="2">
                                <span id="me_email_container" style="float: right">
                                    <span id="mail_link_container">
                                        <a class="js-open-modal fusion-button button-flat button-round button-small button-default" href="javascript:;" data-modal-id="popup1">Email Quote to Us</a>
                                    </span>
                                </span>
                                <a class="fusion-button button-flat button-round button-small button-default" href="javascript:;" onclick="window.print()">Print this Page</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <?php ?>
        </div>
        <div class="right-col">
            <div style="border: 1px solid #eee;padding: 5px">
                <img class="image" src="<?= wp_get_attachment_url(get_post_thumbnail_id($prod_id)) ?>" alt="<?= get_the_title($prod_id) ?>" />
                <?php if ($product->post_content) { ?>
                    <hr />
                    <?php echo $product->post_content ?>
                <?php } ?>
            </div>
            <hr />
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td colspan="2">Your salesperson will provide a quote if you need Art Services.</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span id="me_email_container" style="float: right">
                                <span id="mail_link_container">
                                    <a class="js-open-modal fusion-button button-flat button-round button-small button-default" href="javascript:;" data-modal-id="popup1">Email Quote to Us</a>
                                </span>
                            </span>
                            <a class="fusion-button button-flat button-round button-small button-default" href="javascript:;" onclick="window.print()">Print this Page</a>
                        </td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><a href="mailto:order@nclogowear.com">order@nclogowear.com</a></td>
                    </tr>
                    <tr>
                        <th>Call Us</th>
                        <td>
                            <b>919-821-4646</b>
                            <br />
                            Saturday open 10 am to 12 pm by appointment only please call
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="fusion-clearfix"></div>
        </div>
    </div>
</div>
<div id="popup1" class="modal-box">
    <header> <a href="#" class="js-modal-close close">×</a>
        <h3>Email Quote to Us</h3>
    </header>
    <div class="modal-body">
        <div id="message_d"></div>
        <form id="mail_form_div">
            <div>
                <label>Full Name</label>
                <br/>
                <input class="" type="text" id="quote_full_name"/>
            </div>
            <br/>
            <div>
                <label>Email Address</label>
                <br/>
                <input class="" type="text" id="quote_email"/>
            </div>
            <br/>
            <div>
                <label>Phone Number</label>
                <br/>
                <input class="" type="text" id="quote_phone_number"/>
            </div>
        </form>
    </div>
    <footer>
        <button class="btn btn-small" id="my_email_send_detail"data-quote_id="<?php echo $_id ?>">Send Mail</button>
        <a href="#" class="btn btn-small js-modal-close">Close</a>
    </footer>
</div>
<style>
    .v-center {
        height: 100vh;
        width: 100%;
        display: table;
        position: relative;
        text-align: center;
    }

    .v-center > div {
        display: table-cell;
        vertical-align: middle;
        position: relative;
        top: -10%;
    }

    .btn {
        font-size: 2vmin;
        padding: 0.75em 1.5em;
        background-color: #fff;
        border: 1px solid #bbb;
        color: #333;
        text-decoration: none;
        display: inline;
        border-radius: 4px;
        -webkit-transition: background-color 1s ease;
        -moz-transition: background-color 1s ease;
        transition: background-color 1s ease;
    }

    .btn:hover {
        background-color: #ddd;
        -webkit-transition: background-color 1s ease;
        -moz-transition: background-color 1s ease;
        transition: background-color 1s ease;
    }

    .btn-small {
        padding: .75em 1em;
        font-size: 0.8em;
    }

    .modal-box {
        display: none;
        position: absolute;
        z-index: 1000;
        width: 98%;
        background: white;
        border-bottom: 1px solid #aaa;
        border-radius: 4px;
        box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
        border: 1px solid rgba(0, 0, 0, 0.1);
        background-clip: padding-box;
    }
    @media (min-width: 32em) {

        .modal-box { width: 70%; }
    }

    .modal-box header,
    .modal-box .modal-header {
        padding: 1.25em 1.5em;
        border-bottom: 1px solid #ddd;
    }

    .modal-box header h3,
    .modal-box header h4,
    .modal-box .modal-header h3,
    .modal-box .modal-header h4 { margin: 0; }

    .modal-box .modal-body { padding: 2em 1.5em; }

    .modal-box footer,
    .modal-box .modal-footer {
        padding: 1em;
        border-top: 1px solid #ddd;
        background: rgba(0, 0, 0, 0.02);
        text-align: right;
    }

    .modal-overlay {
        opacity: 0;
        filter: alpha(opacity=0);
        position: absolute;
        top: 0;
        left: 0;
        z-index: 900;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.3) !important;
    }

    a.close {
        line-height: 1;
        font-size: 1.5em;
        position: absolute;
        top: 5%;
        right: 2%;
        text-decoration: none;
        color: #bbb;
    }

    a.close:hover {
        color: #222;
        -webkit-transition: color 1s ease;
        -moz-transition: color 1s ease;
        transition: color 1s ease;
    }
    div#popup1 {
        position: fixed;
        left: 25% !important;
    }
    .modal-box {
        width: 50% !important;
    }
    table {
        //max-width: 100%;
        background-color: transparent;
    }

    th {
        text-align: left;
    }

    .table {
        //width: 100%;
        margin-bottom: 20px;
    }

    .table thead > tr > th,
    .table tbody > tr > th,
    .table tfoot > tr > th,
    .table thead > tr > td,
    .table tbody > tr > td,
    .table tfoot > tr > td {
        padding: 8px;
        line-height: 1.428571429;
        vertical-align: top;
        border-top: 1px solid #dddddd;
    }

    .table thead > tr > th {
        vertical-align: bottom;
    }

    .table caption + thead tr:first-child th,
    .table colgroup + thead tr:first-child th,
    .table thead:first-child tr:first-child th,
    .table caption + thead tr:first-child td,
    .table colgroup + thead tr:first-child td,
    .table thead:first-child tr:first-child td {
        border-top: 0;
    }

    .table tbody + tbody {
        border-top: 2px solid #dddddd;
    }

    .table .table {
        background-color: #ffffff;
    }

    .table-condensed thead > tr > th,
    .table-condensed tbody > tr > th,
    .table-condensed tfoot > tr > th,
    .table-condensed thead > tr > td,
    .table-condensed tbody > tr > td,
    .table-condensed tfoot > tr > td {
        padding: 5px;
    }

    .table-bordered {
        border: 1px solid #dddddd;
    }

    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td {
        border: 1px solid #dddddd;
    }

    .table-striped > tbody > tr:nth-child(odd) > td,
    .table-striped > tbody > tr:nth-child(odd) > th {
        background-color: #f9f9f9;
    }

    .table-hover > tbody > tr:hover > td,
    .table-hover > tbody > tr:hover > th {
        background-color: #f5f5f5;
    }

    table col[class^="col-"] {
        display: table-column;
        float: none;
    }

    table td[class^="col-"],
    table th[class^="col-"] {
        display: table-cell;
        float: none;
    }

    .table > thead > tr > td.active,
    .table > tbody > tr > td.active,
    .table > tfoot > tr > td.active,
    .table > thead > tr > th.active,
    .table > tbody > tr > th.active,
    .table > tfoot > tr > th.active,
    .table > thead > tr.active > td,
    .table > tbody > tr.active > td,
    .table > tfoot > tr.active > td,
    .table > thead > tr.active > th,
    .table > tbody > tr.active > th,
    .table > tfoot > tr.active > th {
        background-color: #f5f5f5;
    }

    .table > thead > tr > td.success,
    .table > tbody > tr > td.success,
    .table > tfoot > tr > td.success,
    .table > thead > tr > th.success,
    .table > tbody > tr > th.success,
    .table > tfoot > tr > th.success,
    .table > thead > tr.success > td,
    .table > tbody > tr.success > td,
    .table > tfoot > tr.success > td,
    .table > thead > tr.success > th,
    .table > tbody > tr.success > th,
    .table > tfoot > tr.success > th {
        background-color: #dff0d8;
        border-color: #d6e9c6;
    }

    .table > thead > tr > td.danger,
    .table > tbody > tr > td.danger,
    .table > tfoot > tr > td.danger,
    .table > thead > tr > th.danger,
    .table > tbody > tr > th.danger,
    .table > tfoot > tr > th.danger,
    .table > thead > tr.danger > td,
    .table > tbody > tr.danger > td,
    .table > tfoot > tr.danger > td,
    .table > thead > tr.danger > th,
    .table > tbody > tr.danger > th,
    .table > tfoot > tr.danger > th {
        background-color: #f2dede;
        border-color: #eed3d7;
    }

    .table > thead > tr > td.warning,
    .table > tbody > tr > td.warning,
    .table > tfoot > tr > td.warning,
    .table > thead > tr > th.warning,
    .table > tbody > tr > th.warning,
    .table > tfoot > tr > th.warning,
    .table > thead > tr.warning > td,
    .table > tbody > tr.warning > td,
    .table > tfoot > tr.warning > td,
    .table > thead > tr.warning > th,
    .table > tbody > tr.warning > th,
    .table > tfoot > tr.warning > th {
        background-color: #fcf8e3;
        border-color: #fbeed5;
    }

    .table-hover > tbody > tr > td.success:hover,
    .table-hover > tbody > tr > th.success:hover,
    .table-hover > tbody > tr.success:hover > td {
        background-color: #d0e9c6;
        border-color: #c9e2b3;
    }

    .table-hover > tbody > tr > td.danger:hover,
    .table-hover > tbody > tr > th.danger:hover,
    .table-hover > tbody > tr.danger:hover > td {
        background-color: #ebcccc;
        border-color: #e6c1c7;
    }

    .table-hover > tbody > tr > td.warning:hover,
    .table-hover > tbody > tr > th.warning:hover,
    .table-hover > tbody > tr.warning:hover > td {
        background-color: #faf2cc;
        border-color: #f8e5be;
    }
    .left-col {
        float: left;
        width: 49%;
    }
    .right-col {
        float: left;
        width: 49%;
        margin-left: 2%;
    }
    .left_bottom_links {
        display: none;
    }
    @media (max-width: 660px) {
        .left-col {
            width: 100%;
        }
        .right-col {
            display: none;
        }
        .left_bottom_links {
            display: table-row;
        }
    }
    @media (max-width: 670px) {
        .fusion-button.button-small {
            padding: 9px;
        }
    }
</style>
<script>
    if (jQuery) {
        (function ($) {
            $(document).ready(function () {
                $(function () {

                    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");

                    $('a[data-modal-id]').click(function (e) {
                        e.preventDefault();
                        $("body").append(appendthis);
                        $(".modal-overlay").fadeTo(500, 0.7);
                        //$(".js-modalbox").fadeIn(500);
                        var modalBox = $(this).attr('data-modal-id');
                        $('#' + modalBox).fadeIn($(this).data());
                    });


                    $(".js-modal-close, .modal-overlay").click(function () {
                        $(".modal-box, .modal-overlay").fadeOut(500, function () {
                            $(".modal-overlay").remove();
                        });

                    });

                    $(window).resize(function () {
                        $(".modal-box").css({
                            top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
                            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
                        });
                    });

                    $(window).resize();

                });
                $('#me_email_container').on('click', '#me_email_link_container a', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $('#me_email_form').show();
                    $('#me_email_link_container').hide();
                });
                $(document).on('click', '#my_email_send_detail', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var full_name = $('#quote_full_name').val();
                    var email = $('#quote_email').val();
                    var phone = $('#quote_phone_number').val();
                    if (!email)
                        return;
                    var quote_id = $(this).data('quote_id');
                    $.post('<?php echo get_admin_url() . 'admin-ajax.php' ?>', {
                        action: 'nc_send_user_quote_email',
                        quote_id: quote_id,
                        email: email,
                        full_name: full_name,
                        phone: phone,
                    }, function () {
                        $(document).find('#message_d').html('| You will shorty receive an email...');
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    });

                });
            });
        })(jQuery);
    } else {
        document.getElementById('me_email_container').innerHTML = '';
    }
</script>
<?php get_footer() ?>