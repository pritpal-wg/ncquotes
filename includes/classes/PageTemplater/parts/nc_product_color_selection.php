<div>
    <?php // pr($product_colors) ?>
    <h3><strong>Step 2. Choose Your Apparel Color</strong></h3>
    <form action="<?php echo site_url() ?>">
        <input type="hidden" name="p" value="<?php echo $prod_page_id ?>" />
        <input type="hidden" name="prod_id" value="<?= $prod_id ?>" />
        <input type="hidden" name="printing_type" value="<?= $printing_type_id ?>" />
        <?php
        $k = 0;
        ?>
        <?php foreach ($product_colors as $product_color) { ?>
            <?php
            $product_color_id = $product_color->ID;
            $product_color_name = $product_color->post_title;
            $product_color_code = '#' . get_post_meta($product_color_id, 'color', true);
            ?>
            <ul class="width-box-st">
                <li>
                    <label>
                        <input type="radio" name="color_id" value="<?php echo $product_color_id ?>"<?= !$k++ ? ' checked' : '' ?> />
                        <span class="width-20" style="background-color: <?php echo $product_color_code ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <?php echo $product_color_name ?>
                    </label>
                </li>
            </ul>
        <?php } ?>
        <div class="fusion-clearfix"></div>
        <input class="fusion-button button-flat button-round button-large button-default button-27" type="submit" value="Submit" />
    </form>
</div>
<style>
    .width-box-st {
        min-width: 350px;
        list-style-type: none;
        display: block;
        width: 30%;
        overflow: hidden;
        margin-bottom: 5px;
        float: left;
        margin: 20px 0;
    }

    .width-box-st li {
        float: left;
        margin: 5px;
        color: #333;
        font-size: 16px;
        padding: 0 5px;
    }

    .width-box-st li label {
        font-weight: normal;
    }

    .width-box-st li input {
        margin-top: -1px;
        cursor: pointer;
        margin-right: 15px;
    }

    .width-box-st li .width-20 {
        width: 40px;
        height: 33px;
        margin-right: 10px;
        display: inline-block;
        line-height: 28px;
        border: 2px solid #508ccb;
        -webkit-transition: all 0.2s ease;
        -moz-transition: all 0.2s ease;
        -o-transition: all 0.2s ease;
        transition: all 0.2s ease;
    }

    .width-box-st:hover li .width-20 {
        width: 60px;
    }

    .width-box-st:hover label {
        cursor: pointer;
    }
</style>