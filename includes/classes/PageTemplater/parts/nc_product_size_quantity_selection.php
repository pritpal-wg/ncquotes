<?php
$printing_type_short = substr($printing_type_slug, 0, 3);
$min_order_quantity = get_option("nc_{$printing_type_short}_min_order_quantity", 1);
?>
<div class="bg-flot-rt">
    <h3><strong>Step 3. Choose Your Sizes and Quantities</strong></h3>
</div>
<?php if (has_post_thumbnail($prod_id)) { ?>
    <div style="float: right">
        <?php echo get_the_post_thumbnail($prod_id, 'large') ?>
    </div>
<?php } ?>
<form method="post" name="product_quantity_form">
    <div style="margin-top: 20px">
        Selected Color : <b><?php echo $product_colors[$color_id]->post_title ?></b>
        <table border="1">
            <thead>
                <tr>
                    <th style="padding: 0 5px">Size</th>
                    <th style="padding: 0 5px">Quantity</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($product_sizes as $product_size_id => $product_size) { ?>
                    <tr>
                        <th style="padding: 0 5px"><?php echo $product_size ?></th>
                        <td style="padding: 1px"><input type="number" step="1" class="prod_quantity" name="quantity[<?php echo $product_size_id ?>]" min="1" /></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div style="margin-top: 20px">
        <input class="fusion-button button-flat button-round button-large button-default button-27" type="submit" name="submit" value="Submit" />
    </div>
</form>
<script>
    (function ($) {
        $(document).on('submit', 'form[name="product_quantity_form"]', function (e) {
            var min_order_quantity = '<?php echo $min_order_quantity ?>';
            if (!isNaN(min_order_quantity)) {
                var prod_quantity = 0;
                $('.prod_quantity').each(function () {
                    var qty = $(this).val();
                    prod_quantity += Number(qty);
                });
                if (prod_quantity < min_order_quantity) {
                    e.preventDefault();
                    alert("Minimum Order Quantity is: " + min_order_quantity);
                }
            }
        });
    })(jQuery);
</script>