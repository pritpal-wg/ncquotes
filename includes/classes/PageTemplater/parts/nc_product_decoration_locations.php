<?php
//short name for printing type
$printing_type_short = substr($printing_type_slug, 0, 3);

//number of print locations
$nc_num_print_locations = get_option("nc_{$printing_type_short}_num_print_locations", 4);

//printing locations
{
    $nc_print_locations = get_option("nc_{$printing_type_short}_print_locations");
    if ($nc_print_locations) {
        $nc_print_locations = array_map('trim', explode(',', $nc_print_locations));
    } else
        $nc_print_locations = array();
}

//nc_allow_vinyl_personalization
$nc_allow_vinyl_personalization = get_option("nc_{$printing_type_short}_allow_vinyl_personalization", 0);

//nc_num_colors_art
$nc_num_colors_art = get_option("nc_{$printing_type_short}_num_colors_art", 12);

//number of stitches
{
    $num_stitches = array();
    $nc_emb_pricings = get_option('nc_emb_pricing');
    if (is_array($nc_emb_pricings))
        foreach ($nc_emb_pricings as $nc_emb_pricing)
            if (!in_array($nc_emb_pricing['stitches'], $num_stitches))
                $num_stitches[] = $nc_emb_pricing['stitches'];
}

//page to show final quote
$quote_final_page = nc_PageTemplater::getPage('quote_final');
?>
<h3><strong>Step 4. Choose Your Decoration Location: </strong></h3>
<div id="ScreenPrintingLocations">
    <form method="post" class="visible-xs">
        <div>
            <?php foreach ($prod_quantity as $s_id => $s_qty) { ?>
                <input type="hidden" name="quantity[<?php echo $s_id ?>]" value="<?php echo $s_qty ?>" />
            <?php } ?>
            <?php if ($printing_type_slug === $_printing_slugs['embroidery']) { ?>
                <table class="table table-condensed other-data">
                    <tbody>
                        <?php for ($i = 1; $i <= $nc_num_print_locations; $i++) { ?>
                            <tr>
                                <td>
                                    Location <?= $i ?>.
                                    <hr style="margin: 5px 0" />
                                    # of Stitches<br />
                                    <select name="num_stitches[<?= $i ?>]">
                                        <?php echo $i > 1 ? '<option value=""></option>' : '' ?>
                                        <?php foreach ($num_stitches as $stitches) { ?>
                                            <option><?= $stitches ?></option>
                                        <?php } ?>
                                    </select>
                                    <hr style="margin: 5px 0" />
                                    Is this location on file from previous order?<br />
                                    <select name="is_previous_order[<?= $i ?>]">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } elseif ($printing_type_slug === $_printing_slugs['screen_printing'] || $printing_type_slug === $_printing_slugs['2screen_printing']) { ?>
                <?php
                $is_dark_color = get_post_meta($color_id, 'is_dark_color', true);
                ?>
                <table class="table table-condensed other-data">
                    <tbody>
                        <?php for ($i = 1; $i <= $nc_num_print_locations; $i++) { ?>
                            <tr>
                                <td>
                                    Location <?= $i ?>.<br />
                                    <select name="print_location[<?= $i ?>]">
                                        <?php echo $i > 1 ? '<option value=""></option>' : '' ?>
                                        <?php foreach ($nc_print_locations as $nc_print_location) { ?>
                                            <option><?= $nc_print_location ?></option>
                                        <?php } ?>
                                    </select>
                                    <hr style="margin: 5px 0" />
                                    # of Colors for Art<br />
                                    <select name="num_colors_art[<?= $i ?>]">
                                        <?php for ($j = 1; $j <= $nc_num_colors_art; $j++) { ?>
                                            <option><?= $j ?></option>
                                        <?php } ?>
                                    </select>
                                    <hr style="margin: 5px 0" />
                                    <?php if ($is_dark_color) { ?>
                                        Is there any white in the design?<br />
                                        <select name="is_white_color[<?= $i ?>]">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                        <hr style="margin: 5px 0" />
                                    <?php } ?>
                                    Is this location on file from previous order?<br />
                                    <select name="is_previous_order[<?= $i ?>]">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if ($nc_allow_vinyl_personalization) { ?>
                            <tr>
                                <td>
                                    Location <?= $i ?>.<br />
                                    Vinyl Personalization<br />
                                    <select name="vinyl_personalization">
                                        <option value=""></option>
                                        <option>Numbers</option>
                                        <option>Names</option>
                                        <option>Both</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } elseif ($printing_type_slug === $_printing_slugs['all_drinkware']) { ?>
                <table class="table table-condensed other-data">
                    <tbody>
                        <?php for ($i = 1; $i <= $nc_num_print_locations; $i++) { ?>
                            <tr>
                                <td>
                                    Location <?= $i ?>.<br />
                                    <select name="print_location[<?= $i ?>]">
                                        <?php echo $i > 1 ? '<option value=""></option>' : '' ?>
                                        <?php foreach ($nc_print_locations as $nc_print_location) { ?>
                                            <option><?= $nc_print_location ?></option>
                                        <?php } ?>
                                    </select>
                                    <hr style="margin: 5px 0" />
                                    Is this location on file from previous order?<br />
                                    <select name="is_previous_order[<?= $i ?>]" style="margin-left: 10px">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } else { ?>
                <table class="table table-condensed other-data">
                    <tbody>
                        <?php for ($i = 1; $i <= $nc_num_print_locations; $i++) { ?>
                            <tr>
                                <td>
                                    Location <?= $i ?>.<br />
                                    <select name="print_location[<?= $i ?>]">
                                        <?php echo $i > 1 ? '<option value=""></option>' : '' ?>
                                        <?php foreach ($nc_print_locations as $nc_print_location) { ?>
                                            <option><?= $nc_print_location ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if ($nc_allow_vinyl_personalization) { ?>
                            <tr>
                                <td>
                                    Vinyl Personalization<br />
                                    <select name="vinyl_personalization">
                                        <option value=""></option>
                                        <option>Numbers</option>
                                        <option>Names</option>
                                        <option>Both</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <input class="fusion-button button-flat button-round button-large button-default button-27" type="submit" name="step4_sub" value="Submit" />
    </form>
    <form method="post" class="visible-sm visible-md visible-lg">
        <div class="table-responsive">
            <?php foreach ($prod_quantity as $s_id => $s_qty) { ?>
                <input type="hidden" name="quantity[<?php echo $s_id ?>]" value="<?php echo $s_qty ?>" />
            <?php } ?>
            <?php if ($printing_type_slug === $_printing_slugs['embroidery']) { ?>
                <table class="table table-condensed other-data">
                    <tbody>
                        <?php for ($i = 1; $i <= $nc_num_print_locations; $i++) { ?>
                            <tr>
                                <td>
                                    Location <?= $i ?>.
                                </td>
                                <td>
                                    # of Stitches
                                    <select name="num_stitches[<?= $i ?>]" style="margin-left: 10px">
                                        <?php echo $i > 1 ? '<option value=""></option>' : '' ?>
                                        <?php foreach ($num_stitches as $stitches) { ?>
                                            <option><?= $stitches ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    Is this location on file from previous order?
                                    <select name="is_previous_order[<?= $i ?>]" style="margin-left: 10px">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } elseif ($printing_type_slug === $_printing_slugs['screen_printing'] || $printing_type_slug === $_printing_slugs['2screen_printing']) { ?>
                <?php
                $is_dark_color = get_post_meta($color_id, 'is_dark_color', true);
                ?>
                <table class="table table-condensed other-data">
                    <tbody>
                        <?php for ($i = 1; $i <= $nc_num_print_locations; $i++) { ?>
                            <tr>
                                <td>
                                    Location <?= $i ?>.
                                </td>
                                <td>
                                    <select name="print_location[<?= $i ?>]" style="margin-left: 10px">
                                        <?php echo $i > 1 ? '<option value=""></option>' : '' ?>
                                        <?php foreach ($nc_print_locations as $nc_print_location) { ?>
                                            <option><?= $nc_print_location ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    # of Colors for Art
                                    <select name="num_colors_art[<?= $i ?>]" style="margin-left: 10px">
                                        <?php for ($j = 1; $j <= $nc_num_colors_art; $j++) { ?>
                                            <option><?= $j ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <?php if ($is_dark_color) { ?>
                                    <td>
                                        Is there any white in the design?
                                        <select name="is_white_color[<?= $i ?>]" style="margin-left: 10px">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </td>
                                <?php } ?>
                                <td>
                                    Is this location on file from previous order?
                                    <select name="is_previous_order[<?= $i ?>]" style="margin-left: 10px">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if ($nc_allow_vinyl_personalization) { ?>
                            <tr>
                                <td>
                                    Location <?= $i ?>.
                                </td>
                                <td colspan="<?php echo $is_dark_color ? 4 : 3 ?>">
                                    Vinyl Personalization
                                    <select name="vinyl_personalization" style="margin-left: 10px">
                                        <option value=""></option>
                                        <option>Numbers</option>
                                        <option>Names</option>
                                        <option>Both</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } elseif ($printing_type_slug === $_printing_slugs['all_drinkware']) { ?>
                <table class="table table-condensed other-data">
                    <tbody>
                        <?php for ($i = 1; $i <= $nc_num_print_locations; $i++) { ?>
                            <tr>
                                <td>
                                    Location <?= $i ?>.
                                </td>
                                <td>
                                    <select name="print_location[<?= $i ?>]">
                                        <?php echo $i > 1 ? '<option value=""></option>' : '' ?>
                                        <?php foreach ($nc_print_locations as $nc_print_location) { ?>
                                            <option><?= $nc_print_location ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    Is this location on file from previous order?
                                    <select name="is_previous_order[<?= $i ?>]" style="margin-left: 10px">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } else { ?>
                <table class="table table-condensed other-data">
                    <tbody>
                        <?php for ($i = 1; $i <= $nc_num_print_locations; $i++) { ?>
                            <tr>
                                <td>Location <?= $i ?>.</td>
                                <td>
                                    <select name="print_location[<?= $i ?>]">
                                        <?php echo $i > 1 ? '<option value=""></option>' : '' ?>
                                        <?php foreach ($nc_print_locations as $nc_print_location) { ?>
                                            <option><?= $nc_print_location ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if ($nc_allow_vinyl_personalization) { ?>
                            <tr>
                                <td style="padding: 10px 0">Vinyl Personalization</td>
                                <td>
                                    <select name="vinyl_personalization">
                                        <option value=""></option>
                                        <option>Numbers</option>
                                        <option>Names</option>
                                        <option>Both</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <input class="fusion-button button-flat button-round button-large button-default button-27" type="submit" name="step4_sub" value="Submit" />
    </form>
</div>
<style>
    .other-data tr {
        border-bottom: 1px solid #ddd;
        padding: 2px 5px;
    }

    .other-data tr:nth-child(2n+1) {
        background: #f2f2f2;
    }
</style>