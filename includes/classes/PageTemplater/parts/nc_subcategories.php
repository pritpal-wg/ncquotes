<?php
$cat_page_id = get_the_ID();
$cat_page_url = get_site_url() . "/?p=$cat_page_id";
$i = 0;
?>
<div class="fusion-row">
    <h4>Choose a Sub-Category in <b><?php echo $cat_info->name ?></b></h4>
</div>
<div class="fusion-row">
    <?php foreach ($children as $child_id => $child_name) { ?>
        <?php
        $child_url = $cat_page_url . "&cat_id=$child_id";
        ?>
        <?php $i++ ?>
        <div class="fusion-one-fourth fusion-layout-column fusion-spacing-yes<?php echo $i % 4 === 0 ? ' fusion-column-last' : '' ?>">
            <div class="fusion-column-wrapper">
                <div class="border-lf">
                    <a href="<?= $child_url ?>">
                        <img src="<?php echo nc_taxonomy_image_url($child_id, 'medium', TRUE) ?>" />
                    </a>
                </div>
                <center>
                    <a href="<?= $child_url ?>"><?php echo $child_name ?></a>
                </center>
            </div>
        </div>
    <?php } ?>
</div>
<style>
    .border-lf {
        border: 1px solid #ddd;
    }

    .border-lf:hover {
        border-color: #508ccb;
        cursor: pointer;
    }
</style>