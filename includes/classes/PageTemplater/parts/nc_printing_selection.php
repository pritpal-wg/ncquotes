<?php if (isset($product_printings)) { ?>
    <div>
        <h3><strong>Step 1. Choose Your Decoration Method</strong></h3>
        <p>
            Orders may only use one decoration method (screen printing, embroidery, etc.). If more than one method is needed, please contact our sales dept.
            <br />
            Need help choosing the right decoration method? <a href="http://www.nclogowear.com/wp-content/uploads/2015/06/ProAndCons-1.jpeg" target="_blank">Click Here For More Information</a>
            <br />
        </p>
        <h4>Printing Method :</h4>
        <form action="<?php echo site_url() ?>">
            <input type="hidden" name="p" value="<?php echo $prod_page_id ?>" />
            <input type="hidden" name="prod_id" value="<?= $prod_id ?>" />
            <select name="printing_type">
                <?php foreach ($product_printings as $printing_id => $printing_name) { ?>
                    <option label="<?= $printing_name ?>" value="<?= $printing_id ?>"><?= $printing_name ?></option>
                <?php } ?>
            </select>
            <input class="fusion-button button-flat button-round button-small button-default button-27" type="submit" value="Next Step" />
        </form>
    </div>
<?php } else { ?>
    <div class="fusion-alert alert error alert-danger alert-shadow">
        <span class="alert-icon"><i class="fa fa-lg fa-exclamation-triangle"></i></span>
        <strong>Sorry,</strong> There are no Printing options for this Product.
    </div>
    <button class="fusion-button button-flat button-round button-xlarge button-default button-27" onclick="history.back();">
        <span class="fusion-button-text">Back to Previous Page</span>
    </button>
<?php } ?>
