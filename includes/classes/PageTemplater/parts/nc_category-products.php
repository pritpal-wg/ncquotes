<?php
$page_id = nc_PageTemplater::getPage('single_quote_product', true);
if ($page_id) {
    $prod_page_url = get_site_url() . "/?p=$page_id";
}
$i = 0;
?>
<?php if ($products->have_posts()) { ?>
    <div class="fusion-row">
        <h4>Choose a Product in <b><?php echo $cat_info->name ?></b></h4>
    </div>
    <div class="fusion-row">
        <?php while ($products->have_posts()) { ?>
            <?php
            $products->the_post();
            $prod_id = $post->ID;
            $prod_url = isset($prod_page_url) ? $prod_page_url . "&prod_id=$prod_id" : get_the_permalink();
            ?>
            <?php $i++ ?>
            <div class="fusion-one-fourth fusion-layout-column fusion-spacing-yes<?php echo $i % 4 === 0 ? ' fusion-column-last' : '' ?>">
                <div class="fusion-column-wrapper">
                    <a href="<?= $prod_url ?>"><div class="border-lf"><?php the_post_thumbnail() ?></div></a>
                    <a href="<?= $prod_url ?>"><div><?php the_title() ?></div></a>
                    <?php echo nc_get_the_excerpt($prod_id) ?>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="fusion-row">
        <div class="fusion-alert alert error alert-danger alert-shadow">
            <span class="alert-icon"><i class="fa fa-lg fa-exclamation-triangle"></i></span>
            <strong>Sorry</strong> There is not any Product in Category: <b><?php echo $cat_info->name ?></b>
        </div>
        <button class="fusion-button button-flat button-round button-xlarge button-default button-27" onclick="history.back();">
            <span class="fusion-button-text">Back to Previous Page</span>
        </button>
    </div>
<?php } ?>
<style>
    .border-lf {
        border: 1px solid #ddd;
    }

    .border-lf:hover {
        border-color: #508ccb;
        cursor: pointer;
    }

    a:hover {
        color: red;
        text-decoration: underline;
    }
</style>