<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/public
 * @author     Your Name <email@example.com>
 */
class Ncquote_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @var      string    $plugin_name       The name of the plugin.
     * @var      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Plugin_Name_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Plugin_Name_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name . '-bootstrap', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/css/bootstrap.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name . '-weather-icons', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/css/ncquote-icons.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name . '-weather-styles', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/css/ncquote-styles.css', array(), $this->version, 'all');
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Plugin_Name_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Plugin_Name_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->plugin_name . '-modernizr', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/js/modernizr-2.6.2-respond-1.1.0.min.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . '-bootstrap', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/js/bootstrap.js', array('jquery'), $this->version, true);
//        wp_enqueue_script($this->plugin_name . '-jquery', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/js/jquery-1.10.1.min.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . '-typeahead', plugin_dir_url(dirname(dirname(__FILE__))) . "public/js/bootstrap-typeahead.js", array('jquery'), $this->version, false);
        //wp_enqueue_script( $this->plugin_name.'-custom', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/js/custom_jquery.js', array( 'jquery' ), $this->version, false );	
//        wp_enqueue_script($this->plugin_name . '-weatherscript', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/js/weatherscript.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . '-mousewheel', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/js/jquery.mousewheel.js', array('jquery'), $this->version, false);
    }

}
