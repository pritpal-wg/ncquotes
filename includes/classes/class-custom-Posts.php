<?php

/* * ***********color Product type Start Here********** */

function create_product_posttype() {

    $labels = array(
        'name' => _x('Products', 'Post Type General Name', 'products'),
        'singular_name' => _x('Products', 'Post Type Singular Name', 'products'),
        'menu_name' => __('Products', 'ncquotes'),
        'all_items' => __('All Products', 'ncquotes'),
        'view_item' => __('View Products', 'ncquotes'),
        'add_new_item' => __('Add New Products', 'ncquotes'),
        'add_new' => __('Add New Products', 'ncquotes'),
        'edit_item' => __('Edit Products', 'ncquotes'),
        'update_item' => __('Update Products', 'ncquotes'),
        'search_items' => __('Search Products', 'ncquotes'),
        'not_found' => __('Not found', 'ncquotes'),
        'not_found_in_trash' => __('Not found in Trash', 'ncquotes'),
    );
    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'ncquotes'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'products'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt')
    );
    register_post_type('products', $args);
}

add_action('init', 'create_product_posttype', 0);

/* * ***********Products post type End Here********** */



/* * ***********color post type Start Here********** */

function create_color_posttype() {

    $labels = array(
        'name' => _x('Colors', 'Post Type General Name', 'colors'),
        'singular_name' => _x('Colors', 'Post Type Singular Name', 'colors'),
        'menu_name' => __('Colors', 'ncquotes'),
        'all_items' => __('All Colors', 'ncquotes'),
        'view_item' => __('View Colors', 'ncquotes'),
        'add_new_item' => __('Add New Colors', 'ncquotes'),
        'add_new' => __('Add New Colors', 'ncquotes'),
        'edit_item' => __('Edit Colors', 'ncquotes'),
        'update_item' => __('Update Colors', 'ncquotes'),
        'search_items' => __('Search Colors', 'ncquotes'),
        'not_found' => __('Not found', 'ncquotes'),
        'not_found_in_trash' => __('Not found in Trash', 'ncquotes'),
    );
    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'ncquotes'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'colors'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title')
    );
    register_post_type('colors', $args);
}

add_action('init', 'create_color_posttype', 0);


/* * ***********Quotes post type Start Here********** */

function create_quotes_posttype() {

    $labels = array(
        'name' => _x('Quotes', 'Post Type General Name', 'quotes'),
        'singular_name' => _x('Quotes', 'Post Type Singular Name', 'quotes'),
        'menu_name' => __('Quotes', 'ncquotes'),
        'all_items' => __('All Quotes', 'ncquotes'),
        'view_item' => __('View Quotes', 'ncquotes'),
        'add_new_item' => __('Add New Quotes', 'ncquotes'),
        'add_new' => __('Add New Quotes', 'ncquotes'),
        'edit_item' => __('Edit Quotes', 'ncquotes'),
        'update_item' => __('Update Quotes', 'ncquotes'),
        'search_items' => __('Search Quotes', 'ncquotes'),
        'not_found' => __('Not found', 'ncquotes'),
        'not_found_in_trash' => __('Not found in Trash', 'ncquotes'),
    );
    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'ncquotes'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => false,
        'query_var' => true,
        'rewrite' => array('slug' => 'quotes'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    );
    register_post_type('Quotes', $args);
}

add_action('init', 'create_quotes_posttype', 0);

/*************printing post type End Here***********/         
         
         
  