<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */
/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */

/**
 * Calls the class on the post edit screen.
 */
function call_CodesCustomFeilds() {
    new codesCustomFeilds();
}

if (is_admin()) {
    add_action('load-post.php', 'call_CodesCustomFeilds');
    add_action('load-post-new.php', 'call_CodesCustomFeilds');
}

$prefix = 'wh';

/**
 * The Class.
 */
class codesCustomFeilds {

    /**
     * Hook into the appropriate actions when the class is constructed.
     */
    public function __construct() {

        add_action('add_meta_boxes', array($this, 'add_products_meta_boxes'));
        add_action('save_post', array($this, 'save_products_meta_boxes'));
//        add_action('add_meta_boxes', array($this, 'add_category_box'));
//        add_action('add_meta_boxes', array($this, 'add_subcat_box'));
//        add_action('add_meta_boxes', array($this, 'get_all_subcat_color_box'));
        add_action('add_meta_boxes', array($this, 'add_size_box'));
        add_action('add_meta_boxes', array($this, 'add_color_box'));
        add_action('save_post', array($this, 'save_color_box'));
    }

    /**
     * Adds the meta box container.
     */
    public function add_products_meta_boxes($post_type) {
        if (in_array($post_type, array(
                    'products',
                ))) {
            add_meta_box(
                    'nc_products_meta_box'
                    , __('Options', 'ncquotes')
                    , array($this, 'render_products_meta_box')
            );
        }
    }

    public function render_products_meta_box($post) {

        //get raw values
        {
            //get printing types
            {
                $printing_types = get_terms('product_printing', array(
                    'hide_empty' => FALSE,
                    'fields' => 'id=>name',
                ));
            }

            //get all sizes
            {
                $sizes = get_terms('product_size', array(
                    'hide_empty' => FALSE,
                    'fields' => 'id=>name',
                ));
            }

            //get all colors
            {
                $args = array(
                    'post_type' => 'colors',
                    'posts_per_page' => -1,
                    'orderby' => 'post_title',
                    'order' => 'ASC',
                );
                $colors = get_posts($args);
                wp_reset_query();
                if (!empty($colors)) {
                    $temp = array();
                    foreach ($colors as $k => $color) {
                        $temp[$color->ID] = get_post_meta($color->ID, 'color', true);
                    }
                    $colors = $temp;
                }
            }
        }
        $minimum_order_quantity = get_post_meta($post->ID, '_minimum_order_quantity', true);
        $nc_printings = get_post_meta($post->ID, '_nc_printings', true);
        $nc_pricings = get_post_meta($post->ID, '_nc_pricings', true);
        $nc_product_tax = get_post_meta($post->ID, '_nc_product_tax', true);
        $nc_scr_pricing_cat = get_post_meta($post->ID, '_nc_scr_pricing_cat', true);
        ob_start();
        // Add an nonce field so we can check for it later.
        wp_nonce_field('nc_render_products_meta_box_nonce', 'nc_render_products_meta_box_nonce');
        ?>
        <div style="margin-bottom: 10px">
            <label><b><?php _e("Pricing", 'ncquotes') ?> :</b></label>
            <div style="margin-top: 5px;padding-left: 10px">
                <div style="margin-bottom: 10px">
                    <b><?php _e('Size Based', 'ncquotes') ?> :</b>
                    <div style="margin-top: 5px;padding-left: 10px;max-height: 120px;overflow: auto">
                        <table style="width: 95%" border="1">
                            <thead>
                                <tr style="border-bottom: solid 1px">
                                    <th style="text-align: right;padding-right: 5px">Size</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($sizes as $term_id => $size) { ?>
                                    <tr>
                                        <td style="text-align: right;padding-right: 5px"><?php _e($size, 'ncquotes') ?></td>
                                        <td>
                                            <input type="number" name="nc_pricings[size][<?php echo $term_id ?>]" value="<?php echo isset($nc_pricings['size']) && isset($nc_pricings['size'][$term_id]) ? $nc_pricings['size'][$term_id] : '' ?>" style="width: 100%" min="0" step="any" />
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <p class="description" style="margin-left: 10px">
                        The Sizes with empty values will be skipped. If you want to show a particular size but didn't want to set any price for it make the price as <b>"0"</b>
                    </p>
                </div>
                <div>
                    <b><?php _e('Color Based', 'ncquotes') ?> :</b>
                    <div style="margin-top: 5px;padding-left: 10px;max-height: 120px;overflow: auto">
                        <table style="width: 95%" border="1">
                            <thead>
                                <tr style="border-bottom: solid 1px">
                                    <th style="text-align: right;padding-right: 5px">Color</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($colors as $term_id => $color) { ?>
                                    <?php
                                    $color_info = get_post($term_id);
                                    ?>
                                    <tr>
                                        <td style="text-align: right;padding-right: 5px;background: #<?php echo $color ?>">
                                            <span style="padding: 0 5px;background: #fff;border-radius: 3px"><?php _e($color_info->post_title, 'ncquotes') ?></span>
                                        </td>
                                        <td>
                                            <input type="number" name="nc_pricings[color][<?php echo $term_id ?>]" value="<?php echo isset($nc_pricings['color']) && isset($nc_pricings['color'][$term_id]) ? $nc_pricings['color'][$term_id] : '' ?>" style="width: 100%" min="0" step="any" />
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <p class="description" style="margin-left: 10px">
                        The Colors with empty values will be skipped. If you want to show a particular color but didn't want to set any price for it make the price as <b>"0"</b>
                    </p>
                </div>
            </div>
        </div>
        <hr />
        <div>
            <label><b><?php _e("Printing Types", 'ncquotes') ?> :</b></label>
            <div style="margin-top: 5px;padding-left: 10px;max-height: 120px;overflow: auto">
                <?php $i = 0 ?>
                <?php foreach ($printing_types as $term_id => $printing_type) { ?>
                    <?php
                    $checked = is_array($nc_printings) && in_array($term_id, $nc_printings) ? ' checked' : '';
                    $i++;
                    ?>
                    <label style="-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;">
                        <input type="checkbox" name="nc_printings[]" value="<?php echo $term_id ?>" <?php echo $checked ?> />
                        <?php _e($printing_type, 'ncquotes') ?>
                    </label>
                    <?php echo $i < count($printing_types) ? '<br />' : '' ?>
                <?php } ?>
            </div>
        </div>
        <hr />
        <div>
            <label><b><?php _e("Screen Printing Pricing Category", 'ncquotes') ?> :</b></label>
            <div style="margin-top: 5px;padding-left: 10px;max-height: 120px;overflow: auto">
                <?php
                $scr_printing_pricing_cats = nc_scr_printing_pricing_cats();
                ?>
                <?php foreach ($scr_printing_pricing_cats as $k => $pricing_cat) { ?>
                    <?php
                    $checked = $pricing_cat == $nc_scr_pricing_cat ? ' checked' : '';
                    ?>
                    <label style="-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;margin-right: 10px">
                        <input type="radio" name="nc_scr_pricing_cat" value="<?php echo $pricing_cat ?>" <?php echo $checked ? $checked : (!$k ? ' checked' : '') ?> />
                        <?php _e($pricing_cat, 'ncquotes') ?>
                    </label>
                    <?php echo $k < count($scr_printing_pricing_cats) - 1 ? '<br />' : '' ?>
                <?php } ?>
            </div>
        </div>
        <hr />
        <div style="margin-bottom: 10px">
            <label for="nc_product_tax"><b><?php _e("Tax", 'ncquotes') ?> :</b></label>
            <div style="margin-left: 10px;margin-top: 5px">
                <input type="number" step="1" id="nc_product_tax" name="nc_product_tax" value="<?php echo esc_attr($nc_product_tax) ?>" size="25" min="0"/>
                <p class="description">
                    Tax to be applied.
                </p>
            </div>
        </div>
        <?php
        echo ob_get_clean();
    }

    public function save_products_meta_boxes($post_id) {

        /*
         * We need to verify this came from our screen and with proper authorization,
         * because the save_post action can be triggered at other times.
         */

        // Check if our nonce is set.
        if (!isset($_POST['nc_render_products_meta_box_nonce'])) {
            return;
        }

        // Verify that the nonce is valid.
        if (!wp_verify_nonce($_POST['nc_render_products_meta_box_nonce'], 'nc_render_products_meta_box_nonce')) {
            return;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        // Check the user's permissions.
        if (!current_user_can('edit_post')) {
            return;
        }

        /* OK, it's safe for us to save the data now. */
        // Sanitize user input.
        $nc_pricings = isset($_POST['nc_pricings']) ? $_POST['nc_pricings'] : array();
        $nc_printings = isset($_POST['nc_printings']) ? $_POST['nc_printings'] : array();
        $nc_product_tax = sanitize_text_field($_POST['nc_product_tax']);
        $nc_scr_pricing_cat = isset($_POST['nc_scr_pricing_cat']) ? $_POST['nc_scr_pricing_cat'] : '';
        if (!empty($nc_pricings)) {
            $nc_pricings['size'] = array_filter($nc_pricings['size'], 'nc_anony_func_1');
            $nc_pricings['color'] = array_filter($nc_pricings['color'], 'nc_anony_func_1');
        }

        // Update the meta field in the database.
        update_post_meta($post_id, '_minimum_order_quantity', $minimum_order_quantity);
        update_post_meta($post_id, '_nc_pricings', $nc_pricings);
        update_post_meta($post_id, '_nc_printings', $nc_printings);
        update_post_meta($post_id, '_nc_scr_pricing_cat', $nc_scr_pricing_cat);
        update_post_meta($post_id, '_nc_product_tax', $nc_product_tax);
    }

    public function add_color_box($post_type) {
        $post_types = array('colors',);     //limit meta box to certain post types
        if (in_array($post_type, $post_types)) {
            add_meta_box(
                    'some_meta_box_name'
                    , __('Add colors', 'ncquotes')
                    , array($this, 'render_color_box_content')
                    , $post_type
                    , 'advanced'
                    , 'high'
            );
        }
    }

    public function add_size_box($post_type) {
        $post_types = array('colors',);     //limit meta box to certain post types
        if (in_array($post_type, $post_types)) {
            add_meta_box(
                    'some_meta_box_name'
                    , __('Add colors', 'ncquotes')
                    , array($this, 'render_category_box_content')
                    , $post_type
                    , 'advanced'
                    , 'high'
            );
        }
    }

    public function save_meta_box($post_id) {
        //echo '<pre>';  print_r($_POST); echo '</pre>'; exit;
        // echo '<pre>';
        $psze = $_POST['psize'];
        $pcolor = $_POST['pr_color'];

        //echo '<pre>'; print_r($pcolor); echo '</pre>'; exit;



        if (!isset($_POST['myplugin_inner_custom_box_nonce']))
            return $post_id;

        $nonce = $_POST['myplugin_inner_custom_box_nonce'];


        if (!wp_verify_nonce($nonce, 'myplugin_inner_custom_box'))
            return $post_id;

        // If this is an autosave, our form has not been submitted,
        //     so we don't want to do anything.
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;

        // Check the user's permissions.
        if ('page' == $_POST['post_type']) {

            if (!current_user_can('edit_page', $post_id))
                return $post_id;
        } else {

            if (!current_user_can('edit_post', $post_id))
                return $post_id;
        }

        $mydata1 = serialize($_POST['printing']);
        //$dt1=serialize($mydata1); 

        $mydata2 = sanitize_text_field($_POST['product_category']);
        //echo $mydata2;
        $mydata3 = sanitize_text_field($_POST['product_sub-cat']);
        $mydata4 = sanitize_text_field($_POST['sub_color']);
        $mydata5 = sanitize_text_field($_POST['color_price']);
        //$mydata6 = sanitize_text_field($_POST['size_price']);

        $mydata7 = implode(",", $_POST['chkid_']);
        $trm_print = trim($mydata7);
        //$mydata8 = implode(",", $_POST['psize']);
        //$trm_size = trim($mydata8);



        update_post_meta($post_id, 'chkid_', $trm_print);

        //update_post_meta($post_id, 'psize', $trm_size);
        $chk_price = array();
        foreach ($pcolor as $scolor => $sdata) {
            if ($sdata['color_price'] != NULL) {
                $chk_price[$sdata['subid_']][$sdata['sub_color']] = $sdata['color_price'];
            } else {
                continue;
            }
        }
        $data_color1 = serialize($chk_price);
        // echo '<pre>';  print_r($chk_price); echo '</pre>'; exit;
        update_post_meta($post_id, 'subcat_colors', $data_color1);

        $chk_size = array();
        //echo '<pre>'; print_r($psze); echo '</pre>'; exit;

        foreach ($psze as $kkt => $vv) {
            if ($vv['set'] == 1 && $vv['size'] != NULL) {
                $chk_size[] = array($vv['sname'] => $vv['size']);
            } else {
                continue;
            }
//                if($chk_size==1  && $chk_size!=NULL):
//                    
//                       $chk_seri = serialize($chk_size);
//                       print_r($chk_seri); exit;
//                   
//                    update_post_meta( $post_id, 'set', $chk_size );
//                    update_post_meta( $post_id, 'sname', $chk_size );
//                    update_post_meta( $post_id, 'size', $chk_size );
//                    
//                    echo 'yes';
//                
//                    else:
//                        
//                        echo 'no';
//                endif;
        }
        $data_size = serialize($chk_size);
        update_post_meta($post_id, 'p_size', $data_size);
        //print_r(unserialize($data));
        //   exit;
        //foreach($trm_print as $kk){
        //update_post_meta( $post_id, $kk, $mydata6 );
        // }
        //$mydata6 = sanitize_text_field( $_POST['$trm'] );
        //$ids = implode(",",$_POST["chkid_"]);
        //print_r($mydata7); exit;
        //echo 'asd'; print_r($mydata7); exit;
        //$mydata6 = sanitize_text_field( $_POST['size_price'] );
        update_post_meta($post_id, 'printing', $mydata1);
        update_post_meta($post_id, 'product_category', $mydata2);
        update_post_meta($post_id, 'product_sub-cat', $mydata3);
        //update_post_meta($post_id, 'sub_color', $mydata4);
        //update_post_meta($post_id, 'color_price', $mydata5);
        //update_post_meta($post_id, 'size_price', $mydata6);
    }

    public function save_color_box($post_id) {


        if (!isset($_POST['myplugin_inner_color_nonce']))
            return $post_id;

        $nonce = $_POST['myplugin_inner_color_nonce'];


        if (!wp_verify_nonce($nonce, 'myplugin_inner_color'))
            return $post_id;

        // If this is an autosave, our form has not been submitted,
        //     so we don't want to do anything.
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;

        // Check the user's permissions.
        if ('page' == $_POST['post_type']) {

            if (!current_user_can('edit_page', $post_id))
                return $post_id;
        } else {

            if (!current_user_can('edit_post', $post_id))
                return $post_id;
        }


        $mydata = sanitize_text_field($_POST['color']);
        $mydata2 = sanitize_text_field($_POST['is_dark_color']);
        $mydata3 = sanitize_text_field($_POST['sort_order']);
        update_post_meta($post_id, 'color', $mydata);
        update_post_meta($post_id, 'is_dark_color', $mydata2);
        update_post_meta($post_id, 'sort_order', $mydata3);
    }

    public function render_catcolor_box_content($post) {

        // Add an nonce field so we can check for it later.
        wp_nonce_field('myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce');
        $argss = array(
            'post_type' => 'products',
            'child_of' => 0,
            'parent' => 0,
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => 0,
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'number' => '',
            'taxonomy' => 'product_category'
        );

        $pro_cat = get_categories($argss);

        // echo '<pre>'; print_r( $pro_cat); echo '</pre>';
        $size_cs = array(
            'post_type' => 'products',
            'child_of' => 0,
            'parent' => '',
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => 0,
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'number' => '',
            'taxonomy' => 'product_size'
        );


        $size_cat = get_categories($size_cs);



        $codes_meta_boxes[] = array(
            array(
                'name' => __('Available Colors', 'Ncquotes'), // field name
                'desc' => __('Available Colors', 'Ncquotes'), // field description, optional
                'id' => $prefix . 'all_subcat_color', // field id, i.e. the meta key
                'type' => 'all_subcat_color', // text box
                'class' => 'acolor',
                'std' => ''                    // default value, optional
            ),
            array(
                'name' => __('Available All Size', 'Ncquotes'), // field name
                'desc' => __('Available All Size', 'Ncquotes'), // field description, optional
                'id' => $prefix . 'all_size', // field id, i.e. the meta key
                'type' => 'all_size', // text box
                'class' => 'allcolor',
                'std' => ''                    // default value, optional
            )
        );
        echo '<table class="form-table">';
        foreach ($codes_meta_boxes['0'] as $field) {
            $meta = get_post_meta($post->ID, $field['id'], true);

            echo '<tr>';
            // call separated methods for displaying each type of field
            //call_user_func(array(&$this, 'show_field_' . $field['type']), $field, $meta);
            echo '<th style="width:23%;"><label for="myplugin_new_field">';
            _e('' . $field['name'] . '', 'Ncquotes');
            echo '</label></th><td class="demo"> ';
            if ($field['type'] == 'all_subcat_color') {

                $all_subcat = array();

                foreach ($pro_cat as $key => $value) {
                    ?>

                    <?php
                    $all_subcat[] = $value->term_id;
                }

                $argsss = array(
                    'post_type' => 'products',
                    'child_of' => 0,
                    'parent' => '',
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'hide_empty' => 0,
                    'hierarchical' => 1,
                    'exclude' => $all_subcat,
                    'include' => '',
                    'number' => '',
                    'taxonomy' => 'product_category'
                );

                $pro_subcat = get_categories($argsss);

                $all_colors = array();

                $pro_subcats = get_post_meta($post->ID, 'product_sub-cat', true);
                // $post->ID;
                // $ttt=get_post_type($post->ID);

                if ($_GET['action'] == 'edit'):
//echo 'yes';

                    $p_subcolor = get_post_meta($post->ID, 'subcat_colors', true);
                    $color_un = unserialize($p_subcolor);
                    $total_array1 = array();
                    if (!empty($color_un)):
                        foreach ($color_un as $key => $data1) {
                            $total_array1 = $data1;
                        }
                    endif;
                    // echo '<pre>';
                    // print_r($total_array1);
                    // echo '</pre>';
                    foreach ($pro_subcat as $key => $value) {

                        $t_id = $value->term_id;

                        $term_meta = unserialize(get_option("taxonomy_$t_id"));
                        $all_colors[$t_id] = $term_meta['custom_term_meta'];
                    }
                    $all_color = '0';
                    foreach ($all_colors as $menu => $kk):
                        ?>
                        <span class="cat_color sub_cat_color_<?php echo $menu; ?>" <?php
                        if (array_key_exists($menu, $color_un)) {
                            echo 'style="display:block;"';
                        } else {
                            echo 'style="display:none;"';
                        }
                        ?>> 
                                  <?php foreach ($kk as $color):
                                      ?> 
                                <input type="hidden" class="sub_color"  name="pr_color[<?php echo $all_color; ?>][subid_]" value="<?php echo $menu; ?>">
                                <input type="text" class="sub_color"  name="pr_color[<?php echo $all_color; ?>][sub_color]" value="<?php echo $color; ?>">
                                <input type="text" class="color_price" name="pr_color[<?php echo $all_color; ?>][color_price]" value="<?php
                                if (array_key_exists($color, $total_array1)) {
                                    echo $total_array1["$color"];
                                }
                                ?>" placeholder="Enter color Price">
                                       <?php
                                       $all_color++;
                                   endforeach;
                                   echo '</span>';
                               endforeach;


                           else:
//echo 'No';
                               foreach ($pro_subcat as $key => $value) {

                                   $t_id = $value->term_id;

                                   $term_meta = unserialize(get_option("taxonomy_$t_id"));
                                   $all_colors[$t_id] = $term_meta['custom_term_meta'];
                               }

                               foreach ($all_colors as $menu => $kk):
                                   ?>
                            <span class="cat_color sub_cat_color_<?php echo $menu; ?> " style="display:none;"> 
                                <?php foreach ($kk as $color):
                                    ?>
                                    <input type="text" class="sub_color" name="sub_color[]" value="<?php echo $color; ?>">
                                    <input type="text" class="color_price" name="color_price[]" value="<?php echo $p_color1; ?>" placeholder="Enter color Price">

                                    <?php
                                endforeach;
                                echo '</span>';
                            endforeach;

                        endif;
                    }


                    elseif ($field['type'] == 'all_size') {
                        $size_p1 = get_post_meta($post->ID, 'p_size', true);
                        $size_un = unserialize($size_p1);
                        $total_array = array();
                        if (!empty($size_un)):
                            foreach ($size_un as $data) {
                                $total_array = array_merge($total_array, $data);
                            }
                        endif;
                        //echo '<pre>';
                        //print_r($total_array);
                        //echo '</pre>';
                        if (!empty($size_cat)):
                            $start = '0';
                            foreach ($size_cat as $key => $value) {
                                ?>

                                <tr> 
                                    <td style="width:5%;"> 

                                        <input type="checkbox" name="psize[<?php echo $start; ?>][set]" value="1" <?php
                                        if (array_key_exists($value->name, $total_array)) {
                                            echo 'checked';
                                        }
                                        ?> />
                                    </td>
                                    <td style="text-align:left;">
                                        <label class="size_<?php echo $key; ?>" name="psize[<?php echo $start; ?>][sname]"> <?php echo $value->name; ?>  </label>
                                        <input type="hidden" name="psize[<?php echo $start; ?>][sname]"  value="<?php echo $value->name; ?>" />        
                                    </td>
                                    <td>
                                        <input type="text" name="psize[<?php echo $start; ?>][size]" value="<?php
                                        if (array_key_exists($value->name, $total_array)) {
                                            echo $total_array["$value->name"];
                                        }
                                        ?>" placeholder="<?php echo $value->name; ?>" />        
                                    </td>
                                    </td>
                                </tr>
                                <?php
                                $start++;
                            } endif;
                    } else {
                        echo '<input type="' . $field['type'] . '" id="' . $field['id'] . '" name="' . $field['id'] . '"';
                        echo ' value="' . esc_attr($meta) . '" style="width:95%" />';
                        echo '</td></tr>';
                        wp_reset_postdata();
                    }
                }


                echo '</table>';
            }

            public function render_color_box_content($post) {

                // Add an nonce field so we can check for it later.
                wp_nonce_field('myplugin_inner_color', 'myplugin_inner_color_nonce');


                $codes_meta_boxes[] = array(// list of meta fields
                    array(
                        'name' => __('Add color', 'ncquotes'), // field name
                        'desc' => __('Add color', 'ncquotes'), // field description, optional
                        'id' => $prefix . 'color', // field id, i.e. the meta key
                        'type' => 'text', // text box
                        'class' => 'color',
                        'std' => '#66ff00'                    // default value, optional
                    ),
                    array(
                        'name' => __('Is Dark Color', 'ncquotes'),
                        'desc' => __('Is Dark Color', 'ncquotes'),
                        'id' => $prefix . 'is_dark_color',
                        'type' => 'checkbox',
                        'class' => 'is_dark_color',
                    ),
                    array(
                        'name' => __('Order', 'ncquotes'),
                        'desc' => __('Order to show the Color on front', 'ncquotes'),
                        'id' => $prefix . 'sort_order',
                        'type' => 'number',
                        'class' => 'sort_order',
                    ),
                );

                echo '<table class="form-table">';

                $categories = get_categories($args);

                foreach ($codes_meta_boxes['0'] as $field) {
                    $meta = get_post_meta($post->ID, $field['id'], true);
                    echo '<tr>';
                    // call separated methods for displaying each type of field
                    //call_user_func(array(&$this, 'show_field_' . $field['type']), $field, $meta);
                    echo '<th style="width: 20%;"><label for="' . $field['id'] . '">';
                    _e($field['name'], 'ncquotes');
                    echo '</label></th><td>';

                    if ($field['type'] == 'text') {

                        echo '<input type="' . $field['type'] . '" id="' . $field['id'] . '" class="' . $field['class'] . '" name="' . $field['id'] . '"';
                        echo ' value="' . esc_attr($meta) . '" style="width:95%" />';
                    } elseif ($field['type'] == 'checkbox') {
                        echo '<input type="hidden" name="' . $field['id'] . '" value="0" />';
                        echo '<input type="' . $field['type'] . '" id="' . $field['id'] . '" name="' . $field['id'] . '" value="1"' . ($meta == 1 ? ' checked' : '') . ' />';
                    } else {
                        echo '<input type="' . $field['type'] . '" id="' . $field['id'] . '" name="' . $field['id'] . '"';
                        echo ' value="' . esc_attr($meta) . '" style="width:95%" />';
                    }
                    echo '</td></tr>';
                }
                wp_reset_postdata();
                echo '</table>';
            }
        }