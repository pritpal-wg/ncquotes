<?php 
// ShortCodes For plugins

	/* Function to add weather forecast to all cities */
	function weather_forecast_for_all() {
		global $citiesArray;
		
		$return='<section id="search" style="opacity:9">
 	
			<div class="container">
				<div class="row pager">
					<div class="col-xs-12">
							<div class="search-logo">
							<h1 class="vaderprognosa-h1"><a href="#">'.__("Weather","weather").' </a></h1>
							</div>
					</div>
				</div>	
				
				<div class="divider"></div>
				<div id="search-transparent-border" class="row pager">
					<form class="form-inline" role="form" action="" method="POST" id="search_city_form">
					  <div id="connect-search-button"  class="form-group col-xs-12">
							<label class="sr-only" for="hitta-vader"><!--translatable-->Skriv in orten du söker väder för.<!--/translatable-end--></label>
							<input type="search" name="city" class="form-control searchCity" id="hitta-vader" placeholder="'.__("Display weather for...","weather").'" maxlength="20" autocomplete="off" />
							<span class="input-search-btn">
								<button type="submit" class="btn btn-info btn-lg custom-search" type="button">
									<i class="glyphicon glyphicon-search"></i>
								</button>
							</span>
					  </div>					
					</form>
				</div>
				
				<div class="divider"></div>

		</div> <!-- /container -->
	</section>
	<section id="hide-weather">     
		<section id="weather-header" class="hide">	
			<div class="container" id="weatherCityHead">
				<div class="row">
 <div data-original-title="Stäng" class="icon-weather-control icon-close-weather" data-toggle="tooltip" data-placement="bottom" title="" onclick="hidemydiv()">
                     <a href="javaScript:;">
                     <span class="mobile-icon-only x-icon">+</span>     
                     <span class="x-mark animateXMark">
                     <span class="line left"></span>      
                     <span class="line right"></span>
                     </span>
                     </a>
                </div>
                
                
                <!-- Temporary Alert -->
                <script>
function myFunction() {
    alert("We are still in BETA testing, This function will be will be available at a later stage.");
}
</script>
                <div data-original-title="Spara som favorit" class="icon-weather-control icon-favorite-weather" data-toggle="tooltip" data-placement="bottom" title="">
                    <a href="javaScript:;" onclick="myFunction()">
                        <span class="glyphicon glyphicon-star-empty mobile-icon-only" aria-hidden="true"></span> 
                     <span class="x-mark animateXMark">
                     <span class="line left"></span>
                         <span class="line left bottom"></span> 
                     <span class="line middle"></span> 
                         <span class="line right bottom"></span> 
                     <span class="line right"></span>
                     </span>
                     </a>
                </div>
                
                <div data-original-title="Dela med vänner" class="icon-weather-control icon-like-weather" data-toggle="tooltip" data-placement="bottom" title="">
                    <a href="javaScript:;" data-placement="bottom" data-toggle="modal" data-target=".bs-example-modal-sm"> 
                   <span class="glyphicon glyphicon-thumbs-up mobile-icon-only" aria-hidden="true"></span> 
                    <span class="x-mark animateXMark">
                     <span class="line left"></span>      
                     <span class="line right"></span>
                     </span>
                     </a>
                </div>
					<h1><span><!--translatable-->'.__("Weather in","weather").'<!--/translatable-end--></span> <!--searched city name to diplay here--><b id="weatherCityName"></b><span class="glyphicon glyphicon-map-marker"></span></h1>
				</div>
			</div>
		</section>
		<section id="weather-display" class="hide">';
			
		$return.='</section>
	</section>
	<div class="transparentview"></div>
	<!-- Small modal -->
                
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    
  
  <div class="modal-dialog ">
    <div class="modal-content">
     
        <div class="modal-body">
            
        <ul>
        <li><a rel="" onclick="_trackableshare_window = window.open(`this.href,`share`,`menubar=0,resizable=1,width=500,height=350`); _trackableshare_window.focus(); return false;" href="http://www.facebook.com/sharer.php?u=http://prognosa.se/" class="fb-light">Dela på Facebook</a></li>
        <li><a rel="" onclick="_trackableshare_window = window.open(this.href,`share`,`menubar=0,resizable=1,width=500,height=350`); _trackableshare_window.focus(); return false;" href="http://twitter.com/share?text=Håll+koll+på+vädret+med+Prognosa.se&amp;url=http://prognosa.se/"   class="tw-light">Dela på Twitter</a></li>
        <li><a rel="" onclick="_trackableshare_window = window.open(this.href,`share`,`menubar=0,resizable=1,width=500,height=350`); _trackableshare_window.focus(); return false;" href="https://plus.google.com/share?url=http://prognosa.se/" class="gl-light">Dela på Google+</a></li>
        </ul>
            
        <div class="modal-close-button">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>    
            
        </div>
        
    </div>
  </div>
</div><!-- /End Small modal -->
	
	';   
	return $return;
	}
	add_shortcode('weather_forecast', 'weather_forecast_for_all');
	
	
	/* Function to add weather forecast to all cities */
	function weather_forecast_for_single($arg) {
		global $citiesArray;
		
		$return='';
		if(!isset($arg['id'])):
			$return.=_e('Sorry, No City Found..!','weather');
		else:
			$city=$arg['id'];
			$city_name=$citiesArray[$city]['name'];
			$return.='
		<section id="search">	
			<div class="container">
				<div class="row pager">
					<div class="col-xs-12">
							<div class="search-logo">
							<h1 class="vaderprognosa-h1"><a href="#">'.__("Weather","weather").' </a></h1>
							</div>
					</div>
				</div>	
				
				<div class="divider"></div>
				<div id="search-transparent-border" class="row pager">
					<form class="form-inline" role="form" action="" method="POST" id="search_city_form">
					  <div id="connect-search-button"  class="form-group col-xs-12">
							<label class="sr-only" for="hitta-vader"><!--translatable-->Skriv in orten du söker väder för.<!--/translatable-end--></label>
							<input type="search" name="city" class="form-control searchCity" id="hitta-vader" placeholder="'.__("Display weather for...","weather").'" maxlength="85" autocomplete="off" />
							<span class="input-search-btn">
								<button type="submit" class="btn btn-info btn-lg custom-search" type="button">
									<i class="glyphicon glyphicon-search"></i>
								</button>
							</span>
					  </div>					
					</form>
				</div>
				
				<div class="divider"></div>

		</div> <!-- /container -->
		</section>			
			<section id="weather-header">	
						<div class="container" id="">
							<div class="row">
          <div data-original-title="Stäng" class="icon-weather-control icon-close-weather" data-toggle="tooltip" data-placement="bottom" title="" onclick="hidemydiv()">
                     <a href="javaScript:;">
                     <span class="mobile-icon-only x-icon">+</span>     
                     <span class="x-mark animateXMark">
                     <span class="line left"></span>      
                     <span class="line right"></span>
                     </span>
                     </a>
                </div>
                
                
                <!-- Temporary Alert -->
                <script>
function myFunction() {
    alert("We are still in BETA testing, This function will be will be available at a later stage.");
}
</script>
                <div data-original-title="Spara som favorit" class="icon-weather-control icon-favorite-weather" data-toggle="tooltip" data-placement="bottom" title="">
                    <a href="javaScript:;" onclick="myFunction()">
                        <span class="glyphicon glyphicon-star-empty mobile-icon-only" aria-hidden="true"></span> 
                     <span class="x-mark animateXMark">
                     <span class="line left"></span>
                         <span class="line left bottom"></span> 
                     <span class="line middle"></span> 
                         <span class="line right bottom"></span> 
                     <span class="line right"></span>
                     </span>
                     </a>
                </div>
                
                <div data-original-title="Dela med vänner" class="icon-weather-control icon-like-weather" data-toggle="tooltip" data-placement="bottom" title="">
                    <a href="javaScript:;" data-placement="bottom" data-toggle="modal" data-target=".bs-example-modal-sm"> 
                   <span class="glyphicon glyphicon-thumbs-up mobile-icon-only" aria-hidden="true"></span> 
                    <span class="x-mark animateXMark">
                     <span class="line left"></span>      
                     <span class="line right"></span>
                     </span>
                     </a>
                </div>
								<h1><span><!--translatable-->'.__("Weather in","weather").'<!--/translatable-end--></span> <!--searched city name to diplay here--><b id="weatherCityName">'.$city_name.'</b><span class="glyphicon glyphicon-map-marker"></span></h1>
							</div>
						</div>
					</section>
			<section id="weather-display">';
				/* Display Data for This City */
				$return.=showWeatherInformation($city);	
			$return.='</section><div class="transparentview"></div>
<!-- Small modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
     
        <div class="modal-body">
            
        <ul>
        <li><a rel="" onclick="_trackableshare_window = window.open(`this.href,`share`,`menubar=0,resizable=1,width=500,height=350`); _trackableshare_window.focus(); return false;" href="http://www.facebook.com/sharer.php?u=http://prognosa.se/" class="fb-light">Dela på Facebook</a></li>
        <li><a rel="" onclick="_trackableshare_window = window.open(this.href,`share`,`menubar=0,resizable=1,width=500,height=350`); _trackableshare_window.focus(); return false;" href="http://twitter.com/share?text=Håll+koll+på+vädret+med+Prognosa.se&amp;url=http://prognosa.se/"   class="tw-light">Dela på Twitter</a></li>
        <li><a rel="" onclick="_trackableshare_window = window.open(this.href,`share`,`menubar=0,resizable=1,width=500,height=350`); _trackableshare_window.focus(); return false;" href="https://plus.google.com/share?url=http://prognosa.se/" class="gl-light">Dela på Google+</a></li>
        </ul>
            
        <div class="modal-close-button">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>    
            
        </div>
        
    </div>
  </div>
</div><!-- /End Small modal -->
			
			';
		endif;
		
		return $return;
	}
	add_shortcode('weather', 'weather_forecast_for_single');
	
?>