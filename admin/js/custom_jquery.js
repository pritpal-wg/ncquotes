(function ($) {
    $(document).ready(function () {
        //$('.cat_color').hide();
        $('.pcat').on('change', function () {
            var cat_id = $(this).val();
            $('.cat_color').hide();
            $(".subcat").val('');
            $(".subcat > option").each(function () {
                var rel = $(this).attr('rel');

                if ('sub_' + cat_id == rel) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            })
        })

        $('.subcat').on('change', function () {
            var cat_id = $(this).val();
            //alert(cat_id); 
            $('.cat_color').hide();
            $('.sub_cat_color_' + cat_id).show();
        })
    });
})(jQuery);