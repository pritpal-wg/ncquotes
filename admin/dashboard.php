<?php
global $wpdb;
global $citiesArray;

if(isset($_POST['dashboard_city'])):
	update_option('dashboard_city',$_POST['dashboard_city']);
endif;

$dashboard_city = get_option('dashboard_city');

?>
<div class="wrap">
	<h2><?php echo __('LW-Quote Dashboard','ncquote');?>​</h2>

	<div id="welcome-panel" class="welcome-panel">
		<div class="welcome-panel-content">
		<p class="about-description"></p>
		<div class="welcome-panel-column-container">
			<div class="weather_data">

				
			
			<div class='pull-right select_city_div'>
				<form method='POST'>
				<div class="dashboard_city">
				<label><strong><?php echo __('Select City','weather');?></strong></label>
				<select name='dashboard_city' onchange="this.form.submit()">
					<?php	
						foreach ($citiesArray as $countid=>$contval): 
							if(empty($dashboard_city)):
								$dashboard_city=$countid;
							endif; ?>
						<option value='<?php echo $countid;?>' <?php echo ($dashboard_city==$countid)?'selected':'';?> ><?php echo $contval['name'];?></option>	
					<?php endforeach; ?>
				</select>
				</div>
				</form>
			</div>
			
			
			<section id="weather-header">	
						<div id="" class="container">
							<div class="row">
								<h1><span><!--translatable--><?php echo __('Weather in','weather');?><!--/translatable-end--></span> <!--searched city name to diplay here--><?php echo $citiesArray[$dashboard_city]['name'];?><span class="glyphicon glyphicon-map-marker"></span></h1>
							</div>
						</div>
			</section>
			<div class='bracker'></div>
			<hr/>
			<?php  echo show_weather_dashboard($dashboard_city);?>
			</div>
			
		</div>
	</div>

	<div class="clear"></div>
</div>
</div>
