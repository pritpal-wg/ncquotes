<?php

//error_reporting(E_ALL);
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Ncquotes
 *
 * @wordpress-plugin
 * Plugin Name:       Ncquotes
 * Description:       This Plugin is developed to Save The quotes Information.
 * Version:           1.0.0
 * Author:            CwebConsultants
 * Author URI:        http://www.cwebconsultants.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nclogowear
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}
@session_start();
define('ADV_PLUGIN_FS_PATH1', plugin_dir_path(__FILE__));
define('ADV_PLUGIN_WS_PATH1', plugin_dir_url(__FILE__));
define('ADV_IMAGE_PLACEHOLDER', ADV_PLUGIN_WS_PATH1 . "public/img/placeholder.png");
//default printing types
$_printing_slugs = array(
    'embroidery' => 'embroidery',
    'personalization_vinyl' => 'personalization-vinyl',
    'printable_vinyl' => 'printable-vinyl',
    'screen_printing' => 'screen-printing',
    '2screen_printing' => '2screen-printing',
    'sublimation' => 'sublimation',
    'hats_embroidery' => 'hats-embroidery',
    'all_drinkware' => 'all-drinkware',
);
update_option('_nc_default_printings', $_printing_slugs);
update_option('nc_all_print_locations', '1 Side, 2 Sides');

require ADV_PLUGIN_FS_PATH1 . 'includes/classes/classNcquotes.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name() {

    $plugin = new Ncquotes();
    $plugin->run();
}

run_plugin_name();


//Declares Common Fucntion File 
require_once ADV_PLUGIN_FS_PATH1 . 'includes/function/functions.php';

//include widget for categories
require_once ADV_PLUGIN_FS_PATH1 . 'includes/classes/class-nc_cat_widget.php';

//include the page templater
require_once ADV_PLUGIN_FS_PATH1 . 'includes/classes/PageTemplater/classPageTemplater.php';

//error message
if (isset($_SESSION['nc_admin_notices']) && $_SESSION['nc_admin_notices']) {
    add_action('admin_notices', $_SESSION['nc_admin_notices']);
    unset($_SESSION['nc_admin_notices']);
}
add_action('edited_product_category', 'save_product_category_custom_meta');
add_action('create_product_category', 'save_product_category_custom_meta');
add_action('product_category_edit_form_fields', 'pippin_product_category_edit_meta_field');
add_action('product_category_add_form_fields', 'pippin_product_category_add_new_meta_field');

add_action('product_printing_edit_form_fields', 'nc_product_printing_edit_form_fields');
add_action('product_printing_add_form_fields', 'nc_product_printing_add_form_fields');
add_action('edited_product_printing', 'nc_save_product_printing_fields');
add_action('create_product_printing', 'nc_save_product_printing_fields');

add_filter('wp_update_term_parent', 'nc_update_term_parent', 10, 3);
add_action('admin_init', 'nc_register_settings');
add_action('admin_footer', 'nc_active_current_menu');
add_action('wp_ajax_nc_add_emb_pricing', 'nc_add_emb_pricing');
add_action('wp_ajax_nc_add_scr_pricing', 'nc_add_scr_pricing');
add_action('wp_ajax_nc_add_2sc_pricing', 'nc_add_2sc_pricing');
add_action('wp_ajax_nc_add_pri_pricing', 'nc_add_pri_pricing');
add_action('wp_ajax_nc_add_hat_pricing', 'nc_add_hat_pricing');
add_action('wp_ajax_nc_add_pre_pricing', 'nc_add_pre_pricing');
add_action('wp_ajax_nc_add_all_pricing', 'nc_add_all_pricing');
add_action('wp_ajax_nc_add_sub_pricing', 'nc_add_sub_pricing');
add_action('wp_ajax_nc_send_user_quote_email', 'nc_send_user_quote_email');
add_action('wp_ajax_nc_delete_pricing', 'nc_delete_pricing');
add_action('wp_ajax_nc_update_pricing', 'nc_update_pricing');
add_action('wp_ajax_nopriv_nc_send_user_quote_email', 'nc_send_user_quote_email');
add_action('admin_head', 'nc_hide_add_quote_link');
add_action('add_meta_boxes', 'nc_quotes_meta_box');
//image for categories
add_action('product_category_add_form_fields', 'nc_add_tax_image');
add_action('product_category_edit_form_fields', 'nc_edit_texonomy_field');
add_filter('manage_edit-product_category_columns', 'nc_taxonomy_columns');
add_filter('manage_product_category_custom_column', 'nc_taxonomy_column', 10, 3);
add_action('edit_term', 'nc_save_taxonomy_image');
add_action('create_term', 'nc_save_taxonomy_image');
//add_action('nc_after_add_quote', 'nc_send_email_admin');
if (strpos($_SERVER['SCRIPT_NAME'], 'edit-tags.php') > 0) {
    add_action('admin_head', 'nc_add_style');
    add_action('quick_edit_custom_box', 'nc_quick_edit_custom_box', 10, 3);
    add_filter("attribute_escape", "nc_change_insert_button_text", 10, 2);
}

add_action('product_size_add_form_fields', 'nc_product_size_add_form_fields');
add_action('product_size_edit_form_fields', 'nc_product_size_edit_form_fields');
add_action('edited_product_size', 'nc_save_product_size_meta');
add_action('create_product_size', 'nc_save_product_size_meta');
add_filter('manage_edit-product_size_columns', 'nc_add_product_size_columns');
add_filter('manage_product_size_custom_column', 'nc_add_product_size_column_content', 10, 3);
add_filter('nc_get_product_pricings', 'nc_sort_product_sizes');
?>